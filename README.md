# HiveMind

Killer Queen stat tracker

## Project Repositories

#### Main Repository: https://gitlab.com/kqhivemind/hivemind-nfc-client

This repository. Contains all server code and configuration.

#### Stats Client: https://gitlab.com/kqhivemind/hivemind-client

Contains the Node.js client that connects to the cabinet and relays the messages to the HiveMind server.

#### NFC Client: https://gitlab.com/kqhivemind/hivemind-nfc-client

Contains the code to run the sign-in client for NFC reader boxes.

#### Raspberry Pi Config: https://gitlab.com/kqhivemind/hivemind-pi-config

Contains scripts to create the Raspberry Pi OS image for NFC reader boxes.

## Local Development

### Initial Setup

Check out code:

    `git clone https://gitlab.com/kqhivemind/hivemind.git`

To run initial setup:

    `make initial-setup`

If you just want to start containers without database migrations:

    `make up`

The app should now be running on your local machine at [http://localhost:8087](http://localhost:8087).

To shutdown your containers:

    `make down`

### OAuth Configuration

**This section may be deprecated**

Logging in requires OAuth 2.0 credentials, which cannot be checked in or shared publicly. To log in to the site, you will need to generate your own credentials.

 - Go to the [Google API Console](https://console.developers.google.com)
 - Create a new project using the "Select a project" link at the top
 - In the menu bar under "APIs & Services", select "OAuth Consent Screen" and follow the steps to create one.
 - In the menu bar, select "Credentials", then "Create Credentials", then "OAuth client ID"
 - For "Application type", select "Web application"
 - For "Authorized JavaScript origins", add both `http://localhost` and `http://localhost:8087`
 - For "Authorized redirect URIs", enter `http://localhost:8087/auth`
 - Paste the Client ID and Secret into the `app.env` file as the values for `OAUTH_CLIENT_ID` and `OAUTH_CLIENT_SECRET`
 - Run `docker-compose up -d` to apply these config changes to your running environment

### Test Lab

To create a scene with four cabinets and add yourself as an administrator, first visit http://localhost:8087 and log in to create your user account, then run:

    `EMAIL=<your admin email here> make test-scene`

The [HiveMind client repository](https://gitlab.com/kqhivemind/hivemind-client) contains a test configuration that will simulate four cabinets running games at random and a client that connects to your local instance.
