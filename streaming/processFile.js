#!/usr/bin/env node
const fs = require('fs');
const readline = require('readline');
const { format } = require('date-fns');
const events = require('events');
const { Pool, Client } = require('pg');
const redis = require('redis');

const redisClient = redis.createClient({host: process.env.REDIS_HOST, password: process.env.REDIS_PASSWORD});

const QUERIES = {
  startGame: `INSERT INTO game_game (start_time, map_name, cabinet_id, uuid)
              VALUES ($1, $2, $3, $4)
              ON CONFLICT (uuid) DO UPDATE SET start_time = $1, map_name = $2, cabinet_id = $3
              RETURNING id`,

  updateGame: `UPDATE game_game
               SET end_time = $1, win_condition = $2, winning_team = $3
               WHERE id = $4`,

  getGame: `SELECT *
            FROM game_game
            WHERE uuid = $1`,

  getCabinet: `SELECT c.*, s.name AS scene_name
               FROM game_cabinet c
                 JOIN game_scene s ON (c.scene_id = s.id)
               WHERE lower(c.name) = lower($1)
                 AND lower(s.name) = lower($2)`,

  insertEvent: `INSERT INTO game_gameevent (uuid, timestamp, event_type, values, game_id)
                VALUES ($1, $2, $3, $4, $5)
                ON CONFLICT (uuid) DO UPDATE SET timestamp = $2, event_type = $3, values = $4, game_id = $5`,

  cabinetOnline: `UPDATE game_cabinet
                  SET client_status = 'online', last_connection = now(), last_connection_ip = $1
                  WHERE id = $2`,

  cabinetOffline: `UPDATE game_cabinet
                   SET client_status = 'offline'
                   WHERE id = $1`,
};

function getCacheKey(message) {
  return `${message.cabinet.sceneName}/${message.cabinet.cabinetName}`;
}

function log(...message) {
  console.log(format(new Date(), 'yyyy-MM-dd HH:mm:ss'), ...message);
}

const pgConfig = {
  user: process.env.POSTGRES_USER,
  host: process.env.POSTGRES_HOST,
  database: process.env.POSTGRES_DB,
  password: process.env.POSTGRES_PASSWORD,
  connectionTimeoutMillis: 1000,
  idleTimeoutMillis: 1000,
};
const pgPool = new Pool(pgConfig);

async function startGame(pgClient, cabinetInfo, message) {
  const result = await pgClient.query(QUERIES.startGame, [
    new Date(message.time),
    message.values[0],
    cabinetInfo.cabinetID,
    message.gameID,
  ]);

  const gameInfo = {
    cabinetInfo,
    gameID: result.rows[0].id,
    startTime: new Date(message.time),
  };

  return gameInfo;
}

async function getGameInfo(pgClient, message) {
  if (message.gameID in gameInfoCache) {
    return gameInfoCache[message.gameID];
  }

  if (gameInfoLastQuery[message.gameID]) {
    return null;
  }

  const result = await pgClient.query(QUERIES.getGame, [message.gameID]);
  if (result && result.rows && result.rows.length > 0) {
    const cabinetInfo = await getCabinetInfo(pgClient, message);
    gameInfoCache[message.gameID] = {
      cabinetInfo,
      gameID: result.rows[0].id,
      startTime: result.rows[0].start_time,
    };

    log(`Fetched game ${result.rows[0].id} for UUID ${message.gameID}`);

    delete gameInfoLastQuery[message.gameID];
    return gameInfoCache[message.gameID];
  }

  gameInfoLastQuery[message.gameID] = Date.now();
}

async function updateResult(pgClient, gameInfo, victoryMessage) {
  const cabinetInfo = gameInfo.cabinetInfo;

  gameInfo.isComplete = true;

  const result = await pgClient.query(QUERIES.updateGame, [
    new Date(victoryMessage.time),
    victoryMessage.values[1],
    victoryMessage.values[0].toLowerCase(),
    gameInfo.gameID,
  ]);
}

async function getCabinetInfo(pgClient, message) {
  const cacheKey = getCacheKey(message);
  if (!(cacheKey in cabinetInfoCache && cabinetInfoCache[cacheKey].lastUpdate > Date.now() - 60000)) {
    log(`Updating cabinet info for ${cacheKey}...`);
    const result = await pgClient.query(QUERIES.getCabinet, [
      message.cabinet.cabinetName,
      message.cabinet.sceneName,
    ]);

    if (result && result.rows && result.rows.length > 0) {
      let row = result.rows[0];
      cabinetInfoCache[cacheKey] = {
        cabinetID: row.id,
        cabinetName: row.name.toLowerCase(),
        token: row.token,
        sceneName: row.scene_name.toLowerCase(),
        lastUpdate: Date.now(),
      };

      await pgClient.query(QUERIES.cabinetOnline, [message.ws._socket.remoteAddress, row.id]);

      log(`Updated cabinet info for ${cacheKey} (cabinet ID ${row.id})`);
    } else {
      log(`Cabinet ${cacheKey} not found.`);
    }
  }

  return cabinetInfoCache[cacheKey];
}

const queuedMessages = [];
const queuedMessagesByID = new Set();
const cabinetInfoCache = {};
const gameInfoCache = {};
const gameInfoLastQuery = {};
const gamesEnded = [];
const cabinets = new Set();
let fileReadComplete = false;

async function processFiles() {
  for (const fn of process.argv.slice(2)) {
    log(`Reading from file ${fn}`);

    const rl = readline.createInterface({
      input: fs.createReadStream(fn),
      crlfDelay: Infinity,
    });

    rl.on('line', messageString => {
      try {
        var message = JSON.parse(messageString);
        if(!message)
          return;

        if (!queuedMessagesByID.has(message.eventID)) {
          queuedMessages.push(message);
          queuedMessagesByID.add(message.eventID);
          cabinets.add(getCacheKey(message));
        }
      } catch (error) {
        log(`Couldn't parse message: ${error}`);
      }
    });

    await events.once(rl, 'close');
    log(`Done reading file ${fn}`);
  }

  fileReadComplete = true;
}

async function processQueue(pgClient) {
  if (queuedMessages.length === 0)
    return;

  const gameStarts = queuedMessages.filter(msg => msg.type == 'gamestart').length;
  log(`${gameStarts} game start messages remaining.`);

  await pgClient.query('begin');

  for (const message of queuedMessages) {
    if (message.type == 'gamestart') {
      const cabinetInfo = await getCabinetInfo(pgClient, message);
      if (message.cabinet.token != cabinetInfo.token) {
        log(`Invalid token for ${message.cabinet.cabinetName}: ${message.cabinet.token} != ${cabinetInfo.token}`);
        continue;
      }

      const gameInfo = gameInfoCache[message.gameID] = await startGame(pgClient, cabinetInfo, message);
    }
  }

  await pgClient.query('commit');
  await pgClient.query('begin');

  const messageBatch = queuedMessages.splice(0, Math.min(queuedMessages.length, 1000));
  for (const message of messageBatch) {
    const gameInfo = await getGameInfo(pgClient, message);
    if (!gameInfo)
      continue;
    const cabinetInfo = gameInfo.cabinetInfo;

    if (message.cabinet.token != cabinetInfo.token) {
      log(`Invalid token for ${message.cabinet.cabinetName}: ${message.cabinet.token} != ${cabinetInfo.token}`);
      continue;
    }

    const messageTime = new Date(message.time);
    if (messageTime.getTime() >= gameInfo.startTime.getTime() - 12000) {
      await pgClient.query(QUERIES.insertEvent, [
        message.eventID,
        messageTime,
        message.type,
        message.values,
        gameInfo.gameID,
      ]);

      if (message.type == 'victory') {
        await updateResult(pgClient, gameInfo, message);
        gamesEnded.push({ message, gameInfo, cabinetInfo });
      }

      message.processed = true;
    }
  }

  await pgClient.query('commit');

  for (const message of messageBatch) {
    if (!message.processed && (gameStarts > 0 || !fileReadComplete)) {
      queuedMessages.push(message);
    }
  }
}

function tryProcessQueue() {
  pgPool.connect().then(pgClient => {
    processQueue(pgClient).then(() => {
    }).catch(error => {
      log('Error processing queue: ', error);
      pgClient.query('rollback').catch(() => {});
    }).finally(() => {
      pgClient.release();
    });
  }).catch(error => {
    log('Error connecting to database', error);
  }).finally(() => {
    if (!fileReadComplete || queuedMessages.length > 0) {
      log(`${queuedMessages.length} messages in queue.`);
      setTimeout(tryProcessQueue, 1000);
    } else {
      log(`Done processing. Files contained ${Object.keys(gameInfoCache).length} games.`);
      process.exit(0);
    }
  });
}

setTimeout(tryProcessQueue, 1000);
processFiles();
