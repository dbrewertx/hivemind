#!/usr/bin/env node
const http = require('http');
const request = require('request');
const WebSocket = require('ws');
const redis = require('redis');
const axios = require('axios');

function log(message) {
  console.log(new Date() + ' ' + message);
}

function heartbeat() {
  this.isAlive = true;
}

const server = http.createServer(function (request, response) {
  response.writeHead(404);
  response.end();
});

const redisClient = redis.createClient({
  host: process.env.REDIS_HOST,
  password: process.env.REDIS_PASSWORD,
});
const subscriptions = {};
const wss = new WebSocket.Server({ server });

const interval = setInterval(function ping() {
  wss.clients.forEach(function each(ws) {
    if (!ws.isAlive) return ws.terminate();

    ws.isAlive = false;
    ws.ping(() => {});
  });
}, 30000);

wss.on('connection', async (ws, request) => {
  log(`New connection to ${request.url}`);

  const [tournamentID] = request.url.split('/').reverse();

  if (!(tournamentID in subscriptions)) {
    log(`Subscribing to Redis channel tournament.${tournamentID}.*`);
    subscriptions[tournamentID] = new Set();
    redisClient.psubscribe(`tournament.${tournamentID}.*`);
  }

  subscriptions[tournamentID].add(ws);

  ws.isAlive = true;
  ws.on('pong', heartbeat);

  ws.on('close', () => {
    log(`Connection to ${request.url} closed.`);
    subscriptions[tournamentID].delete(ws);
  });
});

redisClient.on('pmessage', async (pattern, channel, message) => {
  const [subchannel, tournamentID] = channel.split('.').reverse();

  const messageText = JSON.stringify({
    type: subchannel,
    data: JSON.parse(message),
  });

  for (const ws of subscriptions[tournamentID]) {
    try {
      ws.send(messageText);
    } catch (err) {
      log(err);
      ws.close();
    }
  }
});

server.listen(8080);
