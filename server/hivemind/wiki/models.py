from datetime import datetime, timedelta

from django.db import models
from django.utils import timezone

from ..model import BaseModel
from ..user.models import User


class Page(BaseModel):
    slug = models.SlugField(unique=True)

    def current_revision(self):
        return self.revision_set.filter(deleted=False).order_by("-publish_date").first()


class Revision(BaseModel):
    page = models.ForeignKey(Page, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    slug = models.SlugField()
    publish_date = models.DateTimeField(default=timezone.now)
    published_by = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name="+")
    page_text = models.TextField()
    deleted = models.BooleanField(default=False)
    deleted_by = models.ForeignKey(User, null=True, on_delete=models.SET_NULL, related_name="+")
