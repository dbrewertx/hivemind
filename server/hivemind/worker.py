from celery import Celery
from django.conf import settings

task_modules = [
    "hivemind.stats.tasks",
    "hivemind.game.tasks",
    "hivemind.tournament.challonge",
    "hivemind.tournament.tasks",
    "hivemind.user.tasks",
    "hivemind.video.tasks",
]

if settings.REDIS_PASSWORD:
    broker_url = f"redis://:{settings.REDIS_PASSWORD}@{settings.REDIS_HOST}:6379/0"
else:
    broker_url = f"redis://{settings.REDIS_HOST}:6379/0"

app = Celery("hivemind.worker", broker=broker_url, include=task_modules)
app.conf.result_backend = broker_url
app.conf.beat_schedule = {
    "hivemind.stats.tasks.check_sign_in_timeouts": {
        "task": "hivemind.stats.tasks.check_sign_in_timeouts",
        "schedule": 60*60,
    },
}
