import hashlib
import itertools
import json
import logging
import os
import secrets
from datetime import datetime, timedelta

import pytz
from django.contrib.auth.models import AbstractUser, UserManager
from django.db import models, transaction
from rest_framework.authtoken.models import Token

from ..client import redis_client
from ..constants import AggregateStatType, StatType
from ..game.models import Cabinet, CabinetPosition, Game, GameStat, Scene
from ..model import BaseModel

logger = logging.getLogger(__name__)


class User(AbstractUser):
    name = models.CharField(max_length=200, default="")
    email = models.EmailField(unique=True)
    is_site_admin = models.BooleanField(default=False)
    is_profile_public = models.BooleanField(default=False)
    scene = models.CharField(max_length=10, null=True, blank=True)
    image = models.ImageField(null=True, blank=True, upload_to="user/user-images/")
    pronouns = models.CharField(max_length=30, null=True, blank=True)

    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = []

    objects = UserManager()

    @classmethod
    def by_email(cls, email):
        try:
            return cls.objects.get(email=email)
        except cls.DoesNotExist:
            return cls(email=email)

    def generate_token(self):
        token = Token.objects.create(user=self)
        token.save()

        return token

    def get_current_signin(self):
        return SignInLog.objects.filter(user=self, is_current=True, action=SignInLog.Action.SIGN_IN).first()

    def is_admin_of(self, scene):
        return self.has_permission(scene, PermissionType.ADMIN)

    def has_permission(self, scene, permission):
        return self.permissions.filter(scene=scene, permission=permission).count() > 0

    def get_user_scenes(self):
        scenes = {}

        for usergame in self.usergame_set.filter(game__start_time__gt=datetime.now() - timedelta(days=30)) \
                                         .order_by("-game__end_time"):
            scene = usergame.game.cabinet.scene
            if scene.name not in scenes:
                scenes[scene.name] = {
                    "name": scene.name,
                    "display_name": scene.display_name,
                    "background_color": scene.background_color,
                    "last_played": usergame.game.end_time,
                }

        for permission in self.permissions.filter(permission=PermissionType.ADMIN):
            scene = permission.scene
            if scene.name in scenes:
                scenes[scene.name]["is_admin"] = True
            else:
                scenes[scene.name] = {
                    "name": scene.name,
                    "display_name": scene.display_name,
                    "background_color": scene.background_color,
                    "is_admin": True,
                }

        return sorted(scenes.values(), key=lambda s: s["display_name"])

    def get_last_session(self):
        return self.playsession_set.order_by("-end_time").first()

    @transaction.atomic
    def collect_stats(self):
        all_stat_types = list(AggregateStatType) + list(StatType)

        for user_game in self.usergame_set.filter(play_session__isnull=True) \
                                          .select_for_update(skip_locked=True) \
                                          .order_by("game__start_time"):
            # New session if they haven't played in the past 4 hours
            with transaction.atomic():
                try:
                    session = self.playsession_set.get(
                        start_time__lte=user_game.game.end_time,
                        end_time__gt=user_game.game.end_time - timedelta(hours=4),
                    )
                    session.end_time = max(session.end_time, user_game.game.end_time)
                    if session.cabinet is not None and session.cabinet != user_game.game.cabinet:
                        session.cabinet = None

                except PlaySession.DoesNotExist:
                    session = PlaySession(
                        user=self,
                        cabinet=user_game.game.cabinet,
                        start_time=user_game.game.start_time,
                        end_time=user_game.game.end_time,
                    )

                session.save()

            game_stats = {i: 0 for i in all_stat_types}
            game_stats[AggregateStatType.GAMES] += 1

            if CabinetPosition(user_game.player_id).team == user_game.game.winning_team:
                game_stats[AggregateStatType.WINS] += 1
            else:
                game_stats[AggregateStatType.LOSSES] += 1

            game_stats[AggregateStatType.TIME_PLAYED] += int((user_game.game.end_time - user_game.game.start_time).total_seconds() * 1000)

            for game_stat in user_game.game.gamestat_set.filter(player_id=user_game.player_id):
                game_stats[game_stat.stat_type] += game_stat.value

            for stat_type, value in game_stats.items():
                user_stat, _ = UserStat.objects.get_or_create(user=self, stat_type=stat_type)
                user_stat.value += value
                user_stat.save()

                user_pos_stat, _ = UserPositionStat.objects.get_or_create(
                    user=self,
                    player_id=user_game.player_id,
                    stat_type=stat_type,
                )
                user_pos_stat.value += value
                user_pos_stat.save()

                session_stat, _ = PlaySessionStat.objects.get_or_create(session=session, stat_type=stat_type)
                session_stat.value += value
                session_stat.save()

                session_pos_stat, _ = PlaySessionPositionStat.objects.get_or_create(
                    session=session,
                    player_id=user_game.player_id,
                    stat_type=stat_type,
                )

                session_pos_stat.value += value
                session_pos_stat.save()

            user_game.play_session = session
            user_game.save()

    def get_stats(self):
        stats_dict = {i.stat_type: i for i in self.userstat_set.all()}
        stats = []

        for stat_type in itertools.chain(AggregateStatType, StatType):
            stats.append({
                "name": stat_type.value,
                "label": stat_type.label,
                "value": stat.value if (stat := stats_dict.get(stat_type)) else 0,
            })

        return stats

    def get_stats_by_position(self):
        stats_dict = {(i.player_id, i.stat_type): i for i in self.userpositionstat_set.all()}
        stats = []

        for stat_type in itertools.chain(AggregateStatType, StatType):
            stats.append({
                "name": stat_type,
                "label": stat_type.label,
                "values": {
                    i.value: stat.value if (stat := stats_dict.get((i.value, stat_type))) else 0
                    for i in CabinetPosition
                },
            })

        return stats

    def get_last_session_stats(self):
        session = self.get_last_session()
        if session is None:
            stats_dict = {}
        else:
            stats_dict = {i.stat_type: i for i in session.playsessionstat_set.all()}

        stats = []

        for stat_type in itertools.chain(AggregateStatType, StatType):
            stats.append({
                "name": stat_type,
                "label": stat_type.label,
                "value": stat.value if (stat := stats_dict.get(stat_type)) else 0,
            })

        return stats


class PermissionType(models.TextChoices):
    ADMIN = "admin", "Scene Admin"
    TOURNAMENT = "tournament", "Tournament Manager"


class Permission(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="permissions")
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE, related_name="permissions")
    permission = models.TextField(max_length=20, choices=PermissionType.choices)

    PermissionType = PermissionType

    def __repr__(self):
        return "<Permission: {} / {} / {}>".format(
            self.user.email,
            self.permission,
            self.scene.name,
        )


class UserGame(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    game = models.ForeignKey(Game, on_delete=models.CASCADE)
    player_id = models.IntegerField(choices=CabinetPosition.choices)
    play_session = models.ForeignKey("user.PlaySession", null=True, on_delete=models.SET_NULL)

    class Meta:
        unique_together = ("game", "player_id")


class SceneRequest(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    submit_date = models.DateTimeField(default=datetime.utcnow)
    user_name = models.CharField(max_length=50)
    name = models.CharField(max_length=10)
    display_name = models.CharField(max_length=30)
    comments = models.TextField(null=True, blank=True)
    completed = models.BooleanField(default=False)

    def create_scene(self):
        if self.completed:
            return

        scene = Scene(name=self.name, display_name=self.display_name)
        scene.save()

        Permission.objects.create(user=user, scene=scene, permission=Permission.PermissionType.ADMIN)
        Message.objects.create(user=self.user, url="/scene/{}".format(scene.name),
                               message_text="Your scene, {}, has been created.".format(self.display_name))

        self.completed = True
        self.save()


class Message(BaseModel):
    timestamp = models.DateTimeField(default=datetime.utcnow)
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    message_text = models.TextField()
    is_read = models.BooleanField(default=False)
    url = models.TextField(null=True)


class UserStat(BaseModel):
    stat_type_choices = AggregateStatType.choices + StatType.choices

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    stat_type = models.TextField(max_length=20, choices=stat_type_choices)
    value = models.IntegerField(default=0)

    class Meta:
        unique_together = ("user", "stat_type")


class UserPositionStat(BaseModel):
    stat_type_choices = AggregateStatType.choices + StatType.choices

    user = models.ForeignKey(User, on_delete=models.CASCADE)
    player_id = models.IntegerField(choices=CabinetPosition.choices)
    stat_type = models.TextField(max_length=20, choices=stat_type_choices)
    value = models.IntegerField(default=0)

    class Meta:
        unique_together = ("user", "player_id", "stat_type")


class PlaySession(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    cabinet = models.ForeignKey(Cabinet, null=True, on_delete=models.CASCADE)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()

    class Meta:
        unique_together = ("user", "start_time")


class PlaySessionStat(BaseModel):
    stat_type_choices = AggregateStatType.choices + StatType.choices

    session = models.ForeignKey(PlaySession, on_delete=models.CASCADE)
    stat_type = models.TextField(max_length=20, choices=stat_type_choices)
    value = models.IntegerField(default=0)

    class Meta:
        unique_together = ("session", "stat_type")


class PlaySessionPositionStat(BaseModel):
    stat_type_choices = AggregateStatType.choices + StatType.choices

    session = models.ForeignKey(PlaySession, on_delete=models.CASCADE)
    player_id = models.IntegerField(choices=CabinetPosition.choices)
    stat_type = models.TextField(max_length=20, choices=stat_type_choices)
    value = models.IntegerField(default=0)

    class Meta:
        unique_together = ("session", "player_id", "stat_type")
