from django.db.models import Q
from django_filters import rest_framework as filters

from .models import PlayerInfo, TournamentMatch


class TournamentMatchFilter(filters.FilterSet):
    team_id = filters.NumberFilter(method="team_id_filter")

    def team_id_filter(self, queryset, name, value):
        return queryset.filter(Q(blue_team_id=value) | Q(gold_team_id=value))

    class Meta:
        model = TournamentMatch
        fields = [
            "bracket_id",
            "team_id",
            "active_cabinet_id",
        ]


class PlayerInfoFilter(filters.FilterSet):
    tournament_id = filters.NumberFilter(field_name="field__tournament_id")

    class Meta:
        model = PlayerInfo
        fields = [
            "player_id",
            "field_id",
            "tournament_id",
        ]
