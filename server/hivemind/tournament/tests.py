import http.client
from datetime import datetime

from ..tests import HiveMindTest
from ..user.models import User


class TournamentTestCase(HiveMindTest):
    def setUp(self):
        super().setUp()

        self._basic_user = User(username="test ID 2", email="test2@test.com", is_site_admin=False)
        self._basic_user.save()

    def test_create_tournament(self):
        data = {
            "name": "Test Tournament",
            "description": "Test Tournament",
            "date": datetime.now().strftime("%Y-%m-%d"),
            "is_active": False,
            "scene": self._scene.id,
            "link_type": "manual",
        }

        response = self.client.post("/api/tournament/tournament/", data)
        tournament_id = response.data["id"]

        data.update({
            "allow_registration": True,
            "registration_close_date": None,
            "accept_payments": False,
            "teams": [],
            "brackets": [],
        })

        response = self.client.put("/api/tournament/tournament/{}/".format(tournament_id), data)

        data = {
            "tournament": tournament_id,
            "field_description": "Text Field",
            "field_type": "text",
            "is_required": False,
            "choices": [],
            "order": 1,
        }

        response = self.client.post("/api/tournament/player-info-field/", data)
        field_1 = response.data
        field_name_1 = response.data["field_name"]

        data.update({
            "field_description": "Required Text Field",
            "order": 2,
            "is_required": True,
        })

        response = self.client.post("/api/tournament/player-info-field/", data)
        field_2 = response.data
        field_name_2 = response.data["field_name"]

        response = self.client.get("/api/tournament/player-info-field/", params={"tournament": tournament_id})
        self.assertEqual(response.data["count"], 2)

        data = {
            "id": field_2["id"],
            "field_description": "Required Text Field (Edited)",
        }

        response = self.client.patch("/api/tournament/player-info-field/{}/".format(field_2["id"]), data)
        self.assertEqual(response.status_code, http.client.OK)

        self.client.force_authenticate(self._basic_user)

        data = {
            "tournament": tournament_id,
            "user": self._basic_user.id,
            "name": self.get_player_names(1)[0],
            "pronouns": "they/them",
            "tidbit": "I'm not real",
            field_name_1: "Text Value",
            field_name_2: "Text Value",
        }

        response = self.client.put("/api/tournament/tournament/{}/register/".format(tournament_id), data)

        response = self.client.get("/api/tournament/tournament/{}/registration/".format(tournament_id))
        self.assertEqual(response.data[field_name_1], "Text Value")
        self.assertEqual(response.data[field_name_2], "Text Value")
