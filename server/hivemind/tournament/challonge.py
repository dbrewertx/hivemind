import json
import urllib

import requests
from celery import shared_task
from django.conf import settings

from ..client import redis_client
from ..worker import app


def do_request(path, org=None, method="GET", data=None):
    url = urllib.parse.urljoin("https://api.challonge.com/v1/", path)

    params = None
    subdomain = org
    if method == "GET":
        params={"api_key": settings.CHALLONGE_API_KEY}
        if subdomain:
            params["subdomain"] = subdomain
    else:
        if data is None:
            data = {}

        data["api_key"] = settings.CHALLONGE_API_KEY
        if subdomain:
            data["subdomain"] = subdomain

    headers = requests.utils.default_headers()
    headers.update({ "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/113.0.0.0 Safari/537.36" })

    request = requests.request(method, url, params=params, json=data, headers=headers)
    request.raise_for_status()
    return request

def get_tournament_id(id, org=None):
    if org:
        return "{}-{}".format(org, id)

    return id

def list_tournaments(org=None):
    request = do_request("tournaments.json", org)
    return [i["tournament"] for i in request.json()]

def get_tournament(tournament_id, org=None):
    tournament_id = get_tournament_id(tournament_id, org)
    request = do_request("tournaments/{}.json".format(tournament_id), org)
    return request.json()["tournament"]

def get_participants(tournament_id, org=None):
    tournament_id = get_tournament_id(tournament_id, org)
    request = do_request("tournaments/{}/participants.json".format(tournament_id), org)

    participants = {}
    for row in request.json():
        participants[row["participant"]["id"]] = row["participant"]["name"]
        for group_id in row["participant"]["group_player_ids"]:
            participants[group_id] = row["participant"]["name"]

    return participants

def set_participants(tournament_id, teams, org=None):
    tournament_id = get_tournament_id(tournament_id, org)
    do_request("tournaments/{}/participants/clear.json".format(tournament_id), org, method="DELETE")
    # 422 = it already started

    data = {
        "participants": [
            {
                "name": team.name,
                "seed": idx+1,
                "misc": team.id,
            }
            for idx, team in enumerate(teams)
        ],
    }

    response = do_request("tournaments/{}/participants/bulk_add.json".format(tournament_id), org,
                          method="POST", data=data)

    participants = response.json()
    return [i["participant"]["id"] for i in sorted(participants, key=lambda i: i["participant"]["seed"])]

def get_matches(tournament_id, org=None):
    tournament_id = get_tournament_id(tournament_id, org)
    request = do_request("tournaments/{}/matches.json".format(tournament_id), org)

    matches = []
    first_round_match = None
    stage = 1
    for row in request.json():
        # Assume matches come back in order
        # This is a dangerous assumption, but should give us the info in most cases
        match_stage_id = (row["match"]["round"], row["match"]["identifier"])
        if not first_round_match:
            first_round_match = match_stage_id
        elif match_stage_id == first_round_match:
            stage += 1
        match = {
            "id": row["match"]["id"],
            "state": row["match"]["state"],
            "team1": row["match"]["player1_id"],
            "team2": row["match"]["player2_id"],
            "round": int(row["match"]["round"]),
            "stage": stage,
        }

        matches.append(match)

    return matches

def report_result(match):
    if match.is_flipped:
        team2_score, team1_score = match.blue_score, match.gold_score
    else:
        team1_score, team2_score = match.blue_score, match.gold_score

    if match.bracket.report_as_sets:
        match_data = {
            "scores_csv": ",".join(
                ["1-0" for i in range(int(team1_score))] +
                ["0-1" for i in range(int(team2_score))]
            ),
        }
    else:
        match_data = {
            "scores_csv": "{}-{}".format(team1_score, team2_score),
        }

    winner = None
    if match.is_complete:
        winner = "tie"
        if team1_score > team2_score:
            winner = "player1_id"
        if team2_score > team1_score:
            winner = "player2_id"

    report_result_send.delay(
        match.bracket.tournament_id,
        match.bracket.linked_bracket_id,
        match.linked_match_id,
        match_data,
        winner,
        match.bracket.linked_org,
    )

    add_attachment.delay(
        match.bracket.linked_bracket_id,
        match.linked_match_id,
        "https://kqhivemind.com/tournament-match/{}".format(match.id),
        match.bracket.linked_org,
    )

@shared_task
def report_result_send(tournament_id, linked_bracket_id, match_id, match_data, winner, org=None):
    linked_bracket_id = get_tournament_id(linked_bracket_id, org)
    path = "tournaments/{}/matches/{}.json".format(linked_bracket_id, match_id)

    if winner:
        request = do_request(path, org)

        team1_id = request.json()["match"]["player1_id"]
        team2_id = request.json()["match"]["player2_id"]

        match_data["winner_id"] = request.json()["match"].get(winner, "tie")

    request = do_request(path, org, method="PUT", data={"match": match_data})
    app.send_task("hivemind.tournament.tasks.get_available_matches", args=[tournament_id])

@shared_task
def add_attachment(tournament_id, match_id, url, org=None):
    tournament_id = get_tournament_id(tournament_id, org)
    path = "tournaments/{}/matches/{}/attachments.json".format(tournament_id, match_id)
    description = "HiveMind Match Stats"

    tournament = get_tournament(tournament_id)
    if not tournament.get("accept_attachments"):
        do_request("tournaments/{}.json".format(tournament_id), org, method="PUT", data={
            "tournament": {
                "accept_attachments": True,
            },
        })

    attachments = do_request(path, org)
    for attachment in attachments.json():
        if attachment["match_attachment"]["description"] == description:
            return

    do_request(path, org, method="POST", data={
        "match_attachment": {
            "url": url,
            "description": "HiveMind Match Stats",
        },
    })
