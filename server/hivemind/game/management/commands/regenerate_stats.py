import logging

from django.core.management.base import BaseCommand
from django.db import transaction
from hivemind.game.models import Game, GameStat
from hivemind.user.models import User
from tqdm import tqdm

logger = logging.getLogger()

class Command(BaseCommand):
    @transaction.atomic
    def handle(self, *args, **kwargs):
        GameStat.objects.all().delete()

        for game in tqdm(Game.objects.all()):
            try:
                game.collect_player_stats(regenerate=True)
            except Exception as e:
                logger.warning("Could not regenerate game {}: {}".format(game.id, e), exc_info=True)

        for user in User.objects.all():
            user.collect_stats()
