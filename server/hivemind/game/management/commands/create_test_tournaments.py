from django.core.management.base import BaseCommand

from hivemind.game.models import Scene
from hivemind.tournament.models import Tournament


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument("--count", help="Number of tournaments to create.")

    def handle(self, *args, **kwargs):
        count = int(kwargs.get("count"))

        if count is None:
            return

        for i in range(count):
            tournament = Tournament(
                name="Test Tournament {}".format(i+1),
                scene=Scene.objects.get(name="test"),
            )

            tournament.save()
