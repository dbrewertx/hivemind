from rest_framework import serializers

from ..user.models import UserGame
from .models import Cabinet, Game, Scene

UserGameSerializer = UserGame.serializer()

class SceneSerializer(serializers.ModelSerializer):
    def update(self, instance, validated_data):
        validated_data.pop("name", None)
        return super().update(instance, validated_data)

    name = serializers.RegexField("^[a-z0-9]+$", max_length=20, min_length=2)
    total_games = serializers.IntegerField(read_only=True)
    last_game_time = serializers.DateTimeField(source="last_game.end_time", read_only=True)

    class Meta:
        model = Scene
        fields = "__all__"


class CabinetSerializer(serializers.ModelSerializer):
    name = serializers.RegexField("^[a-z0-9]+$", max_length=20, min_length=3)
    total_games = serializers.IntegerField(read_only=True)
    last_game_time = serializers.DateTimeField(source="last_game.end_time", read_only=True)

    class Meta:
        model = Cabinet
        fields = ["id", "name", "display_name", "scene", "address", "description", "time_zone",
                  "allow_qr_signin", "allow_nfc_signin", "allow_tournament_player", "total_games",
                  "last_game_time", "client_status", "twitch_channel_name"]


class CabinetAdminSerializer(CabinetSerializer):
    class Meta:
        model = Cabinet
        fields = CabinetSerializer.Meta.fields + ["token"]


class GameSerializer(serializers.ModelSerializer):
    map_name = serializers.CharField(source="get_map_name_display", read_only=True)
    cabinet = CabinetSerializer(read_only=True)
    scene = SceneSerializer(read_only=True)
    users = UserGameSerializer(read_only=True, many=True, source="usergame_set")

    class Meta:
        model = Game
        fields = "__all__"
