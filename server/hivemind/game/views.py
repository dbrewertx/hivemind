import pytz
import qrcode
import qrcode.image.svg
from django.db import transaction
from django.http import HttpResponse
from django.shortcuts import get_object_or_404
from rest_framework.decorators import action
from rest_framework.exceptions import PermissionDenied
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response

from ..constants import HTTPMethod
from ..model import BaseViewSet
from ..overlay.models import Overlay
from ..stats.models import SignInLog
from .filters import CabinetActivityFilter, GameFilter
from .models import Cabinet, CabinetActivity, Game, GameEvent, GameMap, Scene
from .permissions import CabinetPermission, GamePermission, ScenePermission
from .serializers import (CabinetAdminSerializer, CabinetSerializer,
                          GameSerializer, SceneSerializer)


class SceneViewSet(BaseViewSet):
    queryset = Scene.objects.all().order_by("display_name")
    serializer_class = SceneSerializer
    permission_classes = [ScenePermission]
    filterset_fields = ["name"]


class CabinetViewSet(BaseViewSet):
    queryset = Cabinet.objects.all().order_by("name")
    serializer_class = CabinetSerializer
    filterset_fields = ["scene_id", "name"]
    permission_classes = [CabinetPermission]

    def get_serializer_class(self):
        if not (self.detail and self.request.user.is_authenticated):
            return CabinetSerializer

        cabinet = self.get_object()
        if self.request.user.is_admin_of(cabinet.scene):
            return CabinetAdminSerializer

        return CabinetSerializer

    @transaction.atomic
    def create(self, request):
        response = super().create(request)

        Overlay.objects.create(
            cabinet_id=response.data["id"],
            is_default=True,
        )

        return response

    @action(detail=False)
    def timezones(self, request):
        return Response({"results": pytz.all_timezones})

    @action(detail=True, methods=[HTTPMethod.GET])
    def signin(self, request, pk=None):
        cabinet = get_object_or_404(Cabinet, id=pk)
        signed_in = SignInLog.objects.filter(cabinet=cabinet, action=SignInLog.Action.SIGN_IN, is_current=True)

        return Response({
            "id": cabinet.id,
            "signed_in": [i.get_publish_data() for i in signed_in],
        })


class GameViewSet(BaseViewSet):
    queryset = Game.objects.all()
    serializer_class = GameSerializer
    filterset_class = GameFilter
    permission_classes = [GamePermission]

    @action(detail=False)
    def recent(self, request):
        games = self.filter_queryset(self.get_queryset()) \
                    .filter(end_time__isnull=False) \
                    .order_by("-start_time")

        page = self.paginate_queryset(games)
        if page is not None:
            serializer = self.get_serializer(page, many=True)
            return self.get_paginated_response(serializer.data)

        serializer = self.get_serializer(games, many=True)
        return Response(serializer.data)

    @action(detail=True)
    def stats(self, request, pk=None):
        game = Game.objects.get(id=pk)
        return Response(game.get_postgame_stats())

    @action(detail=True, methods=[HTTPMethod.GET])
    def qr(self, request, pk=None):
        response = HttpResponse()
        response["Content-Type"] = "image/svg+xml"

        image = qrcode.make("https://kqhivemind.com/game/{}".format(pk),
                            image_factory=qrcode.image.svg.SvgPathImage)
        image.save(response)

        return response


class GameEventViewSet(BaseViewSet):
    queryset = GameEvent.objects.all().order_by("timestamp")
    serializer_class = GameEvent.serializer()
    filterset_fields = ["game_id", "event_type"]


class CabinetActivityPagination(PageNumberPagination):
    page_size = 7 * 24


class CabinetActivityViewSet(BaseViewSet):
    queryset = CabinetActivity.objects.all()
    serializer_class = CabinetActivity.serializer()
    filterset_class = CabinetActivityFilter
    pagination_class = CabinetActivityPagination


class GameMapViewSet(BaseViewSet):
    queryset = GameMap.objects.all()
    serializer_class = GameMap.serializer()
    filterset_fields = ["name"]
