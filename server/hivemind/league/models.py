import hashlib
import itertools
import json
import logging
import math
import random
from datetime import datetime

import trueskill
from django.db import models, transaction

from ..client import redis_client
from ..game.models import Cabinet, CabinetTeam, Scene
from ..model import BaseModel

trueskill_env = trueskill.TrueSkill(draw_probability=0.0)
logger = logging.getLogger(__name__)

PLAYERS_PER_MATCH = 10


class Player(BaseModel):
    name = models.CharField(max_length=200, unique=True)
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE)

    def results_by_season(self):
        seasons = {}

        for team in self.team_set.all():
            event = team.event
            season = event.season
            if season.id not in seasons:
                seasons[season.id] = {"season": season, "events": {}}

            event_result = seasons[season.id]["events"][event.id] = team.results()
            event_result["event"] = event

        for qp_player in self.qpplayer_set.distinct("qp_match__event_id"):
            event = qp_player.qp_match.event
            season = event.season
            if season.id not in seasons:
                seasons[season.id] = {"season": season, "events": {}}

            event_results = event.qp_player_results()
            for player_result in event_results:
                if player_result["player"].id == self.id:
                    event_result = seasons[season.id]["events"][event.id] = player_result
                    event_result["event"] = event

        season_results_sorted = []
        for season in sorted(seasons.values(), key=lambda x: x["season"].start_date):
            season_result = {"season": season["season"], "events": [], "rounds_won": 0, "rounds_lost": 0, "matches_won": 0, "matches_lost": 0, "points": 0}

            season_result["events"] = sorted(season["events"].values(), key=lambda x: x["event"].event_date)

            max_events_counted = season["season"].event_set.count() - season["season"].drop_lowest_scores
            for event_result in sorted(season_result["events"], key=lambda x: x["points"], reverse=True)[:max_events_counted]:
                for k in ["rounds_won", "rounds_lost", "matches_won", "matches_lost", "points"]:
                    season_result[k] += event_result[k]

            season_results_sorted.append(season_result)

        return season_results_sorted

    def last_event(self):
        last_team = self.team_set.order_by("-event__event_date").first()
        last_qp_match = self.qpplayer_set.order_by("-qp_match__event__event_date").first()

        if last_team and (last_qp_match is None or last_team.event.event_date < last_qp_match.qp_match.event.event_date):
            return last_team.event

        if last_qp_match:
            return last_qp_match.qp_match.event

        return None

    @classmethod
    def by_name(cls, name):
        try:
            return cls.objects.filter(name=name).get()
        except cls.DoesNotExist:
            return cls(name=name)

    def rating(self):
        rating = PlayerRating.objects.filter(player=self).order_by("-rating_date").first()
        if rating is None:
            return trueskill_env.create_rating()

        return rating.rating()


class Season(BaseModel):
    class SeasonType(models.TextChoices):
        SHUFFLE = "shuffle", "Shuffle"
        BYOT = "byot", "Bring Your Own Team"

    class PlayoffType(models.TextChoices):
        NONE = "none", "None"
        DOUBLE_ELIM = "double_elim", "Double Elimination"

    name = models.CharField(max_length=200)
    scene = models.ForeignKey(Scene, on_delete=models.CASCADE)
    start_date = models.DateField(null=True)
    end_date = models.DateField(null=True)
    drop_lowest_scores = models.IntegerField(default=0)
    match_count_base = models.IntegerField(default=0)
    season_type = models.CharField(max_length=20, choices=SeasonType.choices)
    playoff_type = models.CharField(max_length=20, null=True, choices=PlayoffType.choices, default=PlayoffType.NONE)
    playoff_teams = models.IntegerField(default=0, null=True)

    @property
    def event_count(self):
        return self.event_set.count()

    @classmethod
    def result_sort_key(cls, result):
        return (
            result["matches_won"] / (result["matches_won"] + result["matches_lost"]) if result["matches_won"] + result["matches_lost"] else 0,
            result["rounds_won"] / (result["rounds_won"] + result["rounds_lost"]) if result["rounds_won"] + result["rounds_lost"] else 0,
            result.get("tiebreak"),
        )

    def team_results(self):
        results = [i.results() for i in self.byoteam_set.all()]

        # First tiebreaker: head-to-head record
        result_sort_keys = {self.result_sort_key(i) for i in results}
        if len(results) > len(result_sort_keys):
            for sort_key in result_sort_keys:
                tied_results = [i for i in results if self.result_sort_key(i) == sort_key]
                if len(tied_results) == 1:
                    continue

                results_by_id = {i["team"].id: i for i in tied_results}

                hth_results = {i: {"rounds_won": 0, "rounds_lost": 0, "matches_won": 0, "matches_lost": 0} for i in results_by_id.keys()}
                hth_matches = Match.objects.filter(
                    event__season_id=self.id,
                    blue_team__byoteam_id__in=results_by_id.keys(),
                    gold_team__byoteam_id__in=results_by_id.keys(),
                    is_complete=True,
                )

                for match in hth_matches:
                    hth_results[match.blue_team.byoteam_id]["rounds_won"] += match.blue_score
                    hth_results[match.gold_team.byoteam_id]["rounds_won"] += match.gold_score
                    hth_results[match.blue_team.byoteam_id]["rounds_lost"] += match.gold_score
                    hth_results[match.gold_team.byoteam_id]["rounds_lost"] += match.blue_score

                    if match.winning_team():
                        hth_results[match.winning_team().byoteam_id]["matches_won"] += 1
                    if match.losing_team():
                        hth_results[match.losing_team().byoteam_id]["matches_lost"] += 1

                for result in tied_results:
                    hth_result = hth_results[result["team"].id]
                    result["tiebreak"] = (
                        hth_result["matches_won"] / (hth_result["matches_won"] + hth_result["matches_lost"]) \
                        if hth_result["matches_won"] + hth_result["matches_lost"] else 0,
                        hth_result["rounds_won"] / (hth_result["rounds_won"] + hth_result["rounds_lost"]) \
                        if hth_result["rounds_won"] + hth_result["rounds_lost"] else 0,
                    )

                    result["tiebreak_desc"] = ["Head-to-Head Tiebreaker: matches {}-{}, rounds {}-{}".format(
                        hth_result["matches_won"],
                        hth_result["matches_lost"],
                        hth_result["rounds_won"],
                        hth_result["rounds_lost"],
                    )]

        # Last tiebreaker: consistently "random"
        # This is probably a terrible idea and I hope we never have to use it
        result_sort_keys = {self.result_sort_key(i) for i in results}
        if len(results) > len(result_sort_keys):
            for sort_key in result_sort_keys:
                tied_results = [i for i in results if self.result_sort_key(i) == sort_key]
                if len(tied_results) == 1:
                    continue

                for result in tied_results:
                    hash_value = hashlib.md5(
                        bytes((result["rounds_won"], result["rounds_lost"], result["matches_won"], result["matches_lost"], 1))
                    ).hexdigest()

                    result["tiebreak"] += (hash_value, )
                    result["tiebreak_desc"].append("Tie broken by coin flip")

        return sorted(results, reverse=True, key=self.result_sort_key)

    def player_results(self):
        results_by_player = {}

        for event in self.event_set.all():
            if event.is_quickplay:
                player_results = event.qp_player_results()
                for result in player_results:
                    if result["player"].id not in results_by_player:
                        results_by_player[result["player"].id] = {"player": result["player"], "events": []}

                    results_by_player[result["player"].id]["events"].append(result)

            else:
                team_results = event.team_results()

                for team_result in team_results:
                    for player in team_result["team"].players.all():
                        if player.id not in results_by_player:
                            results_by_player[player.id] = {"player": player, "events": []}

                        results_by_player[player.id]["events"].append(team_result)

        results = []
        max_events_counted = self.event_set.count() - self.drop_lowest_scores
        for player_results in results_by_player.values():
            result = {
                "points": 0,
                "rounds_won": 0,
                "rounds_lost": 0,
                "matches_won": 0,
                "matches_lost": 0,
                "player_id": player_results["player"].id,
                "player_name": player_results["player"].name,
            }

            player_results["events"].sort(key=lambda x: x["points"], reverse=True)
            for event_result in player_results["events"][:max_events_counted]:
                for k in ["rounds_won", "rounds_lost", "matches_won", "matches_lost", "points"]:
                    result[k] += event_result[k]

            results.append(result)

        return sorted(results, key=lambda x: x["points"], reverse=True)


class Event(BaseModel):
    event_date = models.DateField()
    name = models.CharField(max_length=200, null=True)
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    cabinet = models.ForeignKey(Cabinet, null=True, on_delete=models.SET_NULL)
    is_complete = models.BooleanField(default=False)
    is_active = models.BooleanField(default=False)
    is_quickplay = models.BooleanField(default=False)
    is_playoff = models.BooleanField(default=False)
    unassigned_players = models.ManyToManyField(Player)
    points_per_round = models.IntegerField(default=0)
    points_per_match = models.IntegerField(default=0)
    rounds_per_match = models.IntegerField(default=0)
    wins_per_match = models.IntegerField(default=0)
    qp_matches_per_player = models.IntegerField(default=0)
    matches_per_opponent = models.IntegerField(default=1)

    def get_name(self):
        return self.name or self.event_date.strftime("%B %d, %Y")

    def get_name_and_date(self):
        if self.name:
            return "{} - {}".format(self.name, self.event_date.strftime("%B %d, %Y"))

        return self.event_date.strftime("%B %d, %Y")

    def team_results(self):
        results = {}

        for team in self.team_set.all():
            results[team.id] = team.results()

        return sorted(results.values(), key=lambda x: (x["points"], -x["team"].id), reverse=True)

    def qp_player_results(self):
        results = {}

        for qp_match in self.qpmatch_set.all():
            match_result = {
                "blue": {
                    "rounds_won": qp_match.blue_score,
                    "rounds_lost": qp_match.gold_score,
                    "matches_won": (1 if qp_match.blue_score > qp_match.gold_score else 0),
                    "matches_lost": (1 if qp_match.gold_score > qp_match.blue_score else 0),
                },
                "gold": {
                    "rounds_won": qp_match.gold_score,
                    "rounds_lost": qp_match.blue_score,
                    "matches_won": (1 if qp_match.gold_score > qp_match.blue_score else 0),
                    "matches_lost": (1 if qp_match.blue_score > qp_match.gold_score else 0),
                },
            }

            for qp_player in qp_match.qpplayer_set.all():
                player_result = results[qp_player.player.id] = results.get(qp_player.player.id, {})
                player_result["player"] = qp_player.player

                for k in ("rounds_won", "rounds_lost", "matches_won", "matches_lost"):
                    player_result[k] = player_result.get(k, 0) + match_result[qp_player.team][k]

                player_result["points"] = player_result["rounds_won"] * self.points_per_round + \
                    player_result["matches_won"] * self.points_per_match

        return sorted(results.values(), key=lambda x: (x["points"], x["rounds_won"]), reverse=True)

    def player_count(self):
        if self.is_quickplay:
            return QPPlayer.objects.filter(qp_match__event_id=self.id).distinct("player_id").count()

        return sum([t.players.count() for t in self.team_set.all()])

    def event_number(self):
        return self.season.event_set.filter(event_date__lte=self.event_date).count()

    def generate_teams(self, num_teams, teams_to_include=None):
        if self.season.season_type == "byot":
            if self.is_playoff:
                teams = [i["team"] for i in self.season.team_results()]
                if self.season.playoff_teams:
                    teams = teams[:self.season.playoff_teams]
            else:
                teams = self.season.byoteam_set.order_by("?").all()

            for team in teams:
                if teams_to_include and team.id not in teams_to_include:
                    continue

                team.create_team_for_event(self)

            self.generate_matches()
            return

        teams = [Team(event=self, name="Team #{}".format(i+1)) for i in range(num_teams)]
        Team.objects.bulk_create(teams)

        if self.unassigned_players.count() > 0:
            players = self.unassigned_players.all()

            player_points = {i["player_id"]: i["points"] for i in self.season.player_results()}
            player_ratings = {i.id: i.rating().mu for i in players}

            season_pct_complete = self.event_number() / self.season.event_set.count()

            if player_points:
                points_weight = season_pct_complete * (max(player_points.values()) - min(player_points.values()))
                rating_weight = (1 - season_pct_complete) * (max(player_ratings.values()) - min(player_ratings.values()))
            else:
                points_weight = 0
                rating_weight = 1

            rating_sort = lambda i: (player_points.get(i.id, 0) * points_weight + player_ratings.get(i.id, 0) * rating_weight)

            ranked_players = sorted(players, key=rating_sort, reverse=True)

            for player, team in zip(ranked_players, itertools.cycle(teams + teams[::-1])):
                team.players.add(player)

            self.unassigned_players.clear()

        self.generate_matches()

    @classmethod
    def get_games_since_team_played(cls, team, schedule):
        # Get the number of games since this team has played
        for i, match in enumerate(schedule[::-1]):
            if team in match:
                return i

        return len(schedule)

    @classmethod
    def get_match_sort_value(cls, match, schedule):
        return sum([cls.get_games_since_team_played(i, schedule) for i in match])

    @classmethod
    def get_rr_schedule_by_team_count(cls, num_teams):
        matches = list(itertools.combinations(range(num_teams), 2))
        schedule = []
        while len(matches) > 0:
            # Find the game with teams that have played the least recently
            matches = sorted(matches, key=sum)
            matches = sorted(matches, key=lambda i: cls.get_match_sort_value(i, schedule), reverse=True)
            match = matches.pop(0)

            # Give each team an equal number of games on gold and blue (or as close as possible)
            # by reversing games where the sum of the team IDs is even
            if sum(match) % 2 == 0:
                match = match[::-1]

            schedule.append(match)

        return schedule

    def generate_matches(self):
        teams = [i for i in self.team_set.order_by("id").all()]

        if self.is_playoff and self.season.playoff_type == "double_elim":
            BYE = Team(name="BYE")

            # Pad out to a power of two
            for i in range(2**math.ceil(math.log2(len(teams))) - len(teams)):
                teams.append(BYE)

            # Winners Bracket
            wb_rounds = self.single_elim_rounds(teams)
            for wb_round in wb_rounds:
                for match in wb_round:
                    match.round_name = "Winners Bracket " + match.round_name
                    match.loser_place = None

            # Losers Bracket
            lb_rounds = self.losers_bracket_rounds(teams, wb_rounds)

            # Finals
            finals_first_match = Match(event=self, round_name="Finals (First Match)", winner_place=1, loser_place=2)
            finals_first_match.match_type = Match.MatchType.DOUBLE_ELIM_FIRST_FINAL
            wb_rounds[-1][0].winner_next_match = finals_first_match
            lb_rounds[-1][0].winner_next_match = finals_first_match

            finals_second_match = Match(event=self, round_name="Finals (Second Match)", winner_place=1, loser_place=2)
            finals_first_match.winner_next_match = finals_second_match
            finals_first_match.loser_next_match = finals_second_match

            # Do inserts
            all_matches = []
            queued = [i for i in wb_rounds[0]]

            while len(queued) > 0:
                match = queued.pop(0)
                if match.id:
                    continue

                winner = None
                if match.blue_team == BYE:
                    winner = match.gold_team
                if match.gold_team == BYE:
                    winner = match.blue_team

                if BYE in (match.blue_team, match.gold_team):
                    logger.debug("Skipping match due to bye: {}".format(match))

                    if winner is None and match.winner_next_match:
                        for other_match in all_matches:
                            if other_match.winner_next_match == match:
                                other_match.winner_next_match = match.winner_next_match
                            if other_match.loser_next_match == match:
                                other_match.loser_next_match = match.winner_next_match

                    if winner and match.winner_next_match:
                        match.winner_next_match.assign_team(winner)
                        logger.debug("Assigning {} to match: {}".format(winner.name, match.winner_next_match))

                    if winner and match.loser_next_match:
                        match.loser_next_match.assign_team(BYE)
                        logger.debug("Assigning {} to match: {}".format(BYE.name, match.loser_next_match))

                else:
                    all_matches.append(match)
                    logger.debug("Appending match: {}".format(match))

                for next_match in filter(lambda i: i is not None, (match.winner_next_match, match.loser_next_match)):
                    next_match.prev_games_done = getattr(next_match, "prev_games_done", 0) + 1
                    if next_match.prev_games_done == 2:
                        logger.debug("- Queueing match: {}".format(next_match))
                        queued.append(next_match)

            Match.objects.bulk_create(all_matches)
            for match in all_matches:
                if match.winner_next_match:
                    match.winner_next_match_id = match.winner_next_match.id
                if match.loser_next_match:
                    match.loser_next_match_id = match.loser_next_match.id
                match.save()

            return all_matches

        schedule = self.get_rr_schedule_by_team_count(len(teams))
        round_num = 0
        for i in range(self.matches_per_opponent):
            for matchup in schedule:
                round_num += 1
                Match.objects.create(
                    event=self,
                    blue_team=teams[matchup[(0+i) % 2]],
                    gold_team=teams[matchup[(1+i) % 2]],
                    round_name="Round {} of {}".format(round_num, len(schedule) * self.matches_per_opponent),
                )

    def single_elim_rounds(self, teams):
        num_rounds = math.ceil(math.log2(len(teams)))
        rounds = []

        for round_num in range(num_rounds):
            round_name = self.get_round_name(round_num, num_rounds)
            num_matches = 2**(num_rounds - round_num - 1)
            round_matches = [Match(event=self, round_name=round_name, loser_place=num_matches+1) for i in range(num_matches)]
            rounds.append(round_matches)

        for current_round, next_round in zip(rounds, rounds[1:]):
            for i, match in enumerate(current_round):
                match.winner_next_match = next_round[math.floor(i/2)]

        team_order = [0]
        while len(team_order) < len(rounds[0]):
            old_order = list(team_order)
            for i, team_idx in enumerate(old_order):
                team_order.insert(i*2+1, 2*len(old_order) - 1 - team_idx)

        for i, match in enumerate(rounds[0]):
            blue_idx = team_order[i]
            gold_idx = 2**num_rounds - blue_idx - 1
            if gold_idx < len(teams):
                match.blue_team = teams[blue_idx]
                match.gold_team = teams[gold_idx]
            else:
                if match.winner_next_match.blue_team is None:
                    match.winner_next_match.blue_team = teams[blue_idx]
                else:
                    match.winner_next_match.gold_team = teams[blue_idx]

        rounds[0] = [i for i in rounds[0]]
        return rounds

    def losers_bracket_rounds(self, teams, wb_rounds):
        num_rounds = 2 * (math.ceil(math.log2(len(teams))) - 1)
        rounds = []

        for round_num in range(num_rounds):
            round_name = "Losers Bracket " + self.get_round_name(round_num, num_rounds)
            num_matches = 2**math.floor((num_rounds - round_num - 1) / 2)
            round_matches = [Match(event=self, round_name=round_name) for i in range(num_matches)]
            rounds.append(round_matches)

        for current_round, next_round in zip(rounds, rounds[1:]):
            if len(current_round) == len(next_round):
                for match, next_match in zip(current_round, next_round):
                    match.winner_next_match = next_match

                wb_round_num = int(len(wb_rounds) - math.log2(len(current_round)) - 1)
                wb_round = wb_rounds[wb_round_num]

                # TODO: Fix order here if we ever need more than 8 teams
                for match, next_match in zip(wb_round[::-1], next_round):
                    match.loser_next_match = next_match

            else:
                for i, match in enumerate(current_round):
                    match.winner_next_match = next_round[math.floor(i/2)]

        # First round of winners bracket into first round of losers bracket
        for i, winner_match in enumerate(wb_rounds[0]):
            winner_match.loser_next_match = rounds[0][math.floor(i/2)]

        wb_rounds[-1][0].loser_next_match = rounds[-1][0]

        place = 3
        for current_round in rounds[::-1]:
            for match in current_round:
                match.loser_place = place

            place += len(current_round)

        return rounds

    @classmethod
    def get_round_name(cls, round_num, num_rounds):
        round_names = [
            "Finals",
            "Semifinals",
            "Quarterfinals",
        ]

        try:
            return round_names[num_rounds - round_num - 1]
        except IndexError:
            return "Round {}".format(round_num+1)

    def playoff_rounds(self):
        if self.season.playoff_type == Season.PlayoffType.DOUBLE_ELIM:
            matches = self.match_set.all()
            rounds = {i.round_name for i in matches}
            matches_by_round = {
                round_num: list(filter(lambda i: i.round_name == round_num, matches))
                for round_num in rounds
            }

            first_round = matches_by_round[matches[0].round_name]
            wb_rounds = [first_round]
            lb_rounds = []

            wb_queue = list({i.winner_next_match.round_name for i in first_round})
            lb_queue = list({i.loser_next_match.round_name for i in first_round})

            while len(wb_queue) > 0:
                wb_next = matches_by_round[wb_queue.pop(0)]
                wb_rounds.append(wb_next)

                for match in wb_next:
                    if match.winner_next_match and match.winner_next_match.round_name not in wb_queue:
                        wb_queue.append(match.winner_next_match.round_name)

            while len(lb_queue) > 0:
                lb_next = matches_by_round[lb_queue.pop(0)]
                lb_rounds.append(lb_next)

                for match in lb_next:
                    if match.winner_next_match and match.winner_next_match.round_name not in lb_queue and \
                       match.winner_next_match.match_type != Match.MatchType.DOUBLE_ELIM_FIRST_FINAL:
                        lb_queue.append(match.winner_next_match.round_name)

            return wb_rounds, lb_rounds


    def generate_qp_match(self):
        total_player_rounds = self.unassigned_players.count() * int(self.qp_matches_per_player)
        total_remaining_player_rounds = total_player_rounds
        for qpmatch in self.qpmatch_set.all():
            total_remaining_player_rounds -= qpmatch.qpplayer_set.count()

        if total_remaining_player_rounds <= 0:
            self.is_complete = True
            self.is_active = False
            self.unassigned_players.clear()
            self.save()

            return

        previous_teams = set()
        for qpmatch in self.qpmatch_set.all():
            previous_teams.add(frozenset([i.player_id for i in qpmatch.qpplayer_set.filter(team="blue")]))
            previous_teams.add(frozenset([i.player_id for i in qpmatch.qpplayer_set.filter(team="gold")]))

        if total_remaining_player_rounds % PLAYERS_PER_MATCH > 0:
            player_count = PLAYERS_PER_MATCH - 2
        else:
            player_count = PLAYERS_PER_MATCH

        matches_by_player = {i.id: 0 for i in self.unassigned_players.all()}
        for qp_match in self.qpmatch_set.all():
            for qp_player in qp_match.qpplayer_set.all():
                matches_by_player[qp_player.player.id] += 1

        players = []
        while len(players) < player_count:
            added_players = [k for k, v in matches_by_player.items() if v == min(matches_by_player.values())]
            if len(players) + len(added_players) > player_count:
                added_players = random.sample(added_players, player_count - len(players))

            players.extend(added_players)
            for player in added_players:
                del matches_by_player[player]

        player_ratings = {i: Player.objects.get(id=i).rating() for i in players}
        picked_team = None
        best_quality = 0

        for blue_players in itertools.combinations(players, int(player_count / 2)):
            gold_players = [i for i in players if i not in blue_players]

            if frozenset(blue_players) in previous_teams:
                continue
            if frozenset(gold_players) in previous_teams:
                continue

            blue_ratings = [player_ratings[i] for i in blue_players]
            gold_ratings = [player_ratings[i] for i in gold_players]

            match_quality = trueskill_env.quality((blue_ratings, gold_ratings))
            if match_quality > best_quality:
                picked_team = blue_players
                best_quality = match_quality

        qp_match = QPMatch()
        qp_match.event = self
        qp_match.save()

        for player_id in players:
            qp_player = QPPlayer()
            qp_player.player_id = player_id
            qp_player.qp_match = qp_match
            qp_player.team = "blue" if player_id in picked_team else "gold"
            qp_player.save()

        return qp_match

    def qp_remaining_matches(self):
        total_player_rounds = self.unassigned_players.count() * int(self.qp_matches_per_player)
        total_remaining_player_rounds = total_player_rounds
        for qpmatch in self.qpmatch_set.all():
            total_remaining_player_rounds -= qpmatch.qpplayer_set.count()

        return math.ceil(total_remaining_player_rounds / PLAYERS_PER_MATCH)

    def current_match(self):
        return self.match_set.filter(is_complete=False).first()

    def current_qp_match(self):
        return self.qpmatch_set.filter(is_complete=False).order_by("id").first()

    def publish_match_state(self):
        current_match = None

        if self.is_active:
            if self.is_quickplay:
                qp_match = self.current_qp_match()
                if qp_match:
                    round_num = self.qpmatch_set.count()
                    round_name = "Round {} of {}".format(round_num, round_num + self.qp_remaining_matches())
                    current_match = {
                        "id": qp_match.id,
                        "blue_team": "Blue Team",
                        "blue_score": qp_match.blue_score,
                        "gold_team": "Gold Team",
                        "gold_score": qp_match.gold_score,
                        "is_quickplay": True,
                        "rounds_per_match": self.rounds_per_match,
                        "wins_per_match": self.wins_per_match,
                        "round_name": " - ".join(filter(lambda i: i, [
                            self.season.name,
                            self.get_name(),
                            round_name,
                        ])),
                    }

            else:
                match = self.current_match()
                if match:
                    current_match = {
                        "id": match.id,
                        "blue_team": match.blue_team.name,
                        "blue_team_id": match.blue_team.id,
                        "blue_score": match.blue_score,
                        "gold_team": match.gold_team.name,
                        "gold_team_id": match.gold_team.id,
                        "gold_score": match.gold_score,
                        "is_quickplay": False,
                        "rounds_per_match": self.rounds_per_match,
                        "wins_per_match": self.wins_per_match,
                        "round_name": " - ".join(filter(lambda i: i, [
                            self.season.name,
                            self.get_name(),
                            match.round_name,
                        ])),
                    }

        redis_client.publish("matchstate", json.dumps({
            "type": "match",
            "match_type": "league",
            "current_match": current_match,
            "cabinet_id": self.cabinet.id if self.cabinet else None,
        }))

        return current_match


class BYOTeam(BaseModel):
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    players = models.ManyToManyField(Player)

    def results(self):
        if not hasattr(self, "_results"):
            totals = {"team": self, "rounds_won": 0, "rounds_lost": 0, "matches_won": 0, "matches_lost": 0, "points": 0}
            for team in self.team_set.filter(event__is_playoff=False).all():
                week_results = team.results()
                for k in ["rounds_won", "rounds_lost", "matches_won", "matches_lost", "points"]:
                    totals[k] += week_results[k]

            self._results = totals

        return self._results

    def create_team_for_event(self, event):
        team = Team(event=event, name=self.name, byoteam=self)
        team.save()

        for player in self.players.all():
            team.players.add(player)

        return team


class SeasonPlace(BaseModel):
    season = models.ForeignKey(Season, on_delete=models.CASCADE)
    team = models.ForeignKey(BYOTeam, on_delete=models.CASCADE)
    place = models.IntegerField(default=0)

    class Meta:
        unique_together = ("season", "place")


class Team(BaseModel):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    name = models.CharField(max_length=200)
    players = models.ManyToManyField(Player)
    byoteam = models.ForeignKey(BYOTeam, null=True, on_delete=models.SET_NULL)

    def results(self):
        results = {"team": self, "rounds_won": 0, "rounds_lost": 0, "matches_won": 0, "matches_lost": 0}
        for match in self.matches():
            scores = match.score_by_team_id()
            results["rounds_won"] += scores[self.id]
            results["rounds_lost"] += sum([v for k, v in scores.items() if k != self.id])
            if match.is_complete:
                if scores[self.id] == max(scores.values()):
                    results["matches_won"] += 1
                else:
                    results["matches_lost"] += 1

        results["points"] = self.event.points_per_round * results["rounds_won"] + \
                            self.event.points_per_match * results["matches_won"]

        match_count_base = self.event.season.match_count_base
        if match_count_base and len(self.matches()) > 0:
            results["points"] *= (match_count_base) / (len(self.matches()))

        return results

    def matches(self):
        return Match.objects.filter(blue_team_id=self.id) | Match.objects.filter(gold_team_id=self.id)


class Match(BaseModel):
    class MatchType(models.TextChoices):
        STANDARD = "standard", "Standard"
        DOUBLE_ELIM_FIRST_FINAL = "double_elim_first_final", "Double Elimination Finals First Round"

    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    blue_team = models.ForeignKey(Team, null=True, on_delete=models.CASCADE, related_name="+")
    gold_team = models.ForeignKey(Team, null=True, on_delete=models.CASCADE, related_name="+")
    blue_score = models.IntegerField(default=0)
    gold_score = models.IntegerField(default=0)
    is_complete = models.BooleanField(default=False)
    round_name = models.CharField(max_length=50, null=True)
    match_type = models.CharField(max_length=30, choices=MatchType.choices, default=MatchType.STANDARD)
    winner_next_match = models.ForeignKey("self", null=True, on_delete=models.SET_NULL, related_name="+")
    loser_next_match = models.ForeignKey("self", null=True, on_delete=models.SET_NULL, related_name="+")
    winner_place = models.IntegerField(null=True)
    loser_place = models.IntegerField(null=True)
    match_order = models.IntegerField(null=True)

    class Meta:
        unique_together = ("event", "match_order")
        ordering = ["match_order", "id"]

    def match_number(self):
        return self.event.match_set.filter(id__lte=self.id).count()

    def score_by_team_id(self):
        return {
            self.blue_team_id: self.blue_score,
            self.gold_team_id: self.gold_score,
        }

    def winning_team(self):
        if self.is_complete and self.blue_score > self.gold_score:
            return self.blue_team
        if self.is_complete and self.gold_score > self.blue_score:
            return self.gold_team

        return None

    def losing_team(self):
        if self.is_complete and self.blue_score > self.gold_score:
            return self.gold_team
        if self.is_complete and self.gold_score > self.blue_score:
            return self.blue_team

        return None

    def blue_team_name(self):
        if self.blue_team:
            return self.blue_team.name

        if self.event.is_quickplay:
            return "Blue Team"

        return "TBD"

    def gold_team_name(self):
        if self.gold_team:
            return self.gold_team.name

        if self.event.is_quickplay:
            return "Gold Team"

        return "TBD"

    def assign_team(self, team):
        if self.blue_team is None:
            self.blue_team = team
        else:
            self.gold_team = team

    def rate_players(self):
        blue_players = self.blue_team.players.all()
        gold_players = self.gold_team.players.all()
        blue_ratings = {player: player.rating() for player in blue_players}
        gold_ratings = {player: player.rating() for player in gold_players}

        if len(blue_players) == 0 or len(gold_players) == 0:
            return

        ranks = [0 if self.blue_score > self.gold_score else 1, 0 if self.gold_score > self.blue_score else 1]
        rating_groups = trueskill_env.rate([blue_ratings, gold_ratings], ranks=ranks)
        for group in rating_groups:
            for player, rating in group.items():
                PlayerRating(player=player, match=self, mu=rating.mu, sigma=rating.sigma).save()

    def round_count_reached(self):
        if self.event.rounds_per_match and self.blue_score + self.gold_score >= self.event.rounds_per_match:
            return True

        if self.event.wins_per_match and max(self.blue_score, self.gold_score) >= self.event.wins_per_match:
            return True

        return False

    def set_complete(self):
        self.is_complete = True
        self.save()

        self.rate_players()

        if self.match_type == self.MatchType.DOUBLE_ELIM_FIRST_FINAL:
            results = self.event.team_results()
            second_match_needed = False
            for result in results:
                if result["team"] == self.losing_team():
                    second_match_needed = (result["matches_lost"] < 2)
                    break

            if second_match_needed:
                self.winner_next_match.blue_team = self.gold_team
                self.winner_next_match.gold_team = self.blue_team
                self.winner_next_match.save()
            else:
                next_match = self.winner_next_match
                self.winner_next_match = None
                self.loser_next_match = None
                self.save()
                next_match.delete()

                SeasonPlace(season=self.event.season, team=self.winning_team().byoteam, place=1).save()
                SeasonPlace(season=self.event.season, team=self.losing_team().byoteam, place=2).save()

        else:
            if self.winner_next_match:
                if self.winner_next_match.blue_team is None:
                    self.winner_next_match.blue_team = self.winning_team()
                else:
                    self.winner_next_match.gold_team = self.winning_team()

                self.winner_next_match.save()

            if self.loser_next_match:
                if self.loser_next_match.blue_team is None:
                    self.loser_next_match.blue_team = self.losing_team()
                else:
                    self.loser_next_match.gold_team = self.losing_team()

                self.loser_next_match.save()

            if self.winner_place:
                SeasonPlace(season=self.event.season, team=self.winning_team().byoteam, place=self.winner_place).save()
            if self.loser_place:
                SeasonPlace(season=self.event.season, team=self.losing_team().byoteam, place=self.loser_place).save()

        if self.event.match_set.filter(is_complete=False).count() == 0:
            self.event.is_active = False
            self.event.is_complete = True
            self.event.save()

    def __str__(self):
        return "{:30} vs. {:30} {}".format(
            self.blue_team.name if self.blue_team else "TBD",
            self.gold_team.name if self.gold_team else "TBD",
            " - " + self.round_name if self.round_name else "",
        )


class QPMatch(BaseModel):
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    blue_score = models.IntegerField(default=0)
    gold_score = models.IntegerField(default=0)
    is_complete = models.BooleanField(default=False)

    def match_number(self):
        return self.event.qpmatch_set.filter(id__lte=self.id).count()

    def rate_players(self):
        blue_players = [i.player for i in self.qpplayer_set.filter(team="blue")]
        gold_players = [i.player for i in self.qpplayer_set.filter(team="gold")]
        blue_ratings = {player: player.rating() for player in blue_players}
        gold_ratings = {player: player.rating() for player in gold_players}

        if len(blue_players) == 0 or len(gold_players) == 0:
            return

        ranks = [0 if self.blue_score > self.gold_score else 1, 0 if self.gold_score > self.blue_score else 1]
        rating_groups = trueskill_env.rate([blue_ratings, gold_ratings], ranks=ranks)
        for group in rating_groups:
            for player, rating in group.items():
                PlayerRating(player=player, qp_match=self, mu=rating.mu, sigma=rating.sigma).save()

    def round_count_reached(self):
        if self.event.rounds_per_match and self.blue_score + self.gold_score >= self.event.rounds_per_match:
            return True

        if self.event.wins_per_match and max(self.blue_score + self.gold_score) >= self.event.wins_per_match:
            return True

        return False

    def set_complete(self):
        self.is_complete = True
        self.save()

        self.rate_players()

        self.event.generate_qp_match()


class QPPlayer(BaseModel):
    qp_match = models.ForeignKey(QPMatch, on_delete=models.CASCADE)
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    team = models.CharField(max_length=5, null=True, choices=CabinetTeam.choices)


class PlayerRating(BaseModel):
    player = models.ForeignKey(Player, on_delete=models.CASCADE)
    match = models.ForeignKey(Match, null=True, on_delete=models.CASCADE)
    qp_match = models.ForeignKey(QPMatch, null=True, on_delete=models.CASCADE)
    rating_date = models.DateTimeField(default=datetime.now)
    mu = models.FloatField(default=0)
    sigma = models.FloatField(default=0)

    def rating(self):
        return trueskill_env.create_rating(mu=self.mu, sigma=self.sigma)

    @classmethod
    @transaction.atomic
    def recalculate_all(cls):
        cls.objects.all().delete()

        for event in Event.objects.order_by("event_date").all():
            for match in event.match_set.filter(is_complete=True):
                match.rate_players()
            for qp_match in event.qpmatch_set.filter(is_complete=True).order_by("id").all():
                qp_match.rate_players()

    def get_event(self):
        if self.match is not None:
            return self.match.event
        if self.qp_match is not None:
            return self.qp_match.event

    def get_team(self):
        if self.match is not None:
            if self.match.blue_team.players.filter(id=self.player.id).count() > 0:
                return "blue"
            if self.match.gold_team.players.filter(id=self.player.id).count() > 0:
                return "gold"

        if self.qp_match is not None:
            return self.qp_match.qpplayer_set.filter(player_id=self.player.id).first().team
