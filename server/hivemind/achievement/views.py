from ..model import BaseViewSet
from .models import UserAchievement
from .permissions import UserAchievementPermission
from .serializers import UserAchievementSerializer


class UserAchievementViewSet(BaseViewSet):
    queryset = UserAchievement.objects.all()
    serializer_class = UserAchievementSerializer
    permission_classes = [UserAchievementPermission]
    filterset_fields = ["user_id", "game_id"]
