from rest_framework.permissions import SAFE_METHODS, BasePermission


class UserAchievementPermission(BasePermission):
    def has_object_permission(self, request, view, obj):
        if obj and obj.user and request.user and obj.user == request.user:
            return True

        return obj.user and obj.user.is_profile_public and request.method in SAFE_METHODS
