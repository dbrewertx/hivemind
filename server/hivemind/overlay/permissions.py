from ..game.models import Cabinet
from ..permissions import SceneAdminOrReadOnly
from .models import Overlay


class OverlayPermission(SceneAdminOrReadOnly):
    def get_scene_from_object(self, obj):
        return obj.cabinet.scene


    def get_scene_id_from_request(self, request):
        try:
            cabinet = Cabinet.objects.get(id=request.data.get("cabinet"))
            return cabinet.scene.id
        except Season.DoesNotExist:
            raise BadRequest("Invalid season")
