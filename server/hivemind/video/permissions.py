from django.core.exceptions import BadRequest
from rest_framework.permissions import SAFE_METHODS, BasePermission

from ..constants import HTTPMethod
from ..game.models import Cabinet


class VideoClipPermission(BasePermission):
    def has_permission(self, request, view):
        if request.method in SAFE_METHODS:
            return True

        if request.method in [HTTPMethod.POST, HTTPMethod.PUT] and request.user.is_authenticated:
            return True

        if request.method == HTTPMethod.POST and request.data.get("token"):
            try:
                cabinet = Cabinet.objects.get(id=request.data.get("cabinet"))
            except Cabinet.DoesNotExist:
                raise BadRequest("Invalid cabinet")

            if cabinet.token == request.data.get("token"):
                return True

        return False
