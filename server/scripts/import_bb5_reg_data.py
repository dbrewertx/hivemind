#!/usr/bin/env python3
import csv
import re
from urllib.parse import urlparse

import requests
from django.core.files.base import ContentFile
from django.db import transaction
from django.utils.text import slugify
from hivemind.tournament.models import (Tournament, TournamentBracket,
                                        TournamentMatch, TournamentPlayer,
                                        TournamentTeam)


def run():
    tournament = Tournament.objects.get(name="Baltimore Brawl 5")
    TournamentTeam.objects.filter(tournament=tournament).delete()

    with open("/src/bb5_reg_data.csv", "r") as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            reg_id, name, pronouns, pic_link, shirt_size, team_name, scene, events, tidbit = row[:9]
            if not reg_id.startswith("#"):
                continue

            team, created = TournamentTeam.objects.get_or_create(tournament=tournament, name=team_name)
            team.save()

            player = TournamentPlayer.objects.create(
                name=name,
                team=team,
                scene=re.sub("^.*\[(.+)\].*$", "\\1", scene),
                pronouns=pronouns,
                tidbit=tidbit,
            )

            if pic_link:
                filename = "{}-{}.jpg".format(player.id, slugify(player.name))
                response = requests.get(pic_link)
                player.image.save(filename, ContentFile(response.content), save=True)

            print(player)
