import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { CircularProgress } from '@material-ui/core';
import { format } from 'date-fns';

import { getAxios } from 'util/axios';
import { formatPercent } from 'util/formatters';
import {
  Recap,
  RecapItem,
  RecapMainItem,
  RecapSubItem,
  RecapList,
  RecapListItem,
} from 'components/Recap';
import SceneColorBox from 'components/SceneColorBox';

export default function RecapPage() {
  const axios = getAxios();
  const [recapResponse, setRecapResponse] = useState(null);

  useEffect(() => {
    if (recapResponse === null) {
      axios.get(`/api/stats/recap/2022/`).then(response => {
        setRecapResponse(response.data);
      });
    }
  });

  if (recapResponse === null) {
    return <CircularProgress />;
  }

  if (!recapResponse.success) {
    return <>Could not retrieve recap: {recapResponse.error}</>;
  }

  const recapData = recapResponse.data;

  return (
    <Recap title="HiveMind 2022 Recap">
      <RecapItem>
        <RecapMainItem>
          <b>{recapData.games.total.toLocaleString()}</b> total games were recorded
        </RecapMainItem>

        <RecapSubItem>
          That's over <b>{recapData.games.gameTimeDays.toLocaleString()}</b> days of game time.
        </RecapSubItem>

        <RecapSubItem>
          <b>{recapData.games.scenes}</b> scenes logged games on <b>{recapData.games.cabinets}</b>{' '}
          cabinets.
        </RecapSubItem>

        <RecapSubItem>
          The busiest month was <b>{recapData.games.topMonth.month}</b>, with{' '}
          <b>{recapData.games.topMonth.count.toLocaleString()}</b> games recorded.
        </RecapSubItem>

        <RecapSubItem>
          <b>{recapData.games.topMap.count.toLocaleString()}</b> games were played on the most
          popular map, <b>{recapData.games.topMap.name}</b>.
        </RecapSubItem>

        <RecapList title="Most Games by Scene">
          {recapData.games.topScenes.map(scene => (
            <RecapListItem
              key={scene.scene}
              name={
                <>
                  <SceneColorBox color={scene.backgroundColor} /> {scene.scene}
                </>
              }
              value={scene.games}
            />
          ))}
        </RecapList>
      </RecapItem>

      <RecapItem>
        <RecapMainItem>
          Users logged in for <b>{recapData.loggedIn.games.toLocaleString()}</b> games
        </RecapMainItem>

        <RecapSubItem>
          <b>{recapData.loggedIn.users.toLocaleString()}</b> different users logged in for at least
          one game.
        </RecapSubItem>

        <RecapSubItem>
          <b>{recapData.loggedIn.scenes.toLocaleString()}</b> different scenes had players sign in.
        </RecapSubItem>

        <RecapSubItem>
          Players registered <b>{recapData.loggedIn.nfcTags.toLocaleString()}</b> new NFC tags.
        </RecapSubItem>

        <RecapSubItem>
          Queens accounted for <b>{formatPercent(recapData.loggedIn.queenPct)}</b> of signed-in
          games.
        </RecapSubItem>
      </RecapItem>

      <RecapItem>
        <RecapMainItem>
          <b>{recapData.tournaments.tournaments}</b> tournaments were managed using HiveMind
        </RecapMainItem>

        <RecapSubItem>
          Tournaments accounted for <b>{recapData.tournaments.games.toLocaleString()}</b> of this
          year's games.
        </RecapSubItem>

        <RecapSubItem>
          <b>{recapData.tournaments.scenes}</b> scenes hosted at least one tournament.
        </RecapSubItem>

        <RecapSubItem>
          <b>{formatPercent(recapData.tournaments.signInPct)}</b> of players were signed in for
          their tournament games.
        </RecapSubItem>

        <RecapList title="Most Games by Tournament">
          {recapData.tournaments.topTournaments.map(tournament => (
            <RecapListItem
              key={tournament.name}
              name={
                <>
                  <SceneColorBox color={tournament.backgroundColor} /> {tournament.name}
                </>
              }
              value={tournament.games}
            />
          ))}
        </RecapList>
      </RecapItem>

      <RecapItem>
        <RecapMainItem>
          <b>{recapData.newScenes.length} scenes</b> recorded their first game
        </RecapMainItem>
        <RecapList style={{ maxWidth: 'unset' }}>
          {recapData.newScenes.map(scene => (
            <RecapListItem
              key={scene.id}
              style={{ width: '50%' }}
              name={
                <>
                  <SceneColorBox color={scene.backgroundColor} /> {scene.name}
                </>
              }
              value={format(new Date(scene.firstGame), 'MMM d')}
            />
          ))}
        </RecapList>
      </RecapItem>

      <RecapItem>
        <RecapMainItem>Twilight is Live</RecapMainItem>

        <RecapSubItem>
          <b>
            {recapData.byMap
              .filter(i => i.id === 27 || i.betaOf === 27)
              .reduce((a, b) => a + b.games, 0)
              .toLocaleString()}
          </b>{' '}
          games were played across{' '}
          <b>{recapData.byMap.filter(i => i.id === 27 || i.betaOf === 27).length}</b> different
          versions of Twilight.
        </RecapSubItem>

        <RecapSubItem>
          Out of these, <b>{recapData.byMap.filter(i => i.id === 27)[0].games.toLocaleString()}</b>{' '}
          were played on the final release version.
        </RecapSubItem>
      </RecapItem>

      <RecapItem>
        <RecapMainItem>Thanks to the Patrons</RecapMainItem>

        <RecapSubItem>
          Subscribers on Patreon contributed <b>$1,217.59</b> to cover server costs.
        </RecapSubItem>

        <RecapSubItem>
          You can join these <b>22</b> awesome patrons at{' '}
          <a href="https://www.patreon.com/kqhivemind">patreon.com/kqhivemind</a>.
        </RecapSubItem>

        <RecapSubItem>
          An additional <b>$411.83</b> was received from one-time donations on{' '}
          <a href="https://account.venmo.com/u/KQHiveMind">Venmo</a>.
        </RecapSubItem>

        <RecapSubItem>
          Your donations allowed HiveMind to add extra server infrastructure, with monthly server
          costs increasing from <b>$27.50</b> in January to <b>$88.50</b> in December.
        </RecapSubItem>

        <RecapSubItem>
          <b>1</b> round of shots has been purchased with the HiveMind Software LLC debit card.
        </RecapSubItem>
      </RecapItem>
    </Recap>
  );
}
