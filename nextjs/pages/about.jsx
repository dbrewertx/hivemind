import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid, Paper, Link } from '@material-ui/core';
import Carousel from 'react-material-ui-carousel';

const useStyles = makeStyles(theme => ({
  carouselContainer: {
    margin: theme.spacing(2, 0),
    height: '800px',
  },
  carouselItem: {
    padding: theme.spacing(2),
    textAlign: 'center',
    '& img': {
      maxHeight: '600px',
      margin: '0 auto',
      objectFit: 'contain',
    },
  },
  carouselImageContainer: {
    height: '620px',
    flexBasis: '620px',
    flexGrow: 0,
  },
  textContainer: {
    margin: theme.spacing(2, 0),
    textAlign: 'center',
  },
}));

export default function About() {
  const classes = useStyles();
  return (
    <>
      <Typography variant="h1">HiveMind: Stats for Killer Queen</Typography>
      <Grid container spacing={4}>
        <Grid item container xs={12} direction="column" className={classes.carouselContainer}>
          <Carousel interval={15000} navButtonsAlwaysVisible={true}>
            <Paper className={classes.carouselItem}>
              <Grid item>
                <Typography variant="h2">All your stats from all your games</Typography>
              </Grid>
              <Grid item className={classes.carouselImageContainer}>
                <img src="/static/screenshots/stats.png" alt="Screenshot with example of game statistics" />
              </Grid>
              <Grid item>
                <Typography>Your game stats are sent to HiveMind in real time, and are always available from your phone or computer.</Typography>
              </Grid>
            </Paper>

            <Paper className={classes.carouselItem}>
              <Grid item>
                <Typography variant="h2">All your stats from all your games</Typography>
              </Grid>
              <Grid item className={classes.carouselImageContainer}>
                <img src="/static/screenshots/charts.png" alt="Screenshot with example of charts" />
              </Grid>
              <Grid item>
                <Typography>Unique charts show your team's progress towards victory throughout the match.</Typography>
              </Grid>
            </Paper>

            <Paper className={classes.carouselItem}>
              <Grid item>
                <Typography variant="h2">All your stats from all your games</Typography>
              </Grid>
              <Grid item className={classes.carouselImageContainer}>
                <img src="/static/screenshots/recent_games.png" alt="Screenshot of recent games feature" />
              </Grid>
              <Grid item>
                <Typography>With the list of recent games, you can find out if anyone's playing right now.</Typography>
              </Grid>
            </Paper>

            <Paper className={classes.carouselItem}>
              <Grid item>
                <Typography variant="h2">League Nights made easy</Typography>
              </Grid>
              <Grid item className={classes.carouselImageContainer}>
                <img src="/static/screenshots/standings.png" alt="Screenshot with example of league standings" />
              </Grid>
              <Grid item>
                <Typography>HiveMind tracks your league standings and results automatically.</Typography>
              </Grid>
            </Paper>

            <Paper className={classes.carouselItem}>
              <Grid item>
                <Typography variant="h2">League Nights made easy</Typography>
              </Grid>
              <Grid item className={classes.carouselImageContainer}>
                <img src="/static/screenshots/quickplay.png" alt="Screenshot with example of shuffle mode" />
              </Grid>
              <Grid item>
                <Typography>HiveMind will automatically assign players to a new team each week or each match.</Typography>
              </Grid>
            </Paper>

            <Paper className={classes.carouselItem}>
              <Grid item>
                <Typography variant="h2">League Nights made easy</Typography>
              </Grid>
              <Grid item className={classes.carouselImageContainer}>
                <img src="/static/screenshots/byot.png" alt="Screenshot with example of premade teams" />
              </Grid>
              <Grid item>
                <Typography>If you prefer to use premade teams, you can track standings by team, and assign free agents to replace missing players.</Typography>
              </Grid>
            </Paper>

            <Paper className={classes.carouselItem}>
              <Grid item>
                <Typography variant="h2">Tournament tracking</Typography>
              </Grid>
              <Grid item className={classes.carouselImageContainer}>
                <img src="/static/screenshots/tournament_events.png" alt="Screenshot of tournament match" />
              </Grid>
              <Grid item>
                <Typography>Integrates with <Link href="https://challonge.com/">Challonge</Link> to automatically update your tournament bracket.</Typography>
              </Grid>
            </Paper>

            <Paper className={classes.carouselItem}>
              <Grid item>
                <Typography variant="h2">Stream overlays</Typography>
              </Grid>
              <Grid item className={classes.carouselImageContainer}>
                <img src="/static/screenshots/overlay_ingame.png" alt="Photo of ingame overlay on projector" />
              </Grid>
              <Grid item>
                <Typography>In-game overlay for your stream shows live stats, and automatically updates team names during league nights and tournaments.</Typography>
              </Grid>
            </Paper>

            <Paper className={classes.carouselItem}>
              <Grid item>
                <Typography variant="h2">Stream overlays</Typography>
              </Grid>
              <Grid item className={classes.carouselImageContainer}>
                <img src="/static/screenshots/overlay_postgame.png" alt="Screenshot of postgame overlay" />
              </Grid>
              <Grid item>
                <Typography>After the game ends, switch to the post-game overlay to show victory progress charts.</Typography>
              </Grid>
            </Paper>
          </Carousel>
        </Grid>

        <Grid item container xs={12} direction="column" className={classes.textContainer}>
          <Typography>HiveMind is a free open-source stat tracker for <Link href="https://killerqueenarcade.com/">Killer Queen Arcade</Link>.</Typography>

          <Typography><Link href="/scene-request">Sign up</Link>, follow the <Link href="/wiki/Basic_Client_Setup">setup instructions</Link>, and join <b>#hivemind</b> on Killer Queen Discord to connect your cabinet.</Typography>

          <Typography>Source code is available on <Link href="https://gitlab.com/kqhivemind/hivemind">GitLab</Link>. Contributions and feature requests are welcome.</Typography>
        </Grid>
      </Grid>
    </>
  );
}
