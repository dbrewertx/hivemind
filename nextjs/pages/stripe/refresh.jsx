import React from 'react';

import isServer from 'util/isServer';

export default function StripeRefresh({ }) {
  if (!isServer()) window.close();

  return (
    <>
    </>
  );
}
