import { makeStyles } from '@material-ui/core/styles';
import { Typography } from '@material-ui/core';
import { useRouter } from 'next/router';

import SceneRequestForm from 'components/forms/SceneRequestForm';
import LoginButtons from 'components/LoginButtons';
import { onLoginSuccess, UserConsumer } from 'util/auth';

const useStyles = makeStyles(theme => ({
}));

const SceneRequest = UserConsumer(({ user }) => {
  const classes = useStyles();
  const router = useRouter();

  return (
    <>
      <Typography variant="h1">Join HiveMind</Typography>

      <Typography>
        Once your scene has been created, you will be able to connect your cabinets to HiveMind.
      </Typography>

      {user?.id && (
        <>
          <Typography>
            <b>This is not the player registration form.</b> To edit your player profile, go to the <a href={`/user/${user.id}`}>user profile page</a> and click the Edit button.
          </Typography>

          <SceneRequestForm />
        </>
      )}

      {!user?.id && (
        <>
          <Typography>Log in with your Google account to continue.</Typography>
          <LoginButtons onSuccess={router.reload} />
        </>
      )}
    </>
  );
});

export default SceneRequest;
