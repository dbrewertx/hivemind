import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Breadcrumbs, Link } from '@material-ui/core';
import { useRouter } from 'next/router';
import clsx from 'clsx';

import { BLUE_TEAM, POSITIONS_BY_TEAM, SIGN_IN_ACTIONS, CABINET_POSITIONS } from 'util/constants';
import { useUser } from 'util/auth';
import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';
import Title from 'components/Title';
import LoginButtons from 'components/LoginButtons';

const useStyles = makeStyles(theme => ({
  container: {
    margin: `${theme.spacing(2)}px auto 0 auto`,
    maxWidth: '800px',
  },
  teams: {
    display: 'flex',
    flexDirection: 'row',
  },
  blueTeam: {
    flexGrow: 1,
    flexBasis: 0,
    display: 'flex',
    flexDirection: 'column',
  },
  goldTeam: {
    flexGrow: 1,
    flexBasis: 0,
    display: 'flex',
    flexDirection: 'column',
  },
  cabinetPosition: {
    flexGrow: 1,
    flexBasis: 0,
    gap: '32px',
    padding: theme.spacing(2),
    display: 'flex',
    flexDirection: 'row',
    borderWidth: '1px',
    borderStyle: 'solid',
    borderColor: theme.palette.divider,
    borderRadius: '10px',
    // boxShadow: '0 0 7px rgba(23,23,23,.3)',
    marginTop: '12px',
    backgroundColor: theme.palette.grey['100'],
    '&:first-of-type': {
      // borderTopWidth: '1px',
    },
    '&:hover': {
      cursor: 'pointer',
      outline: `2px solid ${theme.palette.text.primary}`,
      // marginBottom: '-1px',
    },
    '&.active.blue': {
      background: theme.gradients.blue.light,
    },
    '&.active.gold': {
      background: theme.gradients.gold.light,
    },
  },
  icon: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    '& img': {
      height: '96px',
    },
  },
  name: {
    flexGrow: 1,
    flexBasis: 0,
    display: 'flex',
    alignItems: 'center',
    fontSize: '22px',
    fontWeight: 'bold',
  },
}));

export default function SignInPage({ cabinet }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();
  const user = useUser();
  const [loading, setLoading] = useState(false);
  const classes = useStyles({ user, loading });
  const [signInDataLoaded, setSignInDataLoaded] = useState(false);

  const player = {};
  const setPlayer = {};
  const { position } = router.query;
  const [ team, playerPos ] = position;


  for (const pos of Object.values(CABINET_POSITIONS)) {
    [player[pos.ID], setPlayer[pos.ID]] = useState(null);
  }

  useEffect(() => {
    axios
      .get(`/api/game/cabinet/${cabinet.id}/signin/`)
      .then(response => {
        for (const player of response.data.signedIn) {
          setPlayer[player.playerId](player);
        }
        setSignInDataLoaded(true);
      });
  }, []);

  useEffect(() => {
    if(signInDataLoaded && user?.id) {
      const position = POSITIONS_BY_TEAM[team].find(position => position.POSITION === playerPos);
      if (position) {
        const parameters = {
          player: position.ID,
          cabinetId: cabinet.id,
          action: SIGN_IN_ACTIONS.SIGN_IN,
        };

        void axios.post(`/api/user/signin/qr/`, parameters);
      }
    }
  }, [signInDataLoaded, user?.id]);

  const ws = useWebSocket('/ws/signin');
  ws.onJsonMessage(message => {
    if (message.cabinetId !== cabinet.id) {
      return;
    }

    if (message.action === SIGN_IN_ACTIONS.SIGN_IN) {
      setPlayer[message.playerId](message);
    }

    if (message.action === SIGN_IN_ACTIONS.SIGN_OUT) {
      setPlayer[message.playerId](null);
    }
  });


  const login = async (pos) => {
    setLoading(true);

    if (user?.id && !loading) {
      const action = (player[pos.ID] && player[pos.ID].userId === user.id) ? SIGN_IN_ACTIONS.SIGN_OUT : SIGN_IN_ACTIONS.SIGN_IN;
      const response = await axios.post(`/api/user/signin/qr/`, {
        player: pos.ID,
        cabinetId: cabinet.id,
        action,
      });
    }

    setLoading(false);
  };

  const PlayerSignIn = ({ pos }) => {

    return (
      <>
        <div
          className={clsx(classes.cabinetPosition, pos.TEAM, { active: player[pos.ID] !== null })}
          onClick={() => login(pos)}
        >
          <div className={classes.icon}>
            <img src={pos.IMAGE} />
          </div>
          <div className={classes.name}>
            {player[pos.ID] ? player[pos.ID].userName : <span className="italic font-normal text-gray-500"><span className="md:hidden">Tap</span><span className="hidden md:inline">Click</span> to sign-in</span>}
          </div>
        </div>
      </>
    );
  };

  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${cabinet.scene.name}`}>{cabinet.scene.displayName}</Link>
        <Link href={`/cabinet/${cabinet.scene.name}/${cabinet.name}`}>{cabinet.displayName}</Link>
        <Link href={`/cabinet/${cabinet.scene.name}/${cabinet.name}/signin`}>Sign In</Link>
        <Link href={`/cabinet/${cabinet.scene.name}/${cabinet.name}/signin/${team}`}>{team}</Link>
        {playerPos && <span>{playerPos}</span>}
      </Breadcrumbs>

      <Title title={`${cabinet.displayName}: Sign In`} />

      {cabinet.allowQrSignin && (
        <>
          {!(user?.id) && (
            <>
              <Typography>Log in with your Google account to continue.</Typography>
              <LoginButtons onSuccess={router.reload} />
            </>
          )}

          <div className={classes.container}>
            <div className={classes.teams}>
              <div className={team === BLUE_TEAM ? classes.blueTeam : classes.goldTeam}>
                {POSITIONS_BY_TEAM[team].filter(pos => playerPos ? pos.POSITION === playerPos : true ).map(pos => (
                  <PlayerSignIn key={pos.ID} pos={pos} />
                ))}
              </div>
            </div>
          </div>
        </>
      )}

      {!cabinet.allowQrSignin && (
        <Typography>This cabinet is not configured to allow sign-ins.</Typography>
      )}
    </>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/game/scene/`, { params: { name: params.sceneName } });
  const scene = response.data.results[0];

  if (!scene) return { notFound: true };

  response = await axios.get(`/api/game/cabinet/`, { params: { sceneId: scene.id, name: params.cabinetName }});
  const cabinet = response.data.results[0];

  if (!cabinet) return { notFound: true };

  cabinet.scene = scene;

  return {
    props: {
      cabinet,
      title: `${scene.displayName} - ${cabinet.displayName}`,
      description: cabinet.description,
    },
  };
}

