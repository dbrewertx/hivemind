import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { CircularProgress } from '@material-ui/core';
import QRCode from 'qrcode';
import clsx from 'clsx';

import { SIGN_IN_ACTIONS } from 'util/constants';
import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';
import isServer from 'util/isServer';

const useStyles = makeStyles(theme => ({
  page: {
    position: 'fixed',
    top: 0,
    left: 0,
    width: '100%',
    height: '100%',
    margin: 0,
    padding: 0,
    border: 0,
    background: 'white',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  qr: {
    flexBasis: 0,
    flexGrow: 1,
    '& img': {
      height: '100%',
      width: 'auto',
    },
  },
  caption: {
    flexGrow: 0,
    padding: '1vh 0',
    fontSize: '8vh',
  },
  name: {
    fontWeight: 'bold',
    fontSize: '10vh',
  },
}));

export default function NfcKioskPage({ cabinet, readerId }) {
  const axios = getAxios();
  const classes = useStyles();
  const [src, setSrc] = useState(null);
  const [user, setUser] = useState(null);
  const [card, setCard] = useState(null);
  const [tid, setTid] = useState(null);

  const ws = useWebSocket('/ws/signin');
  ws.onJsonMessage(message => {
    if (message.type === SIGN_IN_ACTIONS.NFC_REGISTER &&
        message.sceneName === cabinet.scene.name &&
        message.cabinetName === cabinet.name &&
        message.readerId === readerId) {

      axios.get(`/api/user/user/${message.user}/`).then(response => {
        setUser(response.data);

        if (tid) {
          clearTimeout(tid);
        }

        setTid(setTimeout(() => { setUser(null); }, 60000));
      });
    }

    if (message.type === SIGN_IN_ACTIONS.NFC_REGISTER_SUCCESS && message.userId === user?.id) {
      setCard(message.cardId);

      if (tid) {
        clearTimeout(tid);
      }

      setTid(setTimeout(() => { setUser(null); setCard(null); }, 10000));
    }
  });

  if (src === null) {
    const domain = isServer() ? 'https://kqhivemind.com' : window.location.origin;
    QRCode.toDataURL(`${domain}/cabinet/${cabinet.scene.name}/${cabinet.name}/register-nfc/${readerId}`).then(setSrc);

    return (<CircularProgress />);
  }

  if (user !== null && card === null) {
    return (
      <div className={classes.page}>
        <div className={clsx(classes.caption, classes.name)}>{user.name}</div>
        <div className={classes.caption}>Tap your card to register.</div>
      </div>
    );
  }

  if (user !== null && card !== null) {
    return (
      <div className={classes.page}>
        <div className={clsx(classes.caption, classes.name)}>{user.name}</div>
        <div className={classes.caption}>Registered card <b>{card}</b>.</div>
      </div>
    );
  }

  return (
    <div className={classes.page}>
      <div suppressHydrationWarning className={classes.qr}>
        <img src={src} />
      </div>

      <div className={classes.caption}>
        Scan to register your NFC tag.
      </div>
    </div>
  );
}

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/game/scene/`, { params: { name: params.sceneName } });
  const scene = response.data.results[0];

  if (!scene) return { notFound: true };

  response = await axios.get(`/api/game/cabinet/`, { params: { sceneId: scene.id, name: params.cabinetName }});
  const cabinet = response.data.results[0];

  if (!cabinet) return { notFound: true };

  cabinet.scene = scene;

  return {
    props: { cabinet, readerId: params.readerId },
  };
}

