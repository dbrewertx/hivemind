import { CircularProgress, Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { mdiEmoticonCoolOutline, mdiEmoticonSadOutline, mdiStar } from '@mdi/js';
import Icon from '@mdi/react';
import clsx from 'clsx';
import { parseISO } from 'date-fns';
import Head from 'next/head';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';

import Achievement from 'components/Achievement';
import Title from 'components/Title';
import UserForm from 'components/forms/UserForm';
import GameTable from 'components/tables/GameTable';
import UserStatsByPositionTable from 'components/tables/UserStatsByPositionTable';
import UserStatsSummaryTable from 'components/tables/UserStatsSummaryTable';
import VideoClipsTable from 'components/tables/VideoClipsTable';
import { useUser } from 'util/auth';
import { getAxios } from 'util/axios';
import { POSITIONS_BY_ID, POSITIONS_BY_NAME } from 'util/constants';

const useStyles = makeStyles(theme => ({
  topRow: {
    padding: theme.spacing(0, 1),
  },
  location: {},
  bottomRow: {
    padding: theme.spacing(0, 1),
    justifyContent: 'space-between',
  },
  date: {
    fontSize: '90%',
  },
  playerInfo: {
    fontSize: '90%',
    [theme.breakpoints.down('xs')]: {
      textAlign: 'right',
    },
  },
  result: {
    fontSize: '90%',
    [theme.breakpoints.up('sm')]: {
      textAlign: 'right',
    },
  },
  icon: {
    display: 'block',
    height: '16px',
    width: '16px',
  },
}));

export default function UserPage() {
  const router = useRouter();
  const requestUser = useUser();
  const classes = useStyles();
  const [user, setUser] = useState(null);
  const [isPrivate, setIsPrivate] = useState(false);

  const EmojiIcon = ({ path, text }) => (
    <div className="flex items-center gap-1">
      <Icon path={path} className="w-5 h-5 inline-block" />
      <span>{text}</span>
    </div>
  );
  const playerGameResult = row => {
    const userPositionEntry = row.users.filter(u => u.user === user.id);
    const userPosition = userPositionEntry ? POSITIONS_BY_ID[userPositionEntry[0].playerId] : null;
    return (
      <div item className="inline-flex md:flex mt-1 md:mt-0 md:pl-8">
        <Typography className={clsx(classes.playerInfo, ' flex items-center justify-start gap-3')}>
          <span>
            {userPosition ? (
              <img className={classes.icon} src={POSITIONS_BY_NAME[userPosition.NAME].ICON} />
            ) : (
              'Unknown'
            )}
          </span>
          {userPosition ? (
            userPosition.TEAM === row.winningTeam ? (
              <EmojiIcon path={mdiEmoticonCoolOutline} text="Win" />
            ) : (
              <EmojiIcon path={mdiEmoticonSadOutline} text="Loss" />
            )
          ) : (
            <span>Unknown</span>
          )}
        </Typography>
      </div>
    );
  };

  useEffect(() => {
    async function getUserInfo() {
      if (!router.isReady) {
        return;
      }

      try {
        const axios = getAxios({ authenticated: true });
        let response = await axios.get(`/api/user/user/${router.query.id}/`);
        const userInfo = response.data;

        response = await axios.get(`/api/achievement/user-achievement/`, {
          params: { userId: userInfo.id },
        });
        userInfo.achievements = response.data.results;

        setIsPrivate(false);
        setUser(userInfo);
      } catch (err) {
        setIsPrivate(true);
      }
    }
    getUserInfo();
  }, [router.isReady, requestUser?.id]);

  if (user === null && isPrivate) {
    return (
      <>
        <Title title="User Not Found" />

        <Typography>Either this user does not exist, or their account is private.</Typography>
      </>
    );
  }

  if (user === null) {
    return (
      <>
        <CircularProgress />
      </>
    );
  }

  const form = <UserForm obj={user} />;
  const titleText = user.name + (user.scene ? ` [${user.scene}]` : '');

  return (
    <>
      <Head>
        <title>User Profile: {user.name}</title>
      </Head>
      <Title form={form} title={titleText} subtitle={user.pronouns} avatarSrc={user.image} />

      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} lg={6}>
          <UserStatsSummaryTable user={user} stats={user.stats} />
        </Grid>
        <Grid item xs={12} lg={6} className="relative">
          <GameTable
            filters={{ user_id: user.id }}
            afterDateSlot={playerGameResult}
            rowsPerPage={10}
          />
          <VideoClipsTable filters={{ createdBy: user.id }} title="Video Clips Created" />
        </Grid>
      </Grid>

      <Typography variant="h2">Stats By Position</Typography>
      <UserStatsByPositionTable stats={user.statsByPosition} />
      <div>
        <div className="-mx-4 md:-mx-10 !-mb-12 !mt-12 rounded-b bg-gray-800 text-white px-10 pt-6 pb-12 space-y-8">
          <Typography variant="h2" className="flex items-center gap-3">
            <span>Achievements</span>
            <Icon className="text-gold-main inline-block w-8 h-8 align-middle" path={mdiStar} />
          </Typography>

          <div className="grid grid-cols-1 md:grid-cols-3 gap-6">
            {user.achievements
              .sort((a, b) => parseISO(a.timestamp) - parseISO(b.timestamp))
              .map(achievement => (
                <Achievement key={achievement.id} achievement={achievement} />
              ))}
          </div>
        </div>
      </div>
    </>
  );
}
