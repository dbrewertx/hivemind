import { makeStyles } from '@material-ui/core/styles';
import { Typography, Paper, Breadcrumbs, Link, Grid, Box } from '@material-ui/core';
import { getAxios } from 'util/axios';
import { format } from 'date-fns';
import PropTypes from 'prop-types';
import clsx from 'clsx';

import Title from 'components/Title';
import PlayerStandingsTable from 'components/tables/PlayerStandingsTable';
import BYOTeamFinalStandingsTable from 'components/tables/BYOTeamFinalStandingsTable';
import BYOTeamStandingsTable from 'components/tables/BYOTeamStandingsTable';
import EventsTable from 'components/tables/EventsTable';
import SeasonForm from 'components/forms/SeasonForm';
import { SEASON_TYPES } from 'util/constants';

const useStyles = makeStyles(theme => ({
}));

export default function SeasonPage({ season }) {
  const classes = useStyles({ season });
  const form = (<SeasonForm season={season} />);
  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${season.scene.name}`}>{season.scene.displayName}</Link>
      </Breadcrumbs>

			<Title title={`${season.scene.displayName} - ${season.name}`} form={form} />

      {season.seasonType === SEASON_TYPES.SHUFFLE && (
        <PlayerStandingsTable data={season.standings} />
      )}

      {season.seasonType === SEASON_TYPES.BYOT && (
        <>
          {season.finalStandings?.length > 0 && (
            <BYOTeamFinalStandingsTable data={season.finalStandings} />
          )}
          <BYOTeamStandingsTable season={season} data={season.standings} />
        </>
      )}

      <EventsTable season={season} />
    </>
  );
}

SeasonPage.propTypes = {
  season: PropTypes.object.isRequired,
};

export async function getServerSideProps({ params }) {
  const axios = getAxios();
  let response = await axios.get(`/api/league/season/${params.id}`);
  const season = response.data;

  if (!season) return { notFound: true };

  response = await axios.get(`/api/game/scene/${season.scene}/`);
  season.scene = response.data;

  response = await axios.get(`/api/league/season/${params.id}/standings`);
  season.standings = response.data;

  response = await axios.get(`/api/league/event/`, { params: { seasonId: season.id } });
  season.events = response.data.results;

  if (season.seasonType === SEASON_TYPES.BYOT) {
    response = await axios.get(`/api/league/season-place/`, { params: { seasonId: season.id } });
    season.finalStandings = response.data.results;
  }

  return {
    props: { season, title: season.name, description: `${season.name} in ${season.scene.displayName}` },
  };
}

