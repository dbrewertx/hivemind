import { Breadcrumbs, CircularProgress, Link } from '@material-ui/core';

import Title from 'components/Title';
import TournamentPlayerEditor from 'components/TournamentPlayerEditor';
import { TournamentAdminProvider, useTournamentAdmin } from 'providers/TournamentAdmin';

export function PageContents({}) {
  const { tournament } = useTournamentAdmin();

  if (!tournament) {
    return <CircularProgress />;
  }

  return (
    <>
      <Breadcrumbs>
        <Link href={`/scene/${tournament.scene.name}`}>{tournament.scene.displayName}</Link>
        <Link href={`/tournament/${tournament.id}`}>{tournament.name}</Link>
        <Link href={`/tournament/${tournament.id}/admin`}>Tournament Admin</Link>
      </Breadcrumbs>

      <Title title={`Tournament Team & Player Management`} />
      <TournamentPlayerEditor dialog={false} />
    </>
  );
}

export default function TournamentPlayerAdminPage({ id }) {
  return (
    <TournamentAdminProvider id={id}>
      <PageContents />
    </TournamentAdminProvider>
  );
}

export async function getServerSideProps({ params }) {
  return {
    props: { id: params.id, title: 'Tournament Admin' },
  };
}

