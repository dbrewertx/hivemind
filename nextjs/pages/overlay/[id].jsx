import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Breadcrumbs, Link, Typography, List, ListItem, CircularProgress, Grid } from '@material-ui/core';
import fileDownload from 'js-file-download'
import StringifyWithFloats from 'stringify-with-floats';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import Title from 'components/Title';
import OverlayForm from 'components/forms/OverlayForm';
import { getConfig } from 'overlay/obs';
import { getIngameTheme } from 'overlay/ingame/themes';
import { getPostgameTheme } from 'overlay/postgame/themes';
import { getPlayerCamsTheme } from 'overlay/playerCams/themes';
import Head  from 'next/head';

const loadOverlay = async id => {
  const axios = getAxios();
  let response = await axios.get(`/api/overlay/overlay/${id}/`);
  const overlay = response.data;

  response = await axios.get(`/api/game/cabinet/${overlay.cabinet}/`);
  overlay.cabinet = response.data;

  response = await axios.get(`/api/game/scene/${overlay.cabinet.scene}/`);
  overlay.cabinet.scene = response.data;

  overlay.themes = {
    ingame: getIngameTheme(overlay.ingameTheme),
    postgame: getPostgameTheme(overlay.postgameTheme),
    playerCams: getPlayerCamsTheme(overlay.playerCamsTheme),
  };

  return overlay;
};

export default function OverlayPage({ overlayId }) {
  const [overlay, setOverlay] = useState(null);

  if (overlay === null) {
    loadOverlay(overlayId).then(setOverlay);
    return (<CircularProgress />);
  }

  const schema = {
    x: 'float',
    y: 'float',
    height: 'float',
    width: 'float',
  };

  const stringify = StringifyWithFloats(schema);

  const downloadConfig = evt => {
    evt.preventDefault();

    const config = getConfig(overlay);
    fileDownload(stringify(config, null, 2), `HiveMind_${overlay.cabinet.name}.json`);
  };

  return (
    <>
      <Head>
        <title>{`Edit Overlay: ${overlay.cabinet.displayName}`}</title>
      </Head>
      <Breadcrumbs>
        <Link href={`/scene/${overlay.cabinet.scene.name}`}>{overlay.cabinet.scene.displayName}</Link>
        <Link href={`/cabinet/${overlay.cabinet.scene.name}/${overlay.cabinet.name}`}>{overlay.cabinet.displayName}</Link>
        <Link href={`/overlay/${overlay.id}`}>{overlay.name}</Link>
      </Breadcrumbs>

      <Title title={`Edit Overlay: ${overlay.cabinet.displayName}`} />


      <OverlayForm overlay={overlay} />

      { isAdminOf(overlay?.cabinet?.scene?.id) && (
        <>
          <Typography variant="h3">Overlay Resources</Typography>

          <Grid container direction="row" spacing={2} className="mt-6">
            <Grid item xs={12} md={6}>

            <Typography variant="h4">Setup</Typography>
              <List>
                <ListItem>
                  <Link href="/wiki/OBS_Configuration">OBS Setup Instructions</Link>
                </ListItem>
                <ListItem>
                  <Link href="#" onClick={downloadConfig}>Download OBS Scene Collection</Link>
                </ListItem>

              </List>
            </Grid>
            <Grid item xs={12} md={6}>
              <Typography variant="h4">Direct Links</Typography>
              <List className="md:columns-2">
                <ListItem>
                  <Link href={`/overlay/${overlay.id}/ingame`}>In-Game Overlay</Link>
                </ListItem>
                <ListItem>
                  <Link href={`/overlay/${overlay.id}/postgame`}>Postgame Overlay</Link>
                </ListItem>
                <ListItem>
                  <Link href={`/overlay/${overlay.id}/auto`}>Auto-Switch Overlay</Link>
                </ListItem>
                <ListItem>
                  <Link href={`/overlay/${overlay.id}/match`}>Match Summary Overlay</Link>
                </ListItem>
                <ListItem>
                  <Link href={`/overlay/${overlay.id}/match-preview`}>Match Preview Overlay</Link>
                </ListItem>
                <ListItem>
                  <Link href={`/overlay/${overlay.id}/player-cams`}>Player Cameras Overlay</Link>
                </ListItem>
              </List>
            </Grid>
          </Grid>
        </>
      )}
    </>
  );
}

export async function getServerSideProps({ params }) {
  return {
    props: { overlayId: params.id },
  };
}

