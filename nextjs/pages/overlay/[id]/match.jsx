import React from 'react';
import PropTypes from 'prop-types';

import App from 'overlay/App';
import theme from 'theme/overlay';
import { OverlaySettingsProvider, MatchOverlay } from 'overlay/Settings';
import { MatchStatsProvider } from 'overlay/MatchStats';
import { MatchProvider } from 'overlay/Match';

export default function PostgameOverlayPage({ id }) {
  return (
    <OverlaySettingsProvider id={id}>
      <MatchStatsProvider autoHide={false}>
        <MatchOverlay />
      </MatchStatsProvider>
    </OverlaySettingsProvider>
  );
}

PostgameOverlayPage.propTypes = {
  id: PropTypes.string.isRequired,
};

PostgameOverlayPage.App = App;

export async function getServerSideProps({ params }) {
  return {
    props: params,
  };
}

