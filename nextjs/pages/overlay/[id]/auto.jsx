import React from 'react';
import PropTypes from 'prop-types';

import App from 'overlay/App';
import theme from 'theme/overlay';
import { OverlaySettingsProvider, PostgameOverlay, MatchOverlay, MatchPreviewOverlay } from 'overlay/Settings';
import { SceneController } from 'overlay/SceneController';
import { PostgameStatsProvider } from 'overlay/PostgameStats';
import { MatchStatsProvider } from 'overlay/MatchStats';
import { MatchProvider } from 'overlay/Match';

export default function AutoOverlayPage({ id }) {
  return (
    <OverlaySettingsProvider id={id}>
      <SceneController>
        <MatchProvider>
          <MatchStatsProvider autoHide={true}>
            <PostgameStatsProvider autoHide={true}>
              <PostgameOverlay />
              <MatchOverlay />
              <MatchPreviewOverlay />
            </PostgameStatsProvider>
          </MatchStatsProvider>
        </MatchProvider>
      </SceneController>
    </OverlaySettingsProvider>
  );
}

AutoOverlayPage.propTypes = {
  id: PropTypes.string.isRequired,
};

AutoOverlayPage.App = App;

export async function getServerSideProps({ params }) {
  return {
    props: params,
  };
}

