import React from 'react';
import PropTypes from 'prop-types';

import App from 'overlay/App';
import theme from 'theme/overlay';
import { OverlaySettingsProvider, PostgameOverlay } from 'overlay/Settings';
import { PostgameStatsProvider } from 'overlay/PostgameStats';
import { MatchProvider } from 'overlay/Match';

export default function PostgameOverlayPage({ id }) {
  return (
    <OverlaySettingsProvider id={id}>
      <PostgameStatsProvider autoHide={false}>
        <PostgameOverlay />
      </PostgameStatsProvider>
    </OverlaySettingsProvider>
  );
}

PostgameOverlayPage.propTypes = {
  id: PropTypes.string.isRequired,
};

PostgameOverlayPage.App = App;

export async function getServerSideProps({ params }) {
  return {
    props: params,
  };
}

