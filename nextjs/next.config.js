module.exports = {
  trailingSlash: false,
  publicRuntimeConfig: {
    OAUTH_CLIENT_ID:
      process.env.OAUTH_CLIENT_ID ||
      '416065075376-cm0il1tlgck8n6vevu7l0meuuoslu3o8.apps.googleusercontent.com',
  },
  webpack: config => {
    config.resolve.fallback = {
      canvas: false,
      crypto: false,
      dgram: false,
      net: false,
      os: false,
      stream: false,
      tls: false,
      zlib: false,
    };
    return config;
  },
  eslint: {
    ignoreDuringBuilds: true,
  },
  async redirects() {
    return [
      {
        source: '/wiki',
        destination: '/wiki/Main_Page',
        permanent: true,
      },
      {
        source: '/activate',
        destination: '/wiki/Player_Card_Registration',
        permanent: true,
      },
      {
        source: '/setup/cabinet',
        destination: '/wiki/Basic_Client_Setup',
        permanent: true,
      },
      {
        source: '/setup/obs',
        destination: '/wiki/OBS_Configuration',
        permanent: true,
      },
    ];
  },
};
