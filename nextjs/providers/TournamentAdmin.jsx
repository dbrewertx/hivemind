import { createContext, useContext, useEffect, useState } from 'react';

import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';

export const TournamentAdminContext = createContext({});

export async function loadTournament(id) {
  const axios = getAxios({ authenticated: true });

  let response = await axios.get(`/api/tournament/tournament/${id}/`);
  const obj = { ...response.data };

  if (!obj) {
    setTournament({ notFound: true });
    return;
  }

  response = await axios.get(`/api/game/scene/${obj.scene}/`);
  obj.scene = response.data;

  obj.teams = await axios.getAllPages(`/api/tournament/team/`, {
    params: { tournamentId: obj.id },
  });

  obj.teamsById = {};
  for (const team of obj.teams) {
    obj.teamsById[team.id] = team;
  }

  response = await axios.get(`/api/tournament/bracket/`, { params: { tournamentId: obj.id } });
  obj.brackets = [];

  for (const bracket of response.data.results) {
    bracket.matches = await axios.getAllPages(`/api/tournament/match/`, {
      params: { bracketId: bracket.id },
    });
    bracket.matches.sort((a, b) => a?.roundName?.localeCompare(b?.roundName) || a.id - b.id);
    obj.brackets.push(bracket);
  }

  return obj;
}

export function TournamentAdminProvider({ id, children }) {
  const axios = getAxios({ authenticated: true });

  const [cabinets, setCabinets] = useState(null);
  const [hideInactiveCabs, setHideInactiveCabs] = useState(true);
  const [tournament, setTournament] = useState(null);
  const [matches, setMatches] = useState(null);
  const [loading, setLoading] = useState(false);
  const [wsGameStateOpen, setWsGameStateOpen] = useState(false);
  const [wsGameStateInitialized, setWsGameStateInitialized] = useState(false);

  const reloadTournament = () => {
    loadTournament(id).then(setTournament);
  };

  const loadAvailableMatches = async () => {
    if (loading) {
      return;
    }

    setLoading(true);

    const reloadedTournament = await loadTournament(id);
    const response = await axios.get(`/api/tournament/tournament/${id}/available-matches/`);

    setTournament(reloadedTournament);
    setMatches(response.data);
    setLoading(false);
  };

  const loadCabinets = async () => {
    let response = await axios.get(`/api/game/cabinet/`, {
      params: { sceneId: tournament.scene.id },
    });
    setCabinets(response.data.results);
  };

  useEffect(() => {
    if (tournament === null) {
      reloadTournament();
    }
  }, [tournament]);

  useEffect(() => {
    if (cabinets === null && tournament?.scene?.id) {
      loadCabinets();
    }
  }, [tournament?.scene?.id]);

  const onOpenGameState = () => {
    setWsGameStateOpen(true);
  };

  const wsGameState = useWebSocket('/ws/gamestate', { onOpen: onOpenGameState });

  const wsTournamentRelay = useWebSocket(`/ws/tournament-relay/${id}`);
  wsTournamentRelay.onJsonMessage(message => {
    if (message.type === 'matches') {
      setMatches(message.data);
    }
  });

  useEffect(() => {
    if (wsGameStateOpen && tournament && !wsGameStateInitialized) {
      wsGameState.sendJsonMessage({ scene_name: tournament.scene.name });
      setWsGameStateInitialized(true);
    }
  }, [wsGameStateOpen, wsGameStateInitialized]);

  const teamsById = {};
  if (tournament?.teams) {
    for (const team of tournament.teams) {
      teamsById[team.id] = team;
    }
  }

  const ctx = {
    tournament,
    cabinets,
    hideInactiveCabs,
    setHideInactiveCabs,
    matches,
    loading,
    reloadTournament,
    loadAvailableMatches,
    wsGameState,
    teamsById,
  };

  return <TournamentAdminContext.Provider value={ctx}>{children}</TournamentAdminContext.Provider>;
}

export function useTournamentAdmin() {
  return useContext(TournamentAdminContext);
}
