import React, { useEffect, useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Avatar, Grid } from '@material-ui/core';
import Carousel from 'react-material-ui-carousel';

import { getAxios } from 'util/axios';
import Table, { TableRow, TableCell } from 'components/tables/Table';

const useStyles = makeStyles(theme => ({
  leaderTable: {
    margin: 0,
    padding: 0,
  },
  valueCell: {
    textAlign: 'right',
  },
  carousel: {
    background: theme.palette.grey['200'],
    borderRadius: '5px',
    overflow: 'hidden',
    paddingBottom: '.75rem',
  },
  nameCell: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  avatar: {
    height: '24px',
    width: '24px',
    marginRight: theme.spacing(1),
  },
}));

export default function Leaderboards({}) {
  const axios = getAxios({ authenticated: true });
  const classes = useStyles();
  const [leaderboards, setLeaderboards] = useState(null);

  useEffect(() => {
    if (leaderboards === null) {
      axios.get('/api/stats/leaderboards/').then(response => {
        setLeaderboards(response.data.results);
      });
    }
  }, []);

  if (leaderboards === null) {
    return <></>;
  }

  return (
    <Carousel navButtonsAlwaysInvisible={true} interval={15000} className={classes.carousel}>
      {leaderboards.map(leaderboard => (
        <Table
          key={leaderboard.title}
          title={leaderboard.title}
          tableSubtitle="in past 7 days"
          columnHeaders={['', '']}
          showColumnHeaders={false}
          className={classes.leaderTable}
        >
          {leaderboard?.leaders?.map(row => (
            <TableRow key={row.user}>
              <TableCell className={classes.nameCell}>
                <Grid item>
                  <Avatar src={row.image} alt={row.name} className={classes.avatar} />
                </Grid>
                <Grid item>
                  {row.name || '(no name set)'}
                  {row.scene && ` [${row.scene}]`}
                </Grid>
              </TableCell>
              <TableCell className={classes.valueCell}>{row.value}</TableCell>
            </TableRow>
          ))}
        </Table>
      ))}
    </Carousel>
  );
}
