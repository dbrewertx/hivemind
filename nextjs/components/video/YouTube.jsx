import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

const useStyles = makeStyles(theme => ({
  container: {
    margin: `${theme.spacing(2)}px auto`,
    width: 'min(80vw, 640px)',
    height: 'calc(min(80vw, 640px) * 9 / 16)',
  },
  iframe: {
    width: '100%',
    height: '100%',
  },
}));

export default function YouTube({ url, className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.container, className)}>
      <iframe
        className={classes.iframe}
        width="100%"
        height="100%"
        src={url}
        title="YouTube Video Player"
        frameBorder="0"
        allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
        allowFullScreen="allowFullScreen"
      />
    </div>
  );
}
