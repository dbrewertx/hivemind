import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import Icon from '@mdi/react';
import { mdiTarget } from '@mdi/js';

import { MAP_NAMES } from 'util/constants';
import GameEvent from 'models/GameEvent';

const useStyles = makeStyles(theme => ({
  mapDiv: {
    position: 'relative',
  },
  mapImage: {
    width: '100%',
  },
  target: {
    position: 'absolute',
    color: 'red',
    width: '50px',
    height: '50px',
    marginLeft: '-25px',
    marginTop: '-25px',
  },
}));

export default function GameEventMap({ event }) {
  const classes = useStyles();
  const mapName = event.game.mapName.toLowerCase();
  const hasLocation = event.location?.length >= 2;

  if (!['day', 'night', 'dusk', 'twilight'].includes(mapName)) {
    return (<></>);
  }

  const mapUrl = `/static/maps/${mapName}.png`;
  const targetStyle = {};

  if (hasLocation) {
    targetStyle.left = `${(100 * parseInt(event.location[0]) / 1920)}%`;
    targetStyle.top = `${100 - (100 * parseInt(event.location[1]) / 1080)}%`;
  }

  return (
    <div className={classes.mapDiv}>
      <img src={mapUrl} className={classes.mapImage} />
      {hasLocation && (
        <Icon path={mdiTarget} className={classes.target} style={targetStyle} />
      )}
    </div>
  );
}

GameEventMap.propTypes = {
  event: PropTypes.instanceOf(GameEvent).isRequired,
};

GameEventMap.defaultProps = {
};
