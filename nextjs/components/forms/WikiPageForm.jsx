import React, { useState } from 'react';
import { Link } from '@material-ui/core';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { toast } from 'react-toastify';

import Form from './Form';
import Field, { TextField } from '../fields/Field';
import { useUser } from 'util/auth';
import { getAxios } from 'util/axios';
import { PERMISSION_TYPES } from 'util/constants';

const WikiPageSchema = Yup.object().shape({
  title: Yup.string()
    .required('Title is required')
    .max(100, 'Too long'),
  pageText: Yup.string(),
});

export default function WikiPageForm({ page, onSave }) {
  const axios = getAxios({ authenticated: true });
  const user = useUser();

  const props = {
    buttonText: 'Edit',
    dialogTitle: page.title ? `Editing Wiki Page: ${page.title}` : 'Editing Wiki Page',
    canEdit: page => Boolean(user?.id),
    object: page,
    validationSchema: WikiPageSchema,
    url: `/api/wiki/publish/`,
    method: 'POST',
    onSave,
  };

  return (
    <Form {...props}>
      <Field name='title' label='Page Title' component={TextField} />
      <Field name='pageText' label='Page Text' component={TextField} multiline rows={20} />
      <Link href="https://www.markdownguide.org/cheat-sheet/">Markdown Syntax Cheat Sheet</Link>
    </Form>
  );
}
