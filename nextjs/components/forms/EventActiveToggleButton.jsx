import React, { useState } from 'react';
import { Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useRouter } from 'next/router';
import { Button, Grid } from '@material-ui/core';
import PropTypes from 'prop-types';

import Form from './Form';
import Field, { TextField } from '../fields/Field';
import { UserConsumer } from 'util/auth';
import { getAxios } from 'util/axios';
import { PERMISSION_TYPES } from 'util/constants';

const useStyles = makeStyles(theme => ({
  buttonContainer: {
    justifyContent: 'center',
    padding: theme.spacing(2, 0),
  },
  loading: {
    color: '#777',
    borderColor: '#777',
  },
  activate: {
    color: '#373',
    borderColor: '#373',
  },
  deactivate: {
    color: '#733',
    borderColor: '#733',
  },
}));

const EventActiveToggleButton = UserConsumer(({ user, event, onSave }) => {
  const axios = getAxios({ authenticated: true });
  const classes = useStyles();
  const router = useRouter();
  const [loading, setLoading] = useState(false);

  const onClick = async () => {
    setLoading(true);

    const response = await axios.get(`/api/league/event/${event.id}/`);
    const postdata = response.data;

    postdata.isActive = !event.isActive;

    await axios.put(`/api/league/event/${event.id}/`, postdata);
    await axios.put(`/api/league/event/${event.id}/publish/`, {});

    if (onSave !== null) {
      await onSave();
    }

    setLoading(false);
  };

  let canEdit = false;
  if (user?.permissions) {
    for (const permission of user.permissions) {
      if (event.season.scene.id === permission.scene && permission.permission === PERMISSION_TYPES.ADMIN) {
        canEdit = true;
      }
    }
  }

  if (!canEdit) {
    return (<></>);
  }

  return (
    <Grid container className={classes.buttonContainer}>
      <Grid item>
        {loading && (
          <Button className={classes.loading} variant="outlined">Loading...</Button>
        )}
        {(!loading && event.isActive) && (
          <Button className={classes.deactivate} variant="outlined" onClick={onClick}>Pause Event</Button>
        )}
        {(!loading && !event.isActive) && (
          <Button className={classes.activate} variant="outlined" onClick={onClick}>Start Event</Button>
        )}
      </Grid>
    </Grid>
  );
});

EventActiveToggleButton.propTypes = {
  event: PropTypes.object.isRequired,
  onSave: PropTypes.func,
};

EventActiveToggleButton.defaultProps = {
  onSave: null,
};

export default EventActiveToggleButton;
