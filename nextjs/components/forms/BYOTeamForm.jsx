import React, { useState } from 'react';
import PropTypes from 'prop-types';
import * as Yup from 'yup';
import { toast } from 'react-toastify';
import { useRouter } from 'next/router';

import Form from './Form';
import List from './List';
import Field, { TextField } from '../fields/Field';
import PlayerListAutocomplete from '../fields/PlayerListAutocomplete';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';

const BYOTeamSchema = Yup.object().shape({
  name: Yup.string()
    .required('Team name is required')
    .max(100, 'Too long'),
});

export default function BYOTeamForm({ team }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();

  const getPlayerIds = async (values) => {
    const ids = [];
    for (const player of values.players) {
      if (player.id) {
        ids.push(player.id);
      } else {
        const response = await axios.post('/api/league/player/', { name: player.value, scene: team.season.scene.id });
        ids.push(response.data.id);
      }
    }

    return ids;
  };

  const props = {
    buttonText: team.id ? 'Edit' : 'Create Team',
    dialogTitle: team.id ? `Editing Team: ${team.name}` : 'Create New Team',
    canEdit: team => isAdminOf(team.season.scene.id),
    object: team,
    validationSchema: BYOTeamSchema,
    url: team.id ? `/api/league/byoteam/${team.id}/` : '/api/league/byoteam/',
    method: team.id ? 'PUT' : 'POST',
    getPostData: {
      season: values => values.season.id,
      players: getPlayerIds,
    },
  };

  return (
    <Form {...props}>
      <Field name="name" label="Team Name" component={TextField} />
      <PlayerListAutocomplete name="players" label="Players" scene={team.season.scene} />
    </Form>
  );
}

BYOTeamForm.propTypes = {
  team: PropTypes.object.isRequired,
};
