import React, { useState, useMemo, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogActions,
  Card,
  CardHeader,
  CardContent,
  CircularProgress,
  Typography,
  Grid,
  Button,
  Switch,
  FormGroup,
  FormControlLabel,
} from '@material-ui/core';
import Icon from '@mdi/react';
import { mdiSwapHorizontalBold, mdiRefresh, mdiTimer, mdiMap, mdiFruitGrapes } from '@mdi/js';

import clsx from 'clsx';
import * as Yup from 'yup';

import { TOURNAMENT_LINK_TYPES, MAP_NAMES } from 'util/constants';
import { getAxios } from 'util/axios';
import { formatUTC } from 'util/dates';
import { useTournamentAdmin } from 'providers/TournamentAdmin';
import ScoreUpdater from 'components/ScoreUpdater';
import TournamentGameSelector from 'components/tables/TournamentGameSelector';

const useStyles = makeStyles(theme => ({
  card: {
    cursor: 'pointer',
    width: '100%',
    '&:hover': {
      backgroundColor: theme.palette.action.hover,
    },
    '&.active': {
      backgroundColor: theme.palette.highlight.main,
    },
    '&.disabled': {
      cursor: 'initial',
      '&:not(.active)': {
        backgroundColor: theme.palette.action.disabled,
      },
    },
  },
  cardTitle: {
    fontWeight: 'bold',
  },
  scoreUpdaterBlue: {
    backgroundColor: theme.palette.blue.light1,
    textAlign: 'center',
  },
  scoreUpdaterGold: {
    backgroundColor: theme.palette.gold.light1,
    textAlign: 'center',
  },
  gameStats: {
    backgroundColor: theme.palette.action.selected,
    margin: theme.spacing(0, 5),
  },
  subtitleItem: {
    flexBasis: 0,
    flexGrow: 1,
    textAlign: 'center',
    '& svg': {
      width: '24px',
      height: '24px',
      verticalAlign: 'middle',
    },
  },
  spacer: {
    margin: theme.spacing(1),
  },
  warmupSwitch: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: theme.spacing(2),
  },
}));

export default function TournamentMatchForm({
  cabinet,
  activeMatch,
  open,
  onClose,
  stats,
  ...props
}) {
  const classes = useStyles();
  const axios = getAxios({ authenticated: true });
  const [isLoading, setIsLoading] = useState(false);
  const { tournament, matches, loading, loadAvailableMatches } = useTournamentAdmin();

  const teamsById = useMemo(() => {
    const teams = {};
    for (const team of tournament.teams) {
      teams[team.id] = team;
    }

    return teams;
  }, [tournament.teams]);

  useEffect(() => {
    if (matches === null) {
      loadAvailableMatches();
    }
  }, [matches]);

  const flip = async () => {
    if (isLoading) {
      return;
    }

    setIsLoading(true);
    const postdata = {
      isFlipped: !activeMatch.isFlipped,
      blueTeam: activeMatch.goldTeam,
      goldTeam: activeMatch.blueTeam,
    };

    await axios.patch(`/api/tournament/match/${activeMatch.id}/`, postdata);
    setIsLoading(false);
  };

  const toggleWarmup = async () => {
    if (isLoading) {
      return;
    }

    setIsLoading(true);
    const response = await axios.get(`/api/tournament/match/${activeMatch.id}/`);
    const postdata = {
      isWarmup: !response.data.isWarmup,
    };

    await axios.patch(`/api/tournament/match/${activeMatch.id}/`, postdata);
    setIsLoading(false);
  };

  const updateBlueScore = async value => {
    await axios.patch(`/api/tournament/match/${activeMatch.id}/`, { blueScore: value });
  };

  const updateGoldScore = async value => {
    await axios.patch(`/api/tournament/match/${activeMatch.id}/`, { goldScore: value });
  };

  const MatchCard = ({ match }) => {
    const handleClick = async () => {
      if (match?.activeCabinet) {
        return;
      }

      if (match?.id !== activeMatch?.id) {
        if (activeMatch?.id) {
          await axios.patch(`/api/tournament/match/${activeMatch.id}/`, { activeCabinet: null });
        }

        if (match?.id) {
          await axios.patch(`/api/tournament/match/${match.id}/`, { activeCabinet: cabinet.id });
        }
      }
    };

    const className = clsx(classes.card, {
      active: match?.id === activeMatch?.id,
      disabled: match?.activeCabinet,
    });

    if (!match) {
      return (
        <Grid item xs={12} md={4}>
          <Card onClick={handleClick} className={className}>
            <CardContent>
              <Typography className={classes.cardTitle}>Inactive</Typography>
            </CardContent>
          </Card>
        </Grid>
      );
    }

    const blueTeam = teamsById[match.blueTeam];
    const goldTeam = teamsById[match.goldTeam];

    return (
      <Grid item xs={12} md={4}>
        <Card onClick={handleClick} className={className}>
          <CardContent>
            <Typography className={classes.cardTitle}>{match.roundName}</Typography>
            {blueTeam?.name && goldTeam?.name && (
              <Typography>
                {blueTeam.name} vs. {goldTeam.name}
              </Typography>
            )}
          </CardContent>
        </Card>
      </Grid>
    );
  };

  const activeBlueTeam = teamsById[activeMatch.blueTeam];
  const activeGoldTeam = teamsById[activeMatch.goldTeam];

  return (
    <Dialog open={open} onClose={onClose} fullWidth maxWidth="lg">
      <DialogTitle>{cabinet.displayName}</DialogTitle>

      <DialogContent>
        <Grid container direction="row" spacing={2}>
          <Grid item container direction="row" className={classes.gameStats}>
            <Grid item className={classes.subtitleItem}>
              <Icon path={mdiMap} /> {MAP_NAMES[stats?.mapName] ?? stats?.mapName}
            </Grid>
            <Grid item className={classes.subtitleItem}>
              <Icon path={mdiTimer} /> {formatUTC((stats?.gameTime ?? 0) * 1000, 'm:ss')}
            </Grid>
            <Grid item className={classes.subtitleItem}>
              <Icon path={mdiFruitGrapes} /> {stats?.berriesRemaining}
            </Grid>
          </Grid>

          {activeMatch?.id && (
            <>
              <Grid item container direction="row" xs={12} spacing={2} justifyContent="center">
                <Grid item>
                  <Card className={classes.scoreUpdaterBlue}>
                    <ScoreUpdater
                      title={activeBlueTeam?.name ?? ''}
                      value={activeMatch.blueScore}
                      onChange={updateBlueScore}
                    />
                  </Card>
                </Grid>
                <Grid item>
                  <Card className={classes.scoreUpdaterGold}>
                    <ScoreUpdater
                      title={activeGoldTeam?.name ?? ''}
                      value={activeMatch.goldScore}
                      onChange={updateGoldScore}
                    />
                  </Card>
                </Grid>
              </Grid>
            </>
          )}

          <Grid item className={classes.spacer} xs={12} />

          <TournamentGameSelector
            tournament={tournament}
            cabinet={cabinet}
            activeMatch={activeMatch}
          />

          {activeMatch?.id && (
            <Grid item xs={12}>
              <FormGroup className={classes.warmupSwitch}>
                <FormControlLabel
                  label="Warm-up Game"
                  control={
                    <Switch
                      onChange={toggleWarmup}
                      checked={activeMatch?.isWarmup}
                      disabled={isLoading}
                    />
                  }
                />
              </FormGroup>
            </Grid>
          )}

          {(matches === null || matches.length == 0) && (
            <Typography>No available matches found. Check your bracket on Challonge.</Typography>
          )}

          {matches?.length > 0 && (
            <>
              <MatchCard match={null} />

              {matches.map(match => (
                <MatchCard key={match.id} match={match} />
              ))}
            </>
          )}
        </Grid>
      </DialogContent>

      <DialogActions>
        <Button
          variant="outlined"
          color="primary"
          onClick={flip}
          disabled={isLoading}
          startIcon={<Icon size={1} path={mdiSwapHorizontalBold} />}
        >
          Swap Blue/Gold
        </Button>
        <Button
          variant="outlined"
          color="primary"
          onClick={loadAvailableMatches}
          startIcon={<Icon size={1} path={mdiRefresh} />}
          disabled={loading}
        >
          Reload Matches
        </Button>
        <Button variant="outlined" color="primary" onClick={onClose}>
          Close
        </Button>
      </DialogActions>
    </Dialog>
  );
}
