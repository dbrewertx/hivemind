import {
  Button,
  CircularProgress,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
  FormControl,
  Grid,
  IconButton,
  Typography,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@mdi/react';
import clsx from 'clsx';
import { useFormik } from 'formik';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import React, { useContext, useState } from 'react';
import { toast } from 'react-toastify';

import DeleteButton from 'components/DeleteButton';
import { useUser } from 'util/auth';
import { getAxios } from 'util/axios';

const useStyles = makeStyles(theme => ({
  formControl: {
    width: '100%',
    overflow: 'hidden',
  },
  dialogActions: {
    padding: '0 24px 24px',
  },
  deleteButton: {
    marginRight: 'auto',
  },
}));

export const FormContext = React.createContext({});

export default function Form(props) {
  const {
    dialog,
    showButton,
    open,
    buttonText,
    buttonIconPath,
    buttonVariant,
    buttonColor,
    buttonClass,
    canEdit,
    dialogTitle,
    saveButtonText,
    closeButtonText,
    deleteButtonText,
    object,
    url,
    method,
    getPostData,
    onSave,
    onClose,
    closeOnSave,
    successText,
    deleteConfirmText,
    showDeleteButton,
    dialogActions,
    getErrorMessage,
    className,
    children,
    containerClass,
    hideSaveButton,
  } = props;

  const classes = useStyles();
  const axios = getAxios({ authenticated: true });
  const [isOpen, setIsOpen] = useState(false);
  const user = useUser();
  const router = useRouter();

  const editable = Boolean(canEdit(object));

  const handleOpen = () => {
    setIsOpen(true);
  };

  const handleClose = () => {
    setIsOpen(false);
    if (onClose) {
      onClose();
    }
  };

  const handleSubmit = async values => {
    let postdata = {};

    for (const [field, value] of Object.entries(values)) {
      if (getPostData[field]) {
        postdata[field] = await getPostData[field](values);
      } else {
        postdata[field] = value;
      }
    }

    if (props.validationSchema) {
      postdata = props.validationSchema.cast(postdata);
    }

    let response;
    if (url !== null) {
      try {
        response = await axios({ method: method, url: url, data: postdata });
      } catch (err) {
        if (err.response?.data && err.response?.status == 400) {
          toast.error(getErrorMessage(err.response.data));
          formik.setErrors(err.response.data);
        } else {
          toast.error('An unknown error occurred. Please try again.');
        }

        return;
      }
    }

    await onSave(response?.data ?? values, formik);
    toast(successText(response?.data));
    formik.setSubmitting(false);

    if (closeOnSave) {
      handleClose();
      router.replace(router.asPath);
    }

    return response?.data ?? values;
  };

  const handleDelete = async () => {
    formik.setSubmitting(true);
    const response = await axios.delete(url);
    toast(response.data);
    formik.setSubmitting(false);

    if (closeOnSave) {
      handleClose();
      router.replace(router.asPath);
    }
  };

  const formik = useFormik({
    initialValues: object,
    onSubmit: handleSubmit,
    ...props,
  });

  if (!editable) {
    if (editable === null) {
      return <CircularProgress />;
    }

    if (dialog) {
      return <></>;
    } else {
      return <Typography>You do not have permission to edit this page.</Typography>;
    }
  }

  if (dialog) {
    return (
      <div suppressHydrationWarning>
        {showButton && (
          <>
            {buttonIconPath && (
              <IconButton
                className="text-gray-400 hover:text-gray-500 bg-transparent"
                onClick={handleOpen}
              >
                <Icon path={buttonIconPath} size={1} />
              </IconButton>
            )}

            {!buttonIconPath && (
              <Button variant={buttonVariant} color={buttonColor} onClick={handleOpen}>
                {buttonText}
              </Button>
            )}
          </>
        )}

        <Dialog
          open={open ?? isOpen}
          onClose={handleClose}
          className={clsx(classes.dialog, className)}
          fullWidth
          maxWidth="md"
        >
          <FormControl className={classes.formControl}>
            <FormContext.Provider value={formik}>
              <DialogTitle>{dialogTitle}</DialogTitle>
              <DialogContent className={classes.dialogContent}>
                <Grid container direction="column" className={classes.children}>
                  {typeof children === 'function' ? children({ formik }) : children}
                </Grid>
              </DialogContent>
              <DialogActions className={classes.dialogActions}>
                {dialogActions}
                {showDeleteButton && (
                  <DeleteButton
                    buttonText={deleteButtonText}
                    confirmText={deleteConfirmText}
                    onConfirm={handleDelete}
                    className={clsx(classes.deleteButton, 'text-red-500 border-red-500')}
                  />
                )}
                <Button variant="outlined" color="default" onClick={handleClose}>
                  {closeButtonText}
                </Button>
                <Button variant="contained" color="secondary" onClick={formik.handleSubmit}>
                  {formik.isSubmitting ? <CircularProgress size={24} /> : saveButtonText}
                </Button>
              </DialogActions>
            </FormContext.Provider>
          </FormControl>
        </Dialog>
      </div>
    );
  } else {
    return (
      <div suppressHydrationWarning>
        <FormControl className={clsx(classes.formControl, className)}>
          <FormContext.Provider value={formik}>
            <Grid container direction="column" className={clsx(classes.children, containerClass)}>
              {typeof children === 'function' ? children({ formik }) : children}
            </Grid>

            {!hideSaveButton && (
              <Button
                className={buttonClass}
                variant={buttonVariant}
                color="secondary"
                onClick={formik.handleSubmit}
              >
                {saveButtonText}
              </Button>
            )}
          </FormContext.Provider>
        </FormControl>
      </div>
    );
  }
}

Form.propTypes = {
  dialog: PropTypes.bool,
  showButton: PropTypes.bool,
  hideSaveButton: PropTypes.bool,
  open: PropTypes.bool,
  buttonText: PropTypes.string,
  buttonIconPath: PropTypes.object,
  buttonColor: PropTypes.string,
  buttonClass: PropTypes.string,
  canEdit: PropTypes.func,
  dialogTitle: PropTypes.string,
  closeButtonText: PropTypes.string,
  saveButtonText: PropTypes.string,
  deleteButtonText: PropTypes.string,
  object: PropTypes.object.isRequired,
  url: PropTypes.string,
  method: PropTypes.string,
  getPostData: PropTypes.object,
  onSave: PropTypes.func,
  onClose: PropTypes.func,
  closeOnSave: PropTypes.bool,
  successText: PropTypes.func,
  deleteConfirmText: PropTypes.string,
  dialogActions: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
  getErrorMessage: PropTypes.func,
  className: PropTypes.string,
  containerClass: PropTypes.string,
  children: PropTypes.oneOfType([
    PropTypes.func,
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.node,
  ]).isRequired,
};

Form.defaultProps = {
  dialog: true,
  showButton: true,
  hideSaveButton: false,
  open: undefined,
  method: 'put',
  getPostData: {},
  buttonText: 'Edit',
  buttonIconPath: null,
  buttonVariant: 'outlined',
  buttonColor: 'secondary',
  dialogTitle: '',
  closeButtonText: 'Close',
  saveButtonText: 'Save',
  deleteButtonText: 'Delete',
  url: null,
  canEdit: async () => true,
  onSave: data => {},
  onClose: null,
  closeOnSave: true,
  successText: data => 'Saved',
  deleteConfirmText: 'Delete this?',
  dialogActions: <></>,
  getErrorMessage: data => (
    <>
      {Object.keys(data).map(k => (
        <p key={k}>
          <b>{k}</b>: {data[k]}
        </p>
      ))}
    </>
  ),
  className: null,
  containerClass: null,
};

export const useForm = () => useContext(FormContext);
