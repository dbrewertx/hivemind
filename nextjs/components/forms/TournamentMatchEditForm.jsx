import React, { useState, useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Icon from '@mdi/react';
import { Button, Typography, Grid } from '@material-ui/core';
import { mdiCloudSync } from '@mdi/js';
import { toast } from 'react-toastify';
import clsx from 'clsx';
import { format } from 'date-fns';

import { TOURNAMENT_LINK_TYPES } from 'util/constants';
import Form, { useForm } from './Form';
import List from './List';
import Field, { TextField, Switch } from '../fields/Field';
import { GameMeta } from 'components/tables/GameTable';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';

const useStyles = makeStyles(theme => ({}));

export function ReportResultFunction({ match }) {
  const axios = getAxios({ authenticated: true });
  const formik = useForm();
  const [isLoading, setIsLoading] = useState(false);

  const reportResult = async () => {
    setIsLoading(true);
    await axios.put(`/api/tournament/match/${match.id}/report/`);
    setIsLoading(false);
    toast.success('Match result sent.');
  };

  return (
    <Button
      variant="outlined"
      color="secondary"
      onClick={reportResult}
      disabled={isLoading}
      startIcon={<Icon size={1} path={mdiCloudSync} />}
    >
      Report Result
    </Button>
  );
}

export default function TournamentMatchEditForm({ tournament, match, isOpen, onClose }) {
  const classes = useStyles();
  const axios = getAxios({ authenticated: true });
  const [games, setGames] = useState([]);

  const blueTeamName = tournament?.teamsById[match.blueTeam]?.name ?? '(Unknown Team)';
  const goldTeamName = tournament?.teamsById[match.goldTeam]?.name ?? '(Unknown Team)';

  const addGame = async gameId => {
    let response = await axios.get(`/api/game/game/${gameId}/`);
    const game = response.data;

    if (game?.cabinet?.scene !== tournament.scene.id) {
      toast.error('This game is not associated with the same scene as this tournament.');
      return false;
    }

    if (game?.tournamentMatch !== null) {
      toast.error('This game is already part of a tournament match.');
      return false;
    }

    response = await axios.patch(`/api/game/game/${gameId}/`, { tournamentMatch: match.id });

    setGames(v => [...v, game]);
  };

  const deleteGame = async game => {
    await axios.patch(`/api/game/game/${game.id}/`, { tournamentMatch: null });
    setGames(v => v.filter(g => g.id !== game.id));
  };

  const formProps = {
    dialogTitle: `Editing Tournament Match: ${blueTeamName} vs. ${goldTeamName}`,
    canEdit: match => isAdminOf(tournament.scene.id),
    object: match,
    url: `/api/tournament/match/${match.id}/`,
    method: 'PATCH',
    enableReinitialize: true,
    onClose,
    open: isOpen,
    showButton: false,
  };

  if (tournament.linkType == TOURNAMENT_LINK_TYPES.CHALLONGE) {
    formProps.dialogActions = <ReportResultFunction match={match} />;
  }

  useEffect(() => {
    axios
      .getAllPages(`/api/game/game/`, { params: { tournamentMatchId: match.id } })
      .then(setGames);
  }, [isOpen]);

  if (!isOpen) {
    return <></>;
  }

  return (
    <Form {...formProps}>
      <Grid container spacing={2}>
        <Grid item xs={12} md={6}>
          <Field name="blueScore" label="Blue Score" component={TextField} type="number" />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field name="goldScore" label="Gold Score" component={TextField} />
        </Grid>
      </Grid>

      <Field name="isComplete" label="Match is Complete" component={Switch} />
      <Field name="videoLink" label="Video URL" component={TextField} />

      <List
        title="Game IDs"
        getItemText={row => row.id}
        items={games}
        onAdd={addGame}
        onDelete={deleteGame}
        newItemPlaceholderText="Game ID"
      />
    </Form>
  );
}
