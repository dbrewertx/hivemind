import React, { useEffect, useState } from 'react';
import { MenuItem, Typography } from '@material-ui/core';
import { parseISO, sub } from 'date-fns';

import Form from './Form';
import Field, { TextField, Select } from '../fields/Field';
import { useTournamentAdmin } from 'providers/TournamentAdmin';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';

export default function TournamentVideoForm({}) {
  const axios = getAxios({ authenticated: true });
  const [cabinets, setCabinets] = useState(null);
  const [gameEndTime, setGameEndTime] = useState(null);
  const { tournament } = useTournamentAdmin();

  useEffect(() => {
    axios
      .getAllPages('/api/game/cabinet/', { params: { sceneId: tournament?.scene?.id } })
      .then(setCabinets);
  }, [tournament.scene]);

  const postdata = {
    game: '',
    tournament: tournament.id,
    videoId: '',
    gameEndTime: '',
    length: '',
  };

  const props = {
    buttonText: 'Add Video',
    buttonColor: 'default',
    dialogTitle: 'Adding Video',
    canEdit: () => isAdminOf(tournament.scene.id),
    object: postdata,
    method: 'POST',
    url: '/api/tournament/video/add/',
  };

  return (
    <Form {...props}>
      <Field name="videoId" label="YouTube Video ID" component={TextField} />
      <Field name="length" label="Video Length" component={TextField} />

      <Typography>
        Find any postgame screen from the video. Enter the Game ID and the timestamp in the video
        when that screen appeared.
      </Typography>

      <Field name="game" label="Game ID" component={TextField} />
      <Field name="gameEndTime" label="Game End Time" component={TextField} />
    </Form>
  );
}
