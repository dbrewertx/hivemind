import { makeStyles } from '@material-ui/core/styles';
import { useState } from 'react';
import { toast } from 'react-toastify';
import * as Yup from 'yup';

import { UserConsumer } from 'util/auth';
import { getAxios } from 'util/axios';
import NFC from '../buttons/NFC';
import Field, {
  Autocomplete,
  ImageUploadField,
  Switch,
  TextField,
  imageURLRegex,
} from '../fields/Field';
import { SceneSelect } from '../fields/SceneSelect';
import Form from './Form';
import List from './List';

const useStyles = makeStyles(theme => ({
  sceneMenuItem: {
    opacity: '0.8',
    borderBottom: '2px solid transparent',
    '&:hover': {
      opacity: 1,
      borderBottom: `2px solid ${theme.palette.text.primary}`,
    },
  },
}));

const UserSchema = Yup.object().shape({
  name: Yup.string().max(30, 'Too long'),
  image: Yup.lazy(value =>
    /^data/.test(value)
      ? Yup.string()
          .trim()
          .matches(
            /^data:([a-z]+\/[a-z0-9-+.]+(;[a-z-]+=[a-z0-9-]+)?)?(;base64)?,([a-z0-9!$&',()*+;=\-._~:@/?%\s]*)$/i,
            'Must be a valid data URI',
          )
          .nullable(true)
      : Yup.string()
          .trim()
          .matches(
            /^(?:([a-z0-9+.-]+):\/\/)(?:\S+(?::\S*)?@)?(?:(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/,
            'Must be a valid URL',
          )
          .nullable(true),
  ),
});

const UserForm = UserConsumer(({ user, obj }) => {
  const axios = getAxios({ authenticated: true });
  const classes = useStyles();
  const [nfcs, setNfcs] = useState(null);
  const [allScenes, setAllScenes] = useState(null);

  if (!user || user.id != obj.id) {
    return <></>;
  }

  const props = {
    buttonText: 'Edit',
    buttonVariant: 'contained',
    dialogTitle: `Editing User Profile: ${obj.name}`,
    object: obj,
    validationSchema: UserSchema,
    url: `/api/user/user/${obj.id}/`,
    method: 'PATCH',
    getPostData: {
      image: async data => {
        if (data.image === null) return undefined;
        return imageURLRegex.test(data?.image) ? undefined : data?.image;
      },
    },
    initialData: {
      ...user,
    },
  };

  const getNfcList = () => {
    axios
      .get('/api/user/nfc/')
      .then(response => {
        setNfcs(response.data.results);
      })
      .catch(error => {
        setNfcs([]);
      });
  };

  if (nfcs === null) {
    getNfcList();
  }

  const addNfc = async cardId => {
    const postdata = { cardId: cardId.replace(/[\:\-/s]/, '').toLowerCase() };

    try {
      const response = await axios.post('/api/user/nfc/', postdata);
      setNfcs([...nfcs, response.data]);
    } catch (error) {
      if (error.response?.data?.nfc) {
        const errorText = error.response.data.nfc.join(', ');
        toast.error(`Couldn't add new NFC card: ${errorText}`);
        return errorText;
      }

      throw error;
    }
  };

  const removeNfc = nfc => {
    axios.delete(`/api/user/nfc/${nfc.id}/`).then(response => {
      getNfcList();
    });
  };

  return (
    <Form {...props}>
      <div className="grid grid-cols-1 md:grid-cols-2 gap-x-8 gap-y-4 mb-4">
        <Field name="name" label="Name" component={TextField} />
        <Field
          component={Autocomplete}
          defaultValue=""
          name="pronouns"
          label="Pronouns"
          freeSolo
          options={['he/him', 'she/her', 'they/them']}
          helperText="Choose an option or enter your own."
          noGrid
        />
        <SceneSelect />
        <Field name="isProfilePublic" label="Make Profile Public" component={Switch} noGrid />
        <Field
          name="image"
          label="Picture"
          component={ImageUploadField}
          helperText="Upload a picture of yourself. May be used on stream."
          required={false}
        />
      </div>

      <List
        title="NFC Tags"
        isLoading={nfcs === null}
        getItemText={item => item.cardId}
        items={nfcs}
        onAdd={addNfc}
        onDelete={removeNfc}
        buttons={[<NFC onScan={addNfc} />]}
      />
    </Form>
  );
});

export default UserForm;
