import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import { useRouter } from 'next/router';
import { useState } from 'react';
import * as Yup from 'yup';

import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import { TOURNAMENT_LINK_TYPES } from 'util/constants';
import Field, { DateField, Radio, RadioGroup, Switch, TextField } from '../fields/Field';
import Form from './Form';

const useStyles = makeStyles(theme => ({
  loading: {
    cursor: 'wait',
  },
  payOptionsContainer: {
    padding: theme.spacing(1, 2, 2),
    backgroundColor: '#f3f3f3',
    borderRadius: '5px',
    display: 'flex',
    flexDirection: 'column',
  },
}));

const TournamentSchema = Yup.object().shape({
  name: Yup.string().required('Tournament name is required'),
  date: Yup.string()
    .nullable(true)
    .transform(v => v || null)
    .required('Tournament date is required'),
  description: Yup.string(),
  isActive: Yup.bool()
    .default(false)
    .transform(v => Boolean(v)),
});

export default function TournamentForm({ tournament }) {
  const axios = getAxios({ authenticated: true });
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const router = useRouter();

  const props = {
    buttonText: tournament.id ? 'Edit' : 'Create New',
    buttonVariant: tournament.id ? 'contained' : 'outlined',
    dialogTitle: tournament.id ? `Editing Tournament: ${tournament.name}` : 'Create New Tournament',
    canEdit: tournament => isAdminOf(tournament.scene.id),
    object: tournament,
    validationSchema: TournamentSchema,
    url: tournament.id
      ? `/api/tournament/tournament/${tournament.id}/`
      : '/api/tournament/tournament/',
    method: tournament.id ? 'PUT' : 'POST',
    getPostData: {
      scene: values => values.scene.id,
    },
    onSave: result => {
      if (!tournament.id) {
        router.push(`/tournament/${result.id}/admin`);
      }
    },
    showDeleteButton: false,
    className: clsx({ [classes.loading]: loading }),
  };

  return (
    <Form {...props}>
      <div className="grid grid-cols-2 items-center gap-8">
        <Field name="name" label="Tournament Name" component={TextField} noGrid />

        <div className="flex gap-8 items-center">
          <Field name="date" label="Date" component={DateField} containerClass="my-0" noGrid />

          {tournament.id && (
            <Field name="isActive" label="Active" component={Switch} containerClass="my-0" />
          )}
        </div>
      </div>
      <Field name="description" label="Description" component={TextField} multiline rows={5} />

      {!tournament.id && (
        <Field name="linkType" label="Tournament Integration" component={RadioGroup}>
          <Radio
            value={TOURNAMENT_LINK_TYPES.MANUAL}
            label="Manual"
            helperText="Team names are entered manually. Tournament operators select the teams that are playing."
          />
          <Radio
            value={TOURNAMENT_LINK_TYPES.CHALLONGE}
            label="Challonge"
            helperText="Team names and matches are retrieved from the Challonge API. Tournament operators select the match that is being played. Scores are reported to Challonge automatically."
          />
        </Field>
      )}
    </Form>
  );
}
