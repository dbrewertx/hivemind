import { Typography } from '@material-ui/core';
import { useRouter } from 'next/router';

import Form from './Form';
import Field, { TextField } from '../fields/Field';
import { isAdminOf } from 'util/auth';

export default function EventGenerateTeamsForm({ event }) {
  const router = useRouter();
  const postdata = { event: event.id, numTeams: Math.ceil(event.unassignedPlayers.length / 5) };
  const props = {
    buttonText: event.isQuickplay ? 'Start Event' : 'Generate Teams',
    dialogTitle: event.isQuickplay ? 'Start Event' : 'Generate Teams',
    canEdit: async () => true,
    object: postdata,
    url: `/api/league/event/${event.id}/generate-teams/`,
    method: 'PUT',
    saveButtonText: event.isQuickplay ? 'Start' : 'Generate',
    onSave: result => { router.reload(); },
  };

  return (
    <Form {...props}>
      <Typography variant="subtitle1">Player Count: {event.unassignedPlayers.length}</Typography>

      {!event.isQuickplay && (
        <Field name="numTeams" label="Number of Teams" component={TextField} type="number" />
      )}
    </Form>
  );
}
