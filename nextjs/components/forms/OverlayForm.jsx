import { Grid, MenuItem, Typography } from '@material-ui/core';

import * as Yup from 'yup';

import { allIngameThemes } from 'overlay/ingame/themes';
import { allMatchThemes } from 'overlay/match/themes';
import { allMatchPreviewThemes } from 'overlay/matchPreview/themes';
import { allPostgameThemes } from 'overlay/postgame/themes';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import Field, { Divider, Select, Switch, TextField } from '../fields/Field';
import Form from './Form';
import SwapFieldsButton from './SwapFieldsButton';

const OverlaySchema = Yup.object().shape({});

export default function OverlayForm({ overlay }) {
  const axios = getAxios();

  const props = {
    dialog: false,
    canEdit: overlay => isAdminOf(overlay.cabinet.scene.id),
    object: overlay,
    validationSchema: OverlaySchema,
    url: `/api/overlay/overlay/${overlay.id}/`,
    closeOnSave: false,
    getPostData: {
      cabinet: values => overlay.cabinet.id,
      theme: values => values.themeId,
    },
  };

  const swapTeamNames = () => {
    const { goldTeam, blueTeam } = overlay;
    const tempGold = goldTeam;
    overlay['goldTeam'] = blueTeam;
    overlay['blueTeam'] = tempGold;
  };

  return (
    <Form
      {...props}
      className="relative overflow-visible"
      buttonClass="absolute bottom-full right-0 mb-6"
      buttonVariant="contained"
    >
      <Grid container direction="row" spacing={0}>
        <Grid item xs={12} md>
          <Field name="blueTeam" label="Blue Team Display Name" component={TextField} />
          <Field name="blueScore" label="Blue Score" component={TextField} type="number" />
        </Grid>
        <Grid item md={'auto'}>
          <SwapFieldsButton field1="blueTeam" field2="goldTeam" />
        </Grid>
        <Grid item xs={12} md>
          <Field name="goldTeam" label="Gold Team Display Name" component={TextField} />
          <Field name="goldScore" label="Gold Score" component={TextField} type="number" />
        </Grid>
      </Grid>

      <Divider />
      <Typography variant="h3" className="mb-4">
        Series Settings
      </Typography>
      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} md={6}>
          <Field
            name="showScore"
            label="Show Scores"
            component={Switch}
            helperText="Show these scores on the overlay when a league event or tournament is not active."
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field
            name="updateScore"
            label="Auto-Update Score"
            component={Switch}
            helperText="If checked, scores will automatically update when a game ends."
          />
        </Grid>
      </Grid>
      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} md={6}>
          <Field
            name="matchWinMax"
            label="How many games to win the series?"
            component={TextField}
            type="number"
            helperText="Sets the number of empty match markers displayed if the chosen overlay supports it."
          />
        </Grid>
      </Grid>

      <Divider />

      <Typography variant="h3">Advanced Settings</Typography>

      <Field name="name" label="Overlay Name" component={TextField} />

      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} md={6}>
          <Field name="ingameTheme" label="In-Game Theme" component={Select}>
            {allIngameThemes.map(theme => (
              <MenuItem key={theme.themeProps.name} value={theme.themeProps.name}>
                {theme.themeProps.displayName}
              </MenuItem>
            ))}
          </Field>
        </Grid>
        <Grid item xs={12} md={6}>
          <Field name="postgameTheme" label="Postgame Theme" component={Select}>
            {allPostgameThemes.map(theme => (
              <MenuItem key={theme.themeProps.name} value={theme.themeProps.name}>
                {theme.themeProps.displayName}
              </MenuItem>
            ))}
          </Field>
        </Grid>
        <Grid item xs={12} md={6}>
          <Field name="matchTheme" label="Match Summary Theme" component={Select}>
            {allMatchThemes.map(theme => (
              <MenuItem key={theme.themeProps.name} value={theme.themeProps.name}>
                {theme.themeProps.displayName}
              </MenuItem>
            ))}
          </Field>
        </Grid>
        <Grid item xs={12} md={6}>
          <Field name="matchPreviewTheme" label="MatchPreview Theme" component={Select}>
            {allMatchPreviewThemes.map(theme => (
              <MenuItem key={theme.themeProps.name} value={theme.themeProps.name}>
                {theme.themeProps.displayName}
              </MenuItem>
            ))}
          </Field>
        </Grid>
      </Grid>

      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} md={6}>
          <Field name="showPlayers" label="Show Player Names" component={Switch} />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field
            name="showChat"
            label="Show Twitch Chat"
            component={Switch}
            helperText="Show Twitch chat in stream, if supported by the chosen overlay. Channel name must be set in the cabinet settings menu."
          />
        </Grid>
      </Grid>
      <Grid container direction="row" spacing={2}>
        <Grid item xs={12} md={6}>
          <Field
            name="goldOnLeft"
            label="Gold on Left"
            component={Switch}
            helperText="Reverse the overlay, moving the gold team to the left side."
          />
        </Grid>
        <Grid item xs={12} md={6}>
          <Field
            name="additionalFonts"
            label="Additional Fonts"
            helperText="must be a url that works in a link tag rel='stylesheet'"
            component={TextField}
          />
        </Grid>
      </Grid>
      <Divider />
    </Form>
  );
}
