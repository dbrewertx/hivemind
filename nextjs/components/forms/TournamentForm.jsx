import { Button, Grid, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import CloseIcon from '@material-ui/icons/Close';
import EditIcon from '@material-ui/icons/Edit';
import clsx from 'clsx';
import Title from 'components/Title';
import TournamentVideoForm from 'components/forms/TournamentVideoForm';
import { useRouter } from 'next/router';
import { useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import { TOURNAMENT_LINK_TYPES } from 'util/constants';
import * as Yup from 'yup';

import Field, {
  Conditional,
  DateField,
  ImageUploadField,
  Radio,
  RadioGroup,
  Switch,
  TextField,
  imageURLRegex,
} from '../fields/Field';
import Form, { useForm } from './Form';
import List from './List';

const useStyles = makeStyles(theme => ({
  loading: {
    cursor: 'wait',
  },
  payOptionsContainer: {
    padding: theme.spacing(1, 2, 2),
    backgroundColor: '#f3f3f3',
    borderRadius: '5px',
    display: 'flex',
    flexDirection: 'column',
  },
}));

const TournamentSchema = Yup.object().shape({
  name: Yup.string().required('Tournament name is required'),
  date: Yup.string()
    .nullable(true)
    .transform(v => v || null)
    .required('Tournament date is required'),
  description: Yup.string(),
  isActive: Yup.bool()
    .default(false)
    .transform(v => Boolean(v)),
  allowRegistration: Yup.bool()
    .default(false)
    .transform(v => Boolean(v)),
  registrationBanner: Yup.lazy(value =>
    /^data/.test(value)
      ? Yup.string()
          .trim()
          .matches(
            /^data:([a-z]+\/[a-z0-9-+.]+(;[a-z-]+=[a-z0-9-]+)?)?(;base64)?,([a-z0-9!$&',()*+;=\-._~:@/?%\s]*)$/i,
            'Must be a valid data URI',
          )
          .nullable()
      : Yup.string().trim().matches(imageURLRegex, 'Must be a valid URL').nullable(),
  ),
});
const PaymentOptionSchema = Yup.object().shape({
  name: Yup.string().required('Option name is required'),
  price: Yup.number().positive(),
});

export function ConfigureStripeButton({ tournament }) {
  const axios = getAxios({ authenticated: true });
  const router = useRouter();
  const formik = useForm();

  const configureStripe = async () => {
    const savedTournament = await formik.submitForm();

    axios.put(`/api/tournament/tournament/${savedTournament.id}/stripe/`).then(response => {
      window.open(response.data.url, '_blank');
    });
  };

  return (
    <div className="">
      <Button
        variant="outlined"
        color="secondary"
        onClick={configureStripe}
        disabled={formik.isSubmitting}
      >
        {tournament?.stripeConfigured ? 'Edit' : 'Configure'} Stripe
      </Button>
    </div>
  );
}

export default function TournamentForm({ tournament, onSave }) {
  const axios = getAxios({ authenticated: true });
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [editing, setEditing] = useState(false);
  const [paymentOptions, setPaymentOptions] = useState(null);
  const [unsavedPaymentOptions, setUnsavedPaymentOptions] = useState([]);
  const router = useRouter();

  const addPaymentOption = async (v, { values }) => {
    try {
      if (!values.name && !values.price) {
        toast.error("Payment option name and price can't be blank");
        throw new Error("Payment option name and price can't be blank");
      }

      const postdata = {
        tournament: tournament.id,
        name: values.name,
        price: values.price,
      };

      if (tournament?.id) {
        const response = await axios.post('/api/tournament/payment-option/', postdata);
        setPaymentOptions(opts => [...opts, response.data]);
        return response;
      } else {
        setUnsavedPaymentOptions(opts => [...opts, postdata]);
        setPaymentOptions(opts => [...opts, postdata]);
      }
    } catch (e) {
      console.log(e);
      toast.error(e);
    }
  };

  const deletePaymentOption = async paymentOption => {
    if (tournament?.id) {
      axios.delete(`/api/tournament/payment-option/${paymentOption.id}/`).then(response => {
        getPaymentOptions();
      });
    } else {
      setUnsavedPaymentOptions(unsavedPaymentOptions.filter(item => item.id !== paymentOption.id));
      setPaymentOptions(paymentOptions.filter(item => item.id !== paymentOption.id));
    }
  };

  const getPaymentOptions = () => {
    if (tournament?.id) {
      axios
        .get('/api/tournament/payment-option/', { params: { tournamentId: tournament.id } })
        .then(response => {
          setPaymentOptions(response.data.results);
        });
    }
  };

  useEffect(() => {
    if (tournament?.id && paymentOptions === null) {
      getPaymentOptions();
    }

    if (!tournament?.id && paymentOptions === null) {
      setPaymentOptions([]);
    }
  }, []);

  const props = {
    dialog: false,
    canEdit: tournament => isAdminOf(tournament.scene.id),
    object: tournament,
    validationSchema: TournamentSchema,
    url: `/api/tournament/tournament/${tournament.id}/`,
    method: 'PUT',
    getPostData: {
      registrationBanner: async data =>
        imageURLRegex.test(data?.registrationBanner) || data?.registrationBanner === null
          ? undefined
          : data?.registrationBanner,
      scene: values => values.scene.id,
    },
    onSave: result => {
      setEditing(false);
    },
    showDeleteButton: true,
    className: clsx({ [classes.loading]: loading }, 'overflow-visible'),
  };
  return (
    <Form hideSaveButton={true} {...props}>
      {({ formik }) => {
        return (
          <>
            <Title
              title={
                <>
                  <IconButton onClick={() => setEditing(!editing)} className="-ml-3 mr-1">
                    {editing ? <CloseIcon /> : <EditIcon />}
                  </IconButton>
                  <span>{tournament.name}</span>
                  <span className="ml-4 text-sm font-mono p-2 rounded bg-gray-200 align-middle">
                    {new Date(tournament.date).toDateString()}
                  </span>
                </>
              }
              form={
                <Grid container direction="row" spacing={2}>
                  <Grid item>
                    {formik.dirty && (
                      <Button
                        variant="contained"
                        color="secondary"
                        onClick={formik.handleSubmit}
                        className="animate-fadein"
                      >
                        Save
                      </Button>
                    )}
                  </Grid>
                  <Grid item>
                    <TournamentVideoForm tournament={tournament} />
                  </Grid>
                  <Grid item>
                    <ConfigureStripeButton tournament={tournament} />
                  </Grid>
                  <Grid item>
                    <Button
                      variant="contained"
                      color="secondary"
                      href={`/tournament/${tournament.id}/configure-registration`}
                    >
                      Configure Registration
                    </Button>
                  </Grid>
                </Grid>
              }
            />

            {editing && (
              <div className="mt-4">
                <div>
                  <div className="grid grid-cols-1 md:grid-cols-2 items-center gap-8">
                    <Field name="name" label="Tournament Name" component={TextField} noGrid />

                    <Field
                      name="date"
                      label="Date"
                      component={DateField}
                      containerClass="md:my-0"
                    />
                  </div>
                  <Field
                    name="description"
                    label="Description"
                    component={TextField}
                    multiline
                    rows={5}
                  />
                </div>
              </div>
            )}
            <div className="grid md:grid-cols-2 gap-8  mt-4">
              <div>
                <Field
                  name="allowRegistration"
                  label="Enable Player Registration"
                  component={Switch}
                />
              </div>
              <div>
                <Field
                  name="registrationCloseDate"
                  label="Registration Closes On"
                  component={DateField}
                  disabled={!formik.values['allowRegistration']}
                />
              </div>
              <div>
                <Field
                  name="registrationBanner"
                  label="Registration Banner"
                  component={ImageUploadField}
                  helperText="Upload a banner to appear on top of the registration page."
                  noGrid
                />
              </div>
              <div>
                <Field
                  name="registrationMessage"
                  label="Registration Message"
                  component={TextField}
                  multiline
                  minRows={6}
                  helperText="Add a message to appear on top of the registration page. (markdown supported)"
                  noGrid
                />
              </div>
            </div>
            <div className=" grid grid-cols-1 md:grid-cols-2 gap-8 mt-8">
              <Field
                name="acceptPayments"
                label="Accept Payments With Stripe"
                disabled={!(formik.values['allowRegistration'] && tournament.stripeConfigured)}
                helperText={
                  !tournament.stripeConfigured
                    ? 'Stripe needs to be configured before payments can be accepted.'
                    : ''
                }
                component={Switch}
              />
            </div>

            <Conditional test={v => v.allowRegistration && v.acceptPayments}>
              <Field
                name="paymentMessage"
                label="Payment Message"
                component={TextField}
                multiline
                minRows={4}
                helperText="Add a message to appear on top of the payment section (markdown supported)"
              />

              <div className=" grid grid-cols-1 md:grid-cols-2 gap-8 ">
                <div>
                  <List
                    className="flex flex-col"
                    title="Ticketing Options"
                    fieldsSchema={PaymentOptionSchema}
                    fields={[
                      {
                        name: 'name',
                        placeholder: 'Option Name e.g. General Admission',
                        required: true,
                      },

                      { name: 'price', placeholder: 'Price', type: 'number' },
                    ]}
                    isLoading={paymentOptions === null}
                    getItemText={item => (
                      <>
                        {item.name} - <b>${item.price}</b>
                      </>
                    )}
                    noItemsText="Add tickets/price tiers for your tournament."
                    onAdd={addPaymentOption}
                    onDelete={deletePaymentOption}
                  />
                </div>
                <div>
                  <List
                    title="Added Options"
                    isLoading={paymentOptions === null}
                    getItemText={item => (
                      <div className="grid grid-cols-3">
                        <div className="col-span-2">{item.name}</div>
                        <div className="font-bold">${item.price}</div>
                      </div>
                    )}
                    noItemsText=""
                    onDelete={deletePaymentOption}
                    items={paymentOptions}
                    className="divide-y divide-gray-200 divide-solid"
                    listItemClass="px-0 "
                  />
                </div>
              </div>
            </Conditional>

            {!tournament.id && (
              <Field name="linkType" label="Tournament Integration" component={RadioGroup}>
                <Radio
                  value={TOURNAMENT_LINK_TYPES.MANUAL}
                  label="Manual"
                  helperText="Team names are entered manually. Tournament operators select the teams that are playing."
                />
                <Radio
                  value={TOURNAMENT_LINK_TYPES.CHALLONGE}
                  label="Challonge"
                  helperText="Team names and matches are retrieved from the Challonge API. Tournament operators select the match that is being played. Scores are reported to Challonge automatically."
                />
              </Field>
            )}
          </>
        );
      }}
    </Form>
  );
}
