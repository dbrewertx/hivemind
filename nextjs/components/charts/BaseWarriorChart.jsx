import { Line } from 'react-chartjs-2';
import { colors } from 'theme/colors';
import { getBaseOptions } from './GameChart';

const eggLoss = className => (
  <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 26.41 26.41">
    <path
      d="m22.35,15.22c0,6.28-4.09,9.36-9.14,9.36s-9.14-3.07-9.14-9.36S8.16,1.83,13.2,1.83s9.14,7.11,9.14,13.4Z"
      className={className}
    />
    <polygon
      points="26.41 2.41 24 0 13.21 10.79 2.41 0 0 2.41 10.79 13.21 0 24 2.41 26.41 13.21 15.62 24 26.41 26.41 24 15.62 13.21 26.41 2.41"
      fill="red"
    />
  </svg>
);

export default function BaseWarriorChart({ game, datasetProps }) {
  const defaultDatasetProps = {
    borderWidth: 1,
    lineTension: 0,
    stepped: true,
    fill: 'origin',
  };
  const queenDatasetProps = {
    pointRadius: 7,
    hoverRadius: 7,
    borderWidth: 1,
  };

  const data = {
    datasets: [
      {
        label: 'Blue Queen Deaths',
        pointBackgroundColor: colors.blue.light1,
        pointBorderColor: colors.blue.dark1,
        pointBorderWidth: 16,
        pointStyle: 'rectRot',
        data: game.queenDeathData.blue,
        ...queenDatasetProps,
      },
      {
        label: 'Gold Queen Deaths',
        pointBackgroundColor: colors.gold.light1,
        pointBorderColor: colors.gold.dark3,
        pointBorderWidth: 16,
        pointStyle: 'rectRot',
        data: game.queenDeathData.gold,
        ...queenDatasetProps,
      },
      {
        label: 'Blue Warriors',
        borderColor: colors.blue.dark2,
        backgroundColor: colors.blue.light1,
        data: game.warriorData.blue,
        ...defaultDatasetProps,
        ...datasetProps,
      },
      {
        label: 'Gold Warriors',
        borderColor: colors.gold.dark3,
        backgroundColor: colors.gold.light1,
        data: game.warriorData.gold.map(v => ({ x: v.x, y: -v.y, text: v.text })),
        ...defaultDatasetProps,
        ...datasetProps,
      },
    ],
  };

  const options = getBaseOptions(game);
  options.scales.y = {
    display: false,
    type: 'linear',
    min: -4,
    max: 4,
    scaleLabel: {
      display: false,
      labelString: 'Pixels',
    },
  };

  return <Line options={options} data={data} />;
}
