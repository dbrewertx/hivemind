import { makeStyles } from '@material-ui/core/styles';
import { useState } from 'react';
import { Bar } from 'react-chartjs-2';
import { CircularProgress, Typography } from '@material-ui/core';

import { getAxios } from 'util/axios';

const useStyles = makeStyles(theme => ({
  chartContainer: {
    height: '300px',

  },
}));

const options = {
  title: {display: false},
  animation: {duration: 0},
  maintainAspectRatio: false,
  plugins: {
    legend: {
      align: 'start',
      boxWidth: 24,
    } ,
  },
  scales: {
    xAxes: [{
      display: true,
      stacked: true,
      scaleLabel: {
        display: false,
        labelString: "Week",
      },
    }],
    yAxes: [{
      display: true,
      stacked: true,
      scaleLabel: {
        display: false,
        labelString: "Games",
      },
    }],
  },
};

export default function GamesByWeekChart() {
  const classes = useStyles();
  const axios = getAxios();
  const [data, setData] = useState(null);

  if (data === null) {
    axios.get("/api/stats/games-by-week").then((response) => {
      setData(response.data);
    });

    return (<CircularProgress />);
  }

  return (
    <div className="border border-solid border-gray-300 rounded px-3 py-1 pb-3">
      <Typography align="center" variant="h3" className="mb-2">Games By Week</Typography>
      <div className={classes.chartContainer}>
        <Bar options={options} data={data} />
      </div>
    </div>
  );
}
