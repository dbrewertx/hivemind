import React, { useEffect, useRef, useState } from 'react';
import { Scatter } from 'react-chartjs-2';
import { Chart as ChartJS } from 'chart.js';

import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import { getBaseOptions } from './GameChart';
import { colors } from 'theme/colors';

export default function BaseSnailChart({ game, datasetProps }) {
  const chartRef = useRef(null);

  var maxPx = 900;

  const opacity = (255 / game.snailData.length).toString(16);
  const grayColor = `#555555${opacity})`;
  const blueColor = `${colors.blue.light1}${opacity}`;
  const goldColor = `${colors.gold.light1}${opacity}`;
  const darkblueColor = colors.blue.dark2;
  const darkgoldColor = colors.gold.dark3;

  const defaultDatasetProps = {
    borderColor: '#555',
    fill: 'origin',
    borderWidth: 1,
    lineTension: 0,
    showLine: true,
  };
  const [data, setData] = useState(null);
  const [options, setOptions] = useState(null);

  useEffect(() => {
    const newData = {
      datasets: [
        {
          label: 'Snail Progress',
          data: game.snailData[0],
          ...defaultDatasetProps,
          ...datasetProps,
        },
      ],
    };

    if (game.map === 'Bonus (Snail)') {
      maxPx = 260;
      newData.datasets = [
        {
          label: 'Top Snail Progress',
          data: game.snailData[0],
          ...defaultDatasetProps,
          ...datasetProps,
        },
        {
          label: 'Left Snail Progress',
          data: game.snailData[1],
          ...defaultDatasetProps,
          ...datasetProps,
        },
        {
          label: 'Right Snail Progress',
          data: game.snailData[2],
          ...defaultDatasetProps,
          ...datasetProps,
        },
      ];
    }

    setData(newData);

    const newOptions = getBaseOptions(game);
    newOptions.scales.y = {
      display: false,
      type: 'linear',
      min: -maxPx,
      max: maxPx,
      scaleLabel: {
        display: false,
        labelString: 'Pixels',
      },
    };

    setOptions(newOptions);
  }, [game]);

  useEffect(() => {
    const chart = chartRef.current;
    if (!chart) return;
    const chartData = {
      ...data,
      datasets: data.datasets.map(dataset => {
        const min = dataset.data.reduce((min, p) => (p.y < min ? p.y : min), dataset.data[0].y);
        const max = dataset.data.reduce((max, p) => (p.y > max ? p.y : max), dataset.data[0].y);

        // figure out the pixels for these and the value 0
        const top = chart.scales.y.getPixelForValue(max);
        const zero = chart.scales.y.getPixelForValue(0);
        const bottom = chart.scales.y.getPixelForValue(min);

        // build a gradient that switches color at the 0 point
        const ctx = chart.ctx;
        const gradient = ctx.createLinearGradient(0, top, 0, bottom);
        const borderGradient = ctx.createLinearGradient(0, top, 0, bottom);
        const base = bottom - top || 0.000001;
        const ratio = Math.min((zero - top) / base, 1);
        const topColor = game.teamOnLeft == BLUE_TEAM ? blueColor : goldColor;
        const bottomColor = game.teamOnLeft == BLUE_TEAM ? goldColor : blueColor;
        const topBorderColor = game.teamOnLeft == BLUE_TEAM ? darkblueColor : darkgoldColor;
        const bottomBorderColor = game.teamOnLeft == BLUE_TEAM ? darkgoldColor : darkblueColor;

        gradient.addColorStop(0, topColor);
        gradient.addColorStop(ratio, topColor);
        gradient.addColorStop(ratio, bottomColor);
        gradient.addColorStop(1, bottomColor);

        borderGradient.addColorStop(0, topBorderColor);
        borderGradient.addColorStop(ratio, topBorderColor);
        borderGradient.addColorStop(ratio, bottomBorderColor);
        borderGradient.addColorStop(1, bottomBorderColor);

        return {
          ...dataset,
          backgroundColor: min === 0 && max === 0 ? grayColor : gradient,
          borderColor: min === 0 && max === 0 ? '#555' : borderGradient,
        };
      }),
    };
    setData(chartData);
  }, [game, options]);

  if (data === null || options === null) {
    return <></>;
  }

  return <Scatter ref={chartRef} options={options} data={data} />;
}
