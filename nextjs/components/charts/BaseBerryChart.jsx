import { Line } from 'react-chartjs-2';
import { Typography } from '@material-ui/core';
import cloneDeep from 'lodash/cloneDeep';
import { colors } from 'theme/colors';

import GameChart, { getBaseOptions } from './GameChart';

export default function BerryChart({ game, datasetProps }) {
  const defaultDatasetProps = {
    borderWidth: 1,
    lineTension: 0,
    stepped: true,
    fill: 'origin',
  };

  const data = {
    datasets: [
      {
        label: 'Blue Berries',
        borderColor: colors.blue.dark2,
        backgroundColor: colors.blue.light1,
        data: game.berryData.blue,
        ...defaultDatasetProps,
        ...datasetProps,
      },
      {
        label: 'Gold Berries',
        borderColor: colors.gold.dark3,
        backgroundColor: colors.gold.light1,
        data: game.berryData.gold.map((v) => ({x: v.x, y: -v.y, text: v.text})),
        ...defaultDatasetProps,
        ...datasetProps,
      },
    ],
  };

  const options = getBaseOptions(game);
  options.scales.y = {
    display: false,
    type: 'linear',
    min: -12,
    max: 12,
    scaleLabel: {
      display: false,
      labelString: 'Pixels',
    },
  };

  return (
    <Line options={options} data={data} />
  );
}

