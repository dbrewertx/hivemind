import { IconButton } from '@material-ui/core';
import TapAndPlayIcon from '@material-ui/icons/TapAndPlay';
import { toast } from 'react-toastify';

import isServer from 'util/isServer';

export default function NFC({ onScan }) {
  if (isServer()) {
    return (<></>);
  }

  if (!('NDEFREader' in window)) {
    return (<></>);
  }

  const onClick = async () => {
    try {
      const ndef = new window.NDEFReader();
      await ndef.scan();

      ndef.addEventListener('readingerror', () => {
        toast.error('Cannot read data from the NFC tag.');
      });

      ndef.addEventListener('reading', ({ message, serialNumber }) => {
        onScan(serialNumber);
      });
    } catch (error) {
      toast.error(`Couldn't read: ${error}`);
    }
  };

  return (
    <IconButton aria-label="Scan NFC Tag" onClick={onClick}>
      <TapAndPlayIcon />
    </IconButton>
  );
}
