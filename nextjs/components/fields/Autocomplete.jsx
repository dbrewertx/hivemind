import { FormControl, InputLabel } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import MuiAutocomplete from '@material-ui/lab/Autocomplete';
import PropTypes from 'prop-types';
import { useState } from 'react';

import { useForm } from '../forms/Form';
import TextField from './TextField';

const useStyles = makeStyles(theme => ({
  label: {
    transform: 'translate(12px, -6px) scale(0.75)',
    backgroundColor: 'white',
    padding: '0 3px',
  },
}));

export default function Autocomplete({ label, name, ...props }) {
  const classes = useStyles();
  const formik = useForm();
  const [edited, setEdited] = useState(props.placeholder);

  if (!props.renderInput) {
    props.renderInput = params => (
      <TextField
        {...params}
        name="newItem"
        placeholder={props.placeholder}
        error={formik.touched[name] && formik.errors[name] ? true : undefined}
        helperText={props.helperText}
        onChange={(evt, value, ...fnProps) => {
          formik.setFieldValue(props.name, value);
        }}
      />
    );
  }

  return (
    <FormControl fullWidth>
      <InputLabel
        variant="outlined"
        className={(formik.values[props.name] || edited) && classes.label}
      >
        {label}
      </InputLabel>
      <MuiAutocomplete
        name={name}
        {...props}
        variant="outlined"
        onInputChange={(evt, value, reason) => {
          setEdited(Boolean(value) || Boolean(props.placeholder));
        }}
        onChange={(evt, value, ...fnProps) => {
          formik.setFieldValue(props.name, value);
        }}
      />
    </FormControl>
  );
}

Autocomplete.propTypes = {
  label: PropTypes.string,
};

Autocomplete.defaultProps = {
  label: '',
};
