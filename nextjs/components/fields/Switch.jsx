import {
  FormControl,
  FormControlLabel,
  FormHelperText,
  Switch as MuiSwitch,
} from '@material-ui/core';
import PropTypes from 'prop-types';

export default function Switch({ name, label, formik, helperText, ...props }) {
  delete props.fullWidth;

  const control = <MuiSwitch name={name} checked={Boolean(formik.values[name])} {...props} />;

  return (
    <FormControl>
      <FormControlLabel control={control} label={label} />
      {helperText && (
        <FormHelperText className={props.error && 'text-red-500'}>{helperText}</FormHelperText>
      )}
    </FormControl>
  );
}

Switch.propTypes = {
  name: PropTypes.string.isRequired,
  label: PropTypes.node.isRequired,
  formik: PropTypes.object.isRequired,
};
