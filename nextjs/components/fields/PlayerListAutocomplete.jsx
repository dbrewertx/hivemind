import React, { useEffect, useState } from 'react';
import { Box, Typography } from '@material-ui/core';
import { createFilterOptions } from '@material-ui/lab/Autocomplete';
import PropTypes from 'prop-types';

import Field, { Autocomplete } from './Field';
import { useForm } from '../forms/Form';
import { getAxios } from 'util/axios';

const filter = createFilterOptions();

export default function PlayerListAutocomplete({ name, scene, ...props }) {
  const axios = getAxios({ authenticated: true });
  const formik = useForm();
  const [allPlayers, setAllPlayers] = useState(null);
  const [availablePlayers, setAvailablePlayers] = useState(null);

  const filterOptions = (options, params) => {
    const filtered = filter(options, params);
    const { inputValue } = params;

    const isExisting = options.some((option) => inputValue === option.name);
    if (inputValue !== '' && !isExisting) {
      filtered.push({ name: `New Player: "${inputValue}"`, value: inputValue });
    }

    return filtered;
  };

  const loadPlayerList = () => {
    axios.getAllPages('/api/league/player/', { params: { sceneId: scene.id } }).then(setAllPlayers);
  };

  const updateAvailablePlayers = () => {
    if (allPlayers !== null) {
      const selected = new Set(formik.values[name].map(i => i.id));
      setAvailablePlayers(allPlayers.filter(p => !(p.id in selected)));
    }
  };

  useEffect(updateAvailablePlayers, [allPlayers]);

  if (allPlayers === null) {
    loadPlayerList();
  }

  return (
    <Box>
      <Typography variant="h4">Player Count: <b>{formik?.values[name]?.length ?? 0}</b></Typography>

      <Field
        {...props}
        component={Autocomplete}
        name={name}
        multiple
        freeSolo
        filterSelectedOptions
        options={availablePlayers}
        filterOptions={filterOptions}
        handleHomeEndKeys
        getOptionLabel={opt => opt.name}
        renderOption={opt => opt.name}
        onChange={updateAvailablePlayers}
        clearOnBlur
      />
    </Box>
  );
}

PlayerListAutocomplete.propTypes = {
  name: PropTypes.string.isRequired,
  scene: PropTypes.object.isRequired,
};
