import { TextField as MuiTextField } from '@material-ui/core';

export default function TextField({ ...props }) {
  return <MuiTextField variant="outlined" {...props} />;
}
