import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from '@material-ui/core';
import PropTypes from 'prop-types';
import { NumericFormat } from 'react-number-format';

import { SEASON_TYPES } from 'util/constants';
import Table from './Table';
import EventTeamsForm from 'components/forms/EventTeamsForm';
import EventDeleteTeamsForm from 'components/forms/EventDeleteTeamsForm';
import EventBYOTeamSelectForm from 'components/forms/EventBYOTeamSelectForm';
import { isAdminOf } from 'util/auth';

const useStyles = makeStyles(theme => ({
  teamName: {
    fontWeight: 'bold',
  },
  players: {
    marginTop: theme.spacing(1),
  },
  numeric: {
    ...theme.mixins.tableCell.numeric(theme),
  },
}));

export default function EventStandingsTable({ title, event }) {
  const classes = useStyles();
  const columnHeaders = ['Team Name', 'Points', 'Rounds', 'Matches'];
  const getRowCells = row => [
    <Grid container direction="column">
      <Grid item className={classes.teamName}>
        {row.team.name}
      </Grid>
      <Grid item className={classes.players}>
        {row.team.players.map(player => player.name).join(', ')}
      </Grid>
    </Grid>,
    <NumericFormat
      value={row.points}
      displayType="text"
      decimalScale={2}
      fixedDecimalScale={true}
    />,
    `${row.roundsWon}-${row.roundsLost}`,
    `${row.matchesWon}-${row.matchesLost}`,
  ];

  const cellClassNames = [null, classes.numeric, classes.numeric, classes.numeric];
  const rowLink = row => `/event-team/${row.team.id}`;

  const props = {
    title,
    columnHeaders,
    data: event.standings,
    getRowCells,
    isLoading: false,
    link: rowLink,
    cellClassNames,
  };

  if (
    isAdminOf(event.season.scene.id) &&
    event.season.seasonType == SEASON_TYPES.SHUFFLE &&
    !event.isComplete
  ) {
    props.titleButton = (
      <Grid container direction="row" className="gap-4">
        {event.matches.filter(match => match.isComplete).length == 0 && (
          <EventDeleteTeamsForm event={event} />
        )}
        <EventTeamsForm event={event} />
      </Grid>
    );
  }

  if (
    isAdminOf(event.season.scene.id) &&
    event.season.seasonType == SEASON_TYPES.BYOT &&
    !event.isComplete
  ) {
    props.titleButton = (
      <Grid container direction="row" className="gap-4">
        {event.matches.length > 0 &&
          event.matches.filter(match => match.isComplete).length == 0 && (
            <EventDeleteTeamsForm event={event} />
          )}
        {event.standings.length == 0 && <EventBYOTeamSelectForm event={event} />}
      </Grid>
    );
  }

  return <Table {...props} />;
}

EventStandingsTable.propTypes = {
  title: PropTypes.string,
  event: PropTypes.object.isRequired,
};

EventStandingsTable.defaultProps = {
  title: 'Event Standings',
};
