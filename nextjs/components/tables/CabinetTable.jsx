import { useState } from 'react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { format } from 'date-fns';

import { getAxios } from 'util/axios';
import Table from './Table';
import { isAdminOf } from 'util/auth';
import CabinetForm from 'components/forms/CabinetForm';

export default function CabinetTable({ title, scene }) {
  const axios = getAxios();
  const [data, setData] = useState(null);

  const columnHeaders = ['Name', 'Total Games', 'Last Game'];
  const getRowCells = row => [
    row.displayName,
    row.totalGames,
    row.lastGameTime ? format(new Date(row.lastGameTime), 'MMM d - hh:mm aa') : '',
  ];

  const rowLink = row => `/cabinet/${scene.name}/${row.name}`;

  if (data === null) {
    axios.get('/api/game/cabinet/', { params: { sceneId: scene.id } }).then((response) => {
      const cabinets = response.data.results.sort((a, b) =>
        a.displayName.localeCompare(b.displayName)
      );

      setData(cabinets);
    });
  }

  const props = {
    title,
    columnHeaders,
    data,
    getRowCells,
    isLoading: data === null,
    link: rowLink,
  };

  if (isAdminOf(scene.id)) {
    props.titleButton = (<CabinetForm scene={scene} />);
  }

  return (
    <Table {...props} />
  );
}

CabinetTable.propTypes = {
  title: PropTypes.string,
  scene: PropTypes.object.isRequired,
};

CabinetTable.defaultProps = {
  title: 'Cabinets',
};
