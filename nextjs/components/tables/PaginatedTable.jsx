import { useState } from 'react';
import PropTypes from 'prop-types';

import { getAxios } from 'util/axios';
import Table from './Table';

export default function PaginatedTable({ url, filters, tableClassName, afterGetData, ...props }) {
  const [page, setPage] = useState(0);
  const [totalCount, setTotalCount] = useState(null);
  const [data, setData] = useState(null);
  const [isLoading, setIsLoading] = useState(false);
  const axios = getAxios();

  const getData = pageNum => {
    setIsLoading(true);
    axios.get(url, { params: { page: pageNum + 1, ...filters } }).then(response => {
      setData(response.data.results);
      afterGetData(response.data.results);
      setIsLoading(false);
      setPage(pageNum);
      setTotalCount(response.data.count);
    });
  };

  if (!isLoading && data === null) {
    getData(page);
  }

  const onPageChange = (event, newPageNum) => {
    getData(newPageNum);
  };

  return (
    <Table
      data={data}
      isLoading={isLoading}
      page={page}
      totalCount={totalCount}
      onPageChange={onPageChange}
      tableClassName={tableClassName}
      {...props}
    />
  );
}

PaginatedTable.propTypes = {
  rowsPerPage: PropTypes.number,
  tableClassName: PropTypes.string,
  afterGetData: PropTypes.func,
};

PaginatedTable.defaultProps = {
  rowsPerPage: 20,
  tableClassName: null,
  afterGetData: () => undefined,
};
