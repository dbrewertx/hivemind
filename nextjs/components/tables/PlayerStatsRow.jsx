import { Grid, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';


const useStyles = makeStyles(theme => ({
  backgroundBlue: {
    background: theme.gradients.blue.light,
  },
  backgroundGold: {
    background: theme.gradients.gold.light,
  },
  row: {
    [theme.breakpoints.down('sm')]: {
      flexDirection: 'column',
      flexBasis: 0,
      flexGrow: 1,
    },
  },
  playerCell: {
    textAlign: 'center',
    verticalAlign: 'middle',
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    background: 'transparent',
    [theme.breakpoints.up('md')]: {
      padding: theme.spacing(0.5, 1),
      flexBasis: 0,
      flexGrow: 1,
    },
    [theme.breakpoints.down('sm')]: {
      flexBasis: '36px',
      height: '36px',
      lineHeight: '36px',
      flexGrow: 0,
      fontSize: '12px',
      alignItems: 'center'
    },
  },
  value: {
    fontSize: 'inherit',
  },
  labelCell: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(0.5, 1),
    textAlign: 'center',
    flexGrow: 0,
    fontWeight: 'bold',
    [theme.breakpoints.up('md')]: {
      flexBasis: '175px',
    },
    [theme.breakpoints.down('sm')]: {
      flexBasis: '60px',
      verticalAlign: 'middle',
      fontSize: '12px',
      lineHeight: '14px',
    },
    '& .MuiTypography-root': {
      fontWeight: 'bold',
    },
  },
  icons: {
    [theme.breakpoints.down('sm')]: {
      width: '48px',
      flexBasis: '48px',
      flexGrow: 0,
    },
  },
  icon: {
    padding: '12px 6px',
    [theme.breakpoints.down('sm')]: {
      padding: 0,
      width: '48px',
      '& img': {
        margin: '-8px 0 -8px -8px'
      },
    },
  },
  iconGold: {
    '& span': {
      [theme.breakpoints.down('sm')]: {
        transform: 'scaleX(-1)',
      },
    },
  },
  playerIcon: {
    display: 'block',
    height: '48px',
    width: '48px',
    backgroundSize: '48px 48px',
    backgroundPosition: 'center',
    [theme.breakpoints.down('sm')]: {
      padding: 0,
      height: '37px',
    }
  }
}));
export function PlayerStatsContainer({children}) {
  const classes = useStyles();
  return (<div className="overflow-scroll rounded"><Grid container className={clsx(classes.container, 'flex-nowrap lg:flex-wrap')}>{children}</Grid></div>)
}
export function PlayerStatsHeaderRow() {
  const classes = useStyles();

  return (
    <Grid item container className={clsx(classes.row, classes.icons, 'sticky left-0 md:relative odd:bg-gray-100 bg-gray-200')}>
      {POSITIONS_BY_TEAM[BLUE_TEAM].map(pos => (
        <Grid item className={clsx(classes.icon, classes.iconBlue, classes.playerCell, classes.backgroundBlue)} key={pos.ID}>
          <span className={clsx(classes.playerIcon)} style={{ backgroundImage: `url(${pos.IMAGE}` }}></span>
        </Grid>
      ))}
      <Grid item className={classes.labelCell} />
      {POSITIONS_BY_TEAM[GOLD_TEAM].map(pos => (
        <Grid item className={clsx(classes.icon, classes.iconGold, classes.playerCell, classes.backgroundGold)} key={pos.ID}>
          <span className={clsx(classes.playerIcon)} style={{ backgroundImage: `url(${pos.IMAGE}` }}></span>
        </Grid>
      ))}
    </Grid>
  )
}

export default function PlayerStatsRow({ label, values, format, defaultValue, getValue = null }) {
  const classes = useStyles();
  const getValueDefault =  position => {
    const value = values[position] ?? defaultValue ?? 0;
    return format ? format(value) : value;
  };

  const getValueFn = getValue || getValueDefault;

  return (
    <Grid item container className={clsx(classes.row, classes.statRow, 'odd:bg-gray-100 bg-gray-200')}>
      {POSITIONS_BY_TEAM[BLUE_TEAM].map(pos => (
        <Grid item className={clsx(classes.valueCell, classes.playerCell, classes.backgroundBlue)} key={pos.ID}>
          <Typography className={classes.value}>{getValueFn(pos.ID)}</Typography>
        </Grid>
      ))}

      <Grid item className={classes.labelCell}>
        <span className={classes.label}>{label}</span>
      </Grid>

      {POSITIONS_BY_TEAM[GOLD_TEAM].map(pos => (
        <Grid item className={clsx(classes.valueCell, classes.playerCell, classes.backgroundGold)} key={pos.ID}>
          <Typography className={classes.value}>{getValueFn(pos.ID)}</Typography>
        </Grid>
      ))}
    </Grid>
  );
}