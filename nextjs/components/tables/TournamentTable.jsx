import { Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import { useState } from 'react';

import { isAdminOf } from 'util/auth';
import { getAxios } from 'util/axios';
import { formatUTC } from 'util/dates';
import TournamentForm from '../forms/TournamentCreateForm';
import Table from './Table';

export default function TournamentTable({ title, scene }) {
  const axios = getAxios();
  const [data, setData] = useState(null);

  const columnHeaders = ['Name', 'Date'];
  const getRowCells = row => [row.name, formatUTC(row.date, 'MMM d, yyyy')];

  const rowLink = row => `/tournament/${row.id}`;

  const [showOld, setShowOld] = useState(false);

  if (data === null) {
    axios
      .getAllPages('/api/tournament/tournament/', { params: { sceneId: scene.id } })
      .then(setData);
  }

  const props = {
    title,
    columnHeaders,
    data: data?.filter(tournament => showOld || new Date(tournament.date) > new Date()),
    getRowCells,
    isLoading: data === null,
    link: rowLink,
  };

  if (isAdminOf(scene.id)) {
    const emptyTournament = { scene };
    props.titleButton = (
      <>
        <TournamentForm tournament={emptyTournament} />
      </>
    );
  }

  props.titleButton = (
    <>
      <div className="flex gap-2 items-center">
        <Button
          className=" ml-auto inline-block"
          size="small"
          variant="outlined"
          onClick={() => setShowOld(!showOld)}
        >
          {showOld ? 'Hide' : 'Show'} old
        </Button>
        {props?.titleButton}
      </div>
    </>
  );

  return <Table {...props} />;
}

TournamentTable.propTypes = {
  title: PropTypes.string,
  scene: PropTypes.object.isRequired,
};

TournamentTable.defaultProps = {
  title: 'Tournaments',
};
