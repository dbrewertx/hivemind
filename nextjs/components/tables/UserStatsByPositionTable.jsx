import { formatTime } from 'util/dates';
import PlayerStatsRow, { PlayerStatsContainer, PlayerStatsHeaderRow} from './PlayerStatsRow';

export default function UserStatsByPositionTable({ stats }) {
  return (
    <PlayerStatsContainer>
      <PlayerStatsHeaderRow />
      {stats.map(stat => (
        <PlayerStatsRow
        key={stat.name}
        label={stat.label}
        values={stat.values}
        format={(stat.name == 'warrior_uptime' || stat.name == 'time_played') ? formatTime : undefined}
        />
        ))}
    </PlayerStatsContainer>
  );
}
