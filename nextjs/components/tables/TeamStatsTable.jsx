import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Box, Typography } from '@material-ui/core';
import clsx from 'clsx';
import { forEach } from 'lodash';

const useStyles = makeStyles(theme => ({
  teamStatRow: {
    // padding: theme.spacing(1, 2),
    alignItems: 'center',
  },
  teamStatValue: {
    textAlign: 'left',
    // border: '1px solid black',
    padding: theme.spacing(0.5, 2),
    height: '36px',
    flexGrow: 1,
    '& .MuiTypography-root': {
      fontWeight: 'bold',
    },
  },
  backgroundBlue: {
    background: theme.gradients.blue.light,
  },
  backgroundGold: {
    background: theme.gradients.gold.light,
    textAlign: 'right'
  },
}));

export function TeamStatRow({ title, values }) {
  const classes = useStyles();
  if (!values) return null;
  let { blue, gold } = values;

  if (typeof blue == 'string' || typeof gold == 'string') {
    blue = Number(blue.substring(0, blue.length - 1));
    gold = Number(gold.substring(0, gold.length - 1));
  }
  const total = blue + gold;
  const width = (val) => `${(val / total) * 100}%`;
  return (
    <Grid item container direction="row" className={clsx(classes.teamStatRow, 'lg:pr-6')}>
      <Grid item xs={4} className={classes.teamStatTitle}>
        <Typography className="font-bold">{title}</Typography>
      </Grid>
      <Grid item  className="flex flex-grow">
        <Box className={clsx(classes.teamStatValue, classes.backgroundBlue, 'rounded-l')} style={{ flexBasis: width(blue)}}>
          <Typography className="leading-7">{values?.blue ?? 'N/A'}</Typography>
        </Box>
        <Box className="w-0.5">&nbsp;</Box>
        <Box className={clsx(classes.teamStatValue, classes.backgroundGold, 'rounded-r')} style={{ flexBasis: width(gold)}}>
          <Typography className="leading-7">{values?.gold ?? 'N/A'}</Typography>
        </Box>
      </Grid>
    </Grid>
  );
}

export default function TeamStatsTable({ stats, ...props }) {
  const classes = useStyles();
  const { kills, militaryKills } = stats;
  if (!kills && !militaryKills) return null;
  const droneKills = {};

  forEach({'blue': 0,'gold': 0}, (val, key) => {
    droneKills[key] = kills[key] - militaryKills[key];
  });

  return (
    <Grid item container direction="column" className={clsx(classes.table, 'space-y-1')} {...props}>
      <TeamStatRow title="Total Kills" values={stats.kills} />
      <TeamStatRow title="Military Kills" values={stats.militaryKills} />
      <TeamStatRow title="Drone Kills" values={droneKills} />
      <TeamStatRow title="Berries" values={stats.berries} />
      <TeamStatRow title="Gate Control" values={stats.gateControl} />
    </Grid>
  );
}

