import { Tooltip } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import GameEventMap from 'components/GameEventMap';
import GameEvent from 'models/GameEvent';
import PropTypes from 'prop-types';
import React, { forwardRef } from 'react';

import PaginatedTable from './PaginatedTable';
import { TableCell, TableRow } from './Table';

const useStyles = makeStyles(theme => ({
  timestamp: {
    width: '130px',
    paddingRight: theme.spacing(2),
    textAlign: 'right',
  },
  playerIcon: {
    height: '20px',
    width: '20px',
    verticalAlign: 'middle',
    '&:not(:first-of-type)': {
      marginLeft: theme.spacing(1),
    },
  },
  cellContents: {
    margin: theme.spacing(-1),
    padding: theme.spacing(1),
  },
  tableRow: {
    '&:hover': {
      backgroundColor: theme.palette.highlight.main,
    },
  },
}));

export default function GameEventsTable({ game, filters }) {
  const classes = useStyles();

  const rowElement = ({ row }) => {
    const event = new GameEvent({ ...row, game });

    const MapTableCell = ({ children, ...props }) => {
      if (event.location?.length >= 2) {
        const eventMap = (<GameEventMap event={event} />);
        const Children = forwardRef(({ className, ...props }, ref) => (
          <div className={clsx(className, classes.cellContents)} {...props} ref={ref}>{children}</div>
        ));

        Children.displayName = 'MapTableCellChildren';

        Children.propTypes = {
          className: PropTypes.string,
        };

        Children.defaultProps = {
          className: null,
        };

        return (
          <TableCell {...props}>
            <Tooltip title={eventMap}>
              <Children />
            </Tooltip>
          </TableCell>
        );
      } else {
        return (
          <TableCell {...props}>{children}</TableCell>
        );
      }
    };

    MapTableCell.propTypes = {
      children: PropTypes.oneOfType([PropTypes.string, PropTypes.node, PropTypes.arrayOf(PropTypes.node)]),
    };

    MapTableCell.defaultProps = {
      children: (<></>),
    };

    return (
      <TableRow className={classes.tableRow}>
        <MapTableCell className={classes.timestamp}>{event.formatTimestamp()}</MapTableCell>
        <MapTableCell>{event.description}</MapTableCell>
        <MapTableCell>
          {event.players && event.players.map(pos => pos?.ICON && (
            <img className={classes.playerIcon} key={pos.ID} src={pos.ICON} />
          ))}
        </MapTableCell>
        <MapTableCell>{event.location.join(', ')}</MapTableCell>
      </TableRow>
    );
  };

  return (
    <PaginatedTable
      columnHeaders={['Game Time', 'Event Type', 'Players', 'Location']}
      rowElement={rowElement}
      url="/api/game/game-event"
      filters={{ gameId: game.id, ...filters }}
      emptyText="No events found."
      cellClassNames={[ classes.timestamp, null, null, null ]}
    />
  );
}

GameEventsTable.propTypes = {
  game: PropTypes.object.isRequired,
  filters: PropTypes.shape({
    eventType: PropTypes.string,
  }),
  setTargetLoc: PropTypes.func,
};

GameEventsTable.defaultProps = {
  filters: {},
  onClick: () => null,
};
