import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { Grid } from '@material-ui/core';
import { format, parse } from 'date-fns'

import Table, { TableRow, TableCell } from './Table';

const useStyles = makeStyles(theme => ({
  cell: {
    border: `1px solid ${theme.palette.divider}`,
  },
}));

export default function CabinetActiveTimeTable({ cabinet, cabinetActivity }) {
  const classes = useStyles();
  const columnHeaders = ['Hour', 'Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
  const maxActivity = Math.max(...cabinetActivity.results.map(i => i.activity));

  const activityPct = [...Array(7)].map(() => ({}));
  for (const row of cabinetActivity.results) {
    activityPct[(row.dayOfWeek + 1) % 7][row.hour] = row.activity / maxActivity;
  }

  return (
    <Table title={`Active Times (${cabinet.timeZone})`} columnHeaders={columnHeaders}>
      {[...Array(24)].map((i, hour) => (
        <TableRow key={hour}>
          <TableCell>
            <span className="text-xs md:text-base">{format(parse(hour, 'H', new Date()), 'HH:mm')}</span>
          </TableCell>
          {[...Array(7)].map((i, day) => (
            <TableCell
              key={day}
              className={classes.cell}
              style={{ backgroundColor: `rgba(255, 0, 0, ${activityPct[day][hour]})` }}
            />
          ))}
        </TableRow>
      ))}
    </Table>
  );
}

CabinetActiveTimeTable.propTypes = {
  cabinet: PropTypes.object,
  cabinetActivity: PropTypes.object,
};
