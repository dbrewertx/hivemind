import React, { useState } from 'react';
import { useRouter } from 'next/router';
import { makeStyles } from '@material-ui/core/styles';
import { Grid, Button } from '@material-ui/core';
import PropTypes from 'prop-types';

import Table from './Table';
import AddPlayerForm from 'components/forms/AddPlayerForm';
import EventGenerateTeamsForm from 'components/forms/EventGenerateTeamsForm';
import { getAxios } from 'util/axios';
import { isAdminOf } from 'util/auth';

export default function EventPlayersTable({ title, event }) {
  const axios = getAxios({ authenticated: true });
  const [players, setPlayers] = useState(event.unassignedPlayers);
  const columnHeaders = ['Name'];
  const getRowCells = row => [
    row.name,
  ];

  const reloadPlayers = () => {
    axios.getAllPages('/api/league/player/', { params: { eventId: event.id } }).then(setPlayers);
  };

  const rowLink = row => `/player/${row.id}`;

  const props = {
    title,
    columnHeaders,
    data: players,
    getRowCells,
    isLoading: false,
    link: rowLink,
  };

  const generateTeams = () => {

  };

  if (isAdminOf(event.season.scene.id)) {
    props.titleButton = (
      <Grid container direction="row" className="gap-4">
        <AddPlayerForm event={event} players={players} onSave={reloadPlayers} />
        <EventGenerateTeamsForm event={event} />
      </Grid>
    );
  }

  return (
    <Table {...props} />
  );
}

EventPlayersTable.propTypes = {
  title: PropTypes.string,
  event: PropTypes.object.isRequired,
};

EventPlayersTable.defaultProps = {
  title: 'Teams',
};
