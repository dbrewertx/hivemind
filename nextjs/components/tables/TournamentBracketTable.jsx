import React, { useState } from 'react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { toast } from 'react-toastify';

import { isAdminOf } from 'util/auth';
import { TOURNAMENT_LINK_TYPES } from 'util/constants';
import Table, { TableRow, TableCell } from './Table';
import TournamentBracketForm from 'components/forms/TournamentBracketForm';

export default function TournamentBracketTable({ title, tournament, onSave }) {
  const [formOpen, setFormOpen] = useState(false);
  const [selectedBracket, setSelectedBracket] = useState({});

  const columnHeaders = ['Stage Name'];

  const openForm = (e, bracket) => {
    e.preventDefault();

    setSelectedBracket(bracket);
    setFormOpen(true);
  };

  const closeForm = () => {
    onSave();
    setFormOpen(false);
  };

  const handleSaveNew = response => {
    if (tournament.linkType == TOURNAMENT_LINK_TYPES.MANUAL) {
      return onSave();
    }

    if (tournament.linkType == TOURNAMENT_LINK_TYPES.CHALLONGE) {
      // When saving a new bracket, reopen the form with the link key
      setSelectedBracket(response);
      setFormOpen(true);

      return onSave();
    }
  };

  const props = {
    title,
    columnHeaders,
    isLoading: false,
  };

  if (isAdminOf(tournament.scene.id)) {
    const blankBracket = {
      tournament: tournament.id,
      roundsPerMatch: 0,
      winsPerMatch: 0,
      reportAsSets: false,
      addTiebreaker: false,
    };

    props.titleButton = (
      <TournamentBracketForm
        bracket={blankBracket}
        tournament={tournament}
        onSave={handleSaveNew}
        closeOnSave={false}
        enableReinitialize={false}
      />
    );
  }

  return (
    <>
      <Table {...props}>
        {tournament.brackets.map(bracket => (
          <TableRow key={bracket.id} onClick={e => openForm(e, bracket)}>
            <TableCell>{bracket.name}</TableCell>
          </TableRow>
        ))}
      </Table>
      <TournamentBracketForm
        bracket={selectedBracket}
        tournament={tournament}
        showButton={false}
        open={formOpen}
        onClose={closeForm}
      />
    </>
  );
}

TournamentBracketTable.propTypes = {
  title: PropTypes.string,
  tournament: PropTypes.object.isRequired,
};

TournamentBracketTable.defaultProps = {
  title: 'Tournament Stages',
};
