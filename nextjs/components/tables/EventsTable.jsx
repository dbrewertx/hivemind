import { useRouter } from 'next/router';
import PropTypes from 'prop-types';

import { getAxios } from 'util/axios';
import { isAdminOf } from 'util/auth';
import { formatUTC } from 'util/dates';
import Table from './Table';
import EventForm from 'components/forms/EventForm';

export default function EventsTable({ title, season }) {
  const columnHeaders = ['Event Date'];
  const getRowCells = row => [
    formatUTC(row.eventDate, 'MMMM d, yyyy'),
  ];
  const getCellAttributes = (row, idx) => (
    { suppressHydrationWarning: true }
  );

  const rowLink = row => `/event/${row.id}`;

  const props = {
    title,
    columnHeaders,
    getRowCells,
    getCellAttributes,
    data: season.events,
    isLoading: false,
    link: rowLink,
  };

  if (isAdminOf(season.scene.id)) {
    const emptyEvent = {
      season,
      qpMatchesPerPlayer: 6,
      pointsPerRound: 2,
      pointsPerMatch: 1,
      roundsPerMatch: 3,
      winsPerMatch: 0,
      matchesPerOpponent: 1,
      unassignedPlayers: [],
    };

    props.titleButton = (<EventForm event={emptyEvent} />);
  }

  return (
    <Table {...props} />
  );
}

EventsTable.propTypes = {
  title: PropTypes.string,
  season: PropTypes.object.isRequired,
};

EventsTable.defaultProps = {
  title: 'Events',
};
