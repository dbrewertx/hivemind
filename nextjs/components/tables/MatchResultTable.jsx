import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid, Link } from '@material-ui/core';
import clsx from 'clsx';
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
  matchTable: {
    margin: theme.spacing(2, 0),
    border: '1px solid ' + theme.palette.grey['400'] ,
    borderRadius: '5px',
    overflow: 'hidden'
  },
  teamLink: {
    display: 'flex',
    flexGrow: 1,
    flexBasis: 0,
    color: theme.palette.text.primary,
    borderBottom: '2px solid transparent',
    marginBottom: '-2px',
    '&:hover': {
      color: theme.palette.text.primary,
      textDecoration: 'none',
      borderBottom: `2px solid ${theme.palette.text.primary}`,
    },
  },
  blueTeam: ({ match }) => ({
    background: theme.gradients.blue.light,
    '& .MuiTypography-root': {
      fontWeight: (match.isComplete && match.blueScore > match.goldScore) ? 'bold' : 'normal',
    },
  }),
  goldTeam: ({ match }) => ({
    background: theme.gradients.gold.light,
    '& .MuiTypography-root': {
      fontWeight: (match.isComplete && match.blueScore < match.goldScore) ? 'bold' : 'normal',
    },
  }),
  queenIcon: {
    flexGrow: 0,
    flexBasis: '54px',
    padding: '2px 2px 2px 8px',
  },
  goldQueenIcon: {
    padding: '2px 8px 2px 2px',
  },
  teamName: {
    flexGrow: 1,
    flexBasis: 0,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
  },
  score: {
    flexGrow: 0,
    flexBasis: '60px',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    textAlign: 'center',
    background: theme.palette.grey['100']
  },
  text: {
    margin: theme.spacing(0, 3),
    fontSize: '125%',
  },
  scoreText: {
    fontSize: '150%',
  },
}));

export default function MatchResultTable({ match, teamLinkPath }) {
  const classes = useStyles({ match });

  const TeamRow = ({team, isWinner, className}) => (
    <Link className={clsx(classes.teamLink, className)} href={`/${teamLinkPath}/${match[`${team}Team`].id}`}>
      <Grid item className={clsx(classes[`${team}Team`], classes.queenIcon)}>
        <img className={classes[`${team}Queen`]} src={`/static/${team}_queen.png`} />
      </Grid>

      <Grid item className={clsx(classes.teamName)}>
        <Typography className={clsx(classes.text,'text-sm md:text-lg')}>{match[`${team}Team`].name ?? match[`${team}Team`]}</Typography>
      </Grid>

      <Grid item className={clsx(classes.score, isWinner && classes[`${team}Team`])}>
        <Typography className={classes.scoreText}>{match[`${team}Score`]}</Typography>
      </Grid>
    </Link>
  );

  return (
    <div className={clsx(classes.matchTable, 'grid grid-cols-1 md:grid-cols-2')}>

      <TeamRow team='blue' isWinner={match.blueScore > match.goldScore} />
      <TeamRow team='gold' isWinner={match.goldScore > match.blueScore} className="md:flex-row-reverse md:text-right border-0 border-t border-t-gray-300 border-solid md:border-0" />

    </div>
  );
}

MatchResultTable.propTypes = {
  match: PropTypes.object.isRequired,
  teamLinkPath: PropTypes.string,
};

MatchResultTable.defaultProps = {
  teamLinkPath: 'event-team',
};

