import React, { useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import PropTypes from 'prop-types';
import { format, formatDistance } from 'date-fns';
import { getAxios } from 'util/axios';
import Table, { TableRow, TableCell } from './Table';

export const SceneTableDisplayName = ({ scene }) => (
  <TableCell className="border-b-0 md:border-b whitespace-nowrap -mb-3 md:mb-0">
    <div className="w-8 h-6 mr-2 inline-block align-middle relative">
      <span className="text-white text-base font-bold text-center absolute inset-0 z-10 h-6 w-8 tracking-[-1px] text-shadow leading-6">{scene.displayName[0].toUpperCase()}</span>
      <svg className="z-0 inset-0 absolute h-6 w-8" id="Layer_2" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 27.71 24"><g id="Layer_1-2"><polygon points="6.93 0 0 12 6.93 24 20.78 24 27.71 12 20.78 0 6.93 0" fill={`${scene.backgroundColor}`} /></g></svg>
    </div>
    <span className="whitespace-nowrap">{scene.displayName}</span>
  </TableCell>
);
export default function SceneTable({ title, filters,  }) {
  const axios = getAxios();
  const [data, setData] = useState(null);

  const columnHeaders = ['Location', 'Total Games', 'Last Game'];
  const getRowCells = row => [
    row.displayName,
    row.totalGames,
    row.lastGameTime ? formatDistance(new Date(), new Date(row.lastGameTime)) + ' ago' : '',
  ];

  const rowLink = row => `/scene/${row.name.toLowerCase()}`;

  useEffect(() => {
    if (data === null) {
      axios.getAllPages('/api/game/scene/').then(response => {
        const scenes = response
          .filter(scene => scene.totalGames > 0)
          .sort((a, b) => b.totalGames - a.totalGames);

        setData(scenes);
      });
    }
  }, []);

  return (
    <Table title={title} columnHeaders={columnHeaders} isLoading={data === null} className="first:mt-0" tableClassName="table-auto">
      {data?.map(scene => {
        return (
          <TableRow href={`/scene/${scene.name.toLowerCase()}`} key={scene.id}>
            <SceneTableDisplayName scene={scene} />
            <TableCell>{scene.totalGames}</TableCell>
            <TableCell>
              <span className="text-xs inline-block leading-[1]">

              {scene.lastGameTime
                ? formatDistance(new Date(), new Date(scene.lastGameTime)) + ' ago'
                : ''}
                </span>
            </TableCell>
          </TableRow>
        );
      })}
    </Table>
  );
}

SceneTable.propTypes = {
  title: PropTypes.string,
  filters: PropTypes.object,
  infoCellsSlot: PropTypes.func
};

SceneTable.defaultProps = {
  title: 'Recent Games',
  filters: {},
  infoCellsSlot: null
};
