import React, { useState } from 'react';
import { Dialog, DialogContent } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { getAxios } from 'util/axios';
import PaginatedTable from './PaginatedTable';
import { format } from 'date-fns';
import { TwitchClip } from 'react-twitch-embed';

import Table from './Table';
import { formatInterval } from 'util/dates';

const useStyles = makeStyles(theme => ({
  label: {},
}));

export default function VideoClipsTable({ title, filters, rowsPerPage }) {
  const classes = useStyles();
  const axios = getAxios();
  const [open, setOpen] = useState(false);
  const [clipId, setClipId] = useState(null);
  const [cabinetNames, setCabinetNames] = useState({});

  const columnHeaders = ['Date Created', 'Cabinet Name', 'Game ID'];

  const showVideo = row => {
    console.log(row);
  };

  const getRowCells = row => [
    format(new Date(row.createdDate), 'MMM d - hh:mm aa'),
    cabinetNames[row.cabinet] ?? '...',
    row.gameId,
  ];

  const onRowClick = row => {
    setClipId(row.twitchClipId);
    setOpen(true);
  };

  const afterGetData = results => {
    const cabinetIDs = new Set(results.map(i => i.cabinet));
    for (const id of cabinetIDs) {
      if (!(id in cabinetNames)) {
        axios.get(`/api/game/cabinet/${id}/`).then(response => {
          setCabinetNames(names => ({ ...names, [id]: response.data.displayName }));
        });
      }
    }
  };

  return (
    <>
      <Dialog onClose={() => setOpen(false)} open={open} maxWidth={false}>
        <DialogContent>
          <TwitchClip clip={clipId} autoplay />
        </DialogContent>
      </Dialog>

      <PaginatedTable
        title={title}
        columnHeaders={columnHeaders}
        getRowCells={getRowCells}
        onRowClick={onRowClick}
        url="/api/video/video-clip/"
        filters={filters}
        emptyText="No videos found."
        rowsPerPage={rowsPerPage}
        afterGetData={afterGetData}
      />
    </>
  );
}

VideoClipsTable.propTypes = {
  title: PropTypes.string,
  filters: PropTypes.shape({
    userId: PropTypes.number,
    cabinetId: PropTypes.number,
  }),
  rowsPerPage: PropTypes.number,
};

VideoClipsTable.defaultProps = {
  title: 'Video Clips',
  filters: {},
  rowsPerPage: 20,
};
