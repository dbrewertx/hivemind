import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';
import PropTypes from 'prop-types';

const useStyles = makeStyles(theme => ({
  inlineCodeBlock: {
    fontFamily: '"Roboto Mono", monospace',
    border: '1px solid #ddd',
    background: '#eee',
    padding: theme.spacing(0, 0.5),
    margin: theme.spacing(0, 0.5),
    fontSize: '90%',
  },
}));

export default function InlineCode({ children, className }) {
  const classes = useStyles();

  return (
    <span className={clsx(classes.inlineCodeBlock, className)}>{children}</span>
  );
}

InlineCode.propTypes = {
  children: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.arrayOf(PropTypes.element),
    PropTypes.element,
  ]).isRequired,
  className: PropTypes.string,
};

InlineCode.defaultProps = {
  className: null,
};

