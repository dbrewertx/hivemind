import React, { useState } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import { Typography, Grid, Button } from '@material-ui/core';
import PropTypes from 'prop-types';
import clsx from 'clsx';
import Icon from '@mdi/react';
import { mdiPlus, mdiMinus, mdiNull } from '@mdi/js';

const useStyles = makeStyles(theme => ({
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: theme.spacing(2),
  },
  value: {
    fontSize: '24px',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  button: {
    margin: theme.spacing(1),
  },
  title: {
    fontSize: '16px',
    fontWeight: 'bold',
  },
  titleBox: {
    overflow: 'hidden',
  },
}));

export default function ScoreUpdater({ title, value, onChange }) {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);

  const handleChange = newValue => {
    setLoading(true);
    onChange(newValue).then(setLoading(false));
  };

  const ScoreButton = ({ iconPath, ...props }) => (
    <Button variant="outlined" className={classes.button} disabled={loading} {...props}>
      <Icon size={1} path={iconPath} />
    </Button>
  );

  return (
    <Grid container direction="column" className={clsx(classes.container, { loading })}>
      {title && (
        <Grid item className={classes.titleBox}>
          <Typography className={clsx(classes.title, { loading })}>
            {title}
          </Typography>
        </Grid>
      )}
      <Grid item className={classes.valueBox}>
        <Typography className={clsx(classes.value, { loading })}>
          {value}
        </Typography>
      </Grid>
      <Grid item container direction="row" className={classes.buttons}>
        <ScoreButton iconPath={mdiMinus} onClick={() => handleChange(value - 1)} />
        <ScoreButton iconPath={mdiNull} onClick={() => handleChange(0)} />
        <ScoreButton iconPath={mdiPlus} onClick={() => handleChange(value + 1)} />
      </Grid>
    </Grid>
  );
}

ScoreUpdater.propTypes = {
  title: PropTypes.string,
  value: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

ScoreUpdater.defaultProps = {
  title: null,
};
