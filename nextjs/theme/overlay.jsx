import { createTheme } from '@material-ui/core/styles';

import { colors, gradients } from './colors';

const theme = createTheme({
  palette: {
    ...colors,
  },
  gradients: {
    ...gradients,
  },
  overrides: {
    MuiCssBaseline: {
      '@global': {
        body: {
          backgroundColor: 'transparent',
          overflow: 'hidden',
        },
      },
    },
  },
});

export default theme;
