import React, { createContext, useState, useContext } from 'react';

import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';
import { useOverlaySettings } from 'overlay/Settings';

export const GameStatsContext = createContext({});
const ALL_MAPS = {};

export function GameStatsProvider({ cabinetId, children }) {
  const axios = getAxios();
  const settings = useOverlaySettings();
  const [stats, setStats] = useState(null);
  const [subscribed, setSubscribed] = useState(false);

  const ws = useWebSocket('/ws/ingame_stats');

  const subscribe = () => {
    if (ws && settings?.cabinet?.id) {
      setSubscribed(true);
      ws.sendJsonMessage({ type: 'subscribe', cabinet_id: settings.cabinet.id });
    }
  };

  if (settings?.cabinet && !subscribed) {
    subscribe();
  }

  ws.onJsonMessage(message => {
    if (!(message.mapName in ALL_MAPS)) {
      axios.get('/api/game/map/', { params: { name: message.mapName } }).then(response => {
        if (response?.data?.results?.length > 0) {
          ALL_MAPS[message.mapName] = response.data.results[0];
        } else {
          ALL_MAPS[message.mapName] = null;
        }
      });
    }

    setStats(message);
  });

  if (stats === null) {
    return (
      <GameStatsContext.Provider value={null}>
        {children}
      </GameStatsContext.Provider>
    );
  }

  const value = { ...stats };
  if (stats?.mapName in ALL_MAPS) {
    value.gameMap = ALL_MAPS[stats.mapName];
  }

  return (
    <GameStatsContext.Provider value={value}>
      {children}
    </GameStatsContext.Provider>
  );
}

export function useGameStats() {
  return useContext(GameStatsContext);
}
