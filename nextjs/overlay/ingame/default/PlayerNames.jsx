import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';
import PlayerIcon from 'overlay/components/PlayerIcon';
import PlayerName from 'overlay/components/PlayerName';
import PlayerNamesBlock from 'overlay/components/PlayerNamesBlock';

const useStyles = makeStyles(theme => ({
  block: {
    position: 'absolute',
    top: '400px',
  },
  title: {
    width: '160px',
    color: 'white',
    textAlign: 'center',
    fontFamily: 'Roboto',
    fontSize: '15px',
  },
  position: {
    display: 'flex',
    width: '160px',
    margin: '5px 0',
    flexDirection: ({ team, settings }) => (settings?.isTeamOnLeft(team) ? 'row' : 'row-reverse'),
  },
  icon: {
    height: '20px',
    width: '20px',
    margin: '0 4px',
    opacity: 0.3,
    '&.active': {
      opacity: 1,
    },
  },
  name: {
    color: 'white',
    display: 'block',
    flexBasis: 0,
    flexGrow: 1,
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    fontFamily: 'Roboto',
    fontSize: '14px',
    textAlign: ({ team, settings }) => (settings?.isTeamOnLeft(team) ? 'left' : 'right'),
  },
}));

export default function PlayerNames({ team }) {
  const settings = useOverlaySettings();
  const classes = useStyles({ team, settings });

  return (
    <PlayerNamesBlock className={clsx(classes.block, 'player-names')}>
      <div className={clsx(classes.title, 'title')}>Players Signed In</div>

      {POSITIONS_BY_TEAM[team].map(pos => (
        <div className={clsx(classes.position, 'position')} key={pos.ID}>
          <PlayerIcon className={clsx(classes.icon, 'icon')} position={pos} />
          <PlayerName className={clsx(classes.name, 'name')} position={pos} />
        </div>
      ))}
    </PlayerNamesBlock>
  );
}

PlayerNames.propTypes = {
  team: PropTypes.string.isRequired,
};
