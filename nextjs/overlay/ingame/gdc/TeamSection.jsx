import React from 'react';
import Head from 'next/head';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import TeamName from 'overlay/components/TeamName';
import TeamScoreCounter from 'overlay/components/TeamScoreCounter';
import { useMatch } from 'overlay/Match';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: ({ team }) => team == GOLD_TEAM ? 'row-reverse' : 'row',
    marginTop: '13px',
  },
  scoreCounter: {
    display: 'flex',
    flexDirection: ({ team }) => team == GOLD_TEAM ? 'row-reverse' : 'row',
  },
  marker: {
    transform: ({ team }) => team == GOLD_TEAM && 'scaleX(-1)',
    width: '70px',
    height: '50px',
    '&:not(:last-of-type)': {
      marginRight: ({ team }) => team == BLUE_TEAM && '-31px',
      marginLeft: ({ team }) => team == GOLD_TEAM && '-31px',
    },
  },
  markerEmpty: {
  },
  markerFilled: {
  },
  teamName: {
    flexGrow: 1,
    flexBasis: 0,
    fontFamily: 'Teko, Roboto, sans-serif',
    color: 'white',
    fontSize: '42px',
    whiteSpace: 'nowrap',
    overflow: 'hidden',
    textAlign: ({ team }) => team == GOLD_TEAM ? 'left' : 'right',
  },
}));

export default function TeamSection({ team, className }) {
  const classes = useStyles({ team });
  const match = useMatch();

  const MarkerFilled = () => (
    <img
      src={`/static/gdc/game-counters/score-tick-left-${team}@2x.png`}
      className={clsx(classes.marker, classes.markerEmpty)}
    />
  );

  const MarkerEmpty = () => (
    <img
      src={`/static/gdc/game-counters/score-tick-left@2x.png`}
      className={clsx(classes.marker, classes.markerFilled)}
    />
  );

  return (
    <div className={clsx(classes.container, className)}>
      <Head>
        <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Teko:300" />
      </Head>

      <div className={classes.scoreCounter}>
        <TeamScoreCounter team={team} markerFilled={MarkerFilled} markerEmpty={MarkerEmpty} />
      </div>

      <div className={classes.teamName}>
        <TeamName className={classes.teamName} team={team} />
      </div>
    </div>
  );
}
