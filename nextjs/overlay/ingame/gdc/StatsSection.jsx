import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';
import PlayerIconsRow from './PlayerIconsRow';
import StatsRow from './StatsRow';
import SnailPercentageRow from './SnailPercentageRow';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
}));

export default function StatsSection({ className }) {
  const classes = useStyles();

  const queenKillIcon = (
    <span>&#8224;</span>
  );

  return (
    <div className={clsx(classes.container, className)}>
      <PlayerIconsRow className={classes.row} />
      <StatsRow title="Kills" statKey="kills" colorOnChange={{ higher: '#0f0', duration: 1200 }} queenKillIcon={queenKillIcon} />
      <StatsRow title="Deaths" statKey="deaths" colorOnChange={{ higher: '#f00', duration: 1200 }} />
      <StatsRow title="Berries" statKey="totalBerries" colorOnChange={{ higher: '#0f0', duration: 1200 }} />
      <SnailPercentageRow />
      <SnailPercentageRow useEstimatedDistance={true} />
    </div>
  );
}
