import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';
import PlayerIcon from './PlayerIcon';

const useStyles = makeStyles(theme => ({
  logo: {
    width: '120px',
    height: '79px',
    backgroundRepeat: 'no-repeat',
    backgroundPosition: 'center center',
    backgroundSize: '120px 50px',
    backgroundImage: 'url(/static/gdc/kqsf_logo.png)',
  },
}));

export default function PlayerIconsRow({ className }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.row, className)}>

      {[...POSITIONS_BY_TEAM[BLUE_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={classes.icon}>
          <PlayerIcon position={pos} />
        </div>
      ))}

      <div className={classes.logo} />

      {[...POSITIONS_BY_TEAM[GOLD_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={classes.icon}>
          <PlayerIcon position={pos} />
        </div>
      ))}

    </div>
  );
}
