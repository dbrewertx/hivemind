import clsx from 'clsx';
import Head from 'next/head';

import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, LEFT_TEAM, RIGHT_TEAM } from 'util/constants';
import classes from './manuka/Chicago-League.module.css';

import CurrentTime from 'overlay/components/CurrentTime';
import EventName from 'overlay/components/EventName';
import Jumbotron from './manuka/Jumbotron';
import KillFeed from './manuka/KillFeed';
import PlayersLoggedIn from './manuka/PlayersLoggedIn';
import TeamSection from './manuka/TeamSection';

export default function Manuka({}) {
  const settings = useOverlaySettings();
  const flipTeams = settings?.isTeamOnLeft(GOLD_TEAM);
  const leftTeam = flipTeams ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = flipTeams ? BLUE_TEAM : GOLD_TEAM;

  return (
    <>
      <Head>
        {settings?.additionalFonts && <link rel="stylesheet" href={settings.additionalFonts} />}
      </Head>

      <div
        className={clsx(
          classes.overlay,
          'ingame-overlay text-[#ef2694] custom-color',
          flipTeams && classes.flipTeams,
        )}
      >
        {/* <div
          ID="DELETEME"
          className={clsx(
            'absolute top-[56px] left-[240px] z-50 w-[1440px] h-[810px] aspect-video',
            'bg-gradient-to-b from-gray-800 to-gray-900',
          )}
        ></div> */}

        <div className={clsx('w-full h-full relative')}>
          <div id="clock-time-container" className="absolute top-[104px] left-[1710px]">
            <CurrentTime
              className={clsx('relative text-left flex flex-col', 'font-stat-numbers')}
              dateClass="order-2"
              timeFormat="a   h:mm"
              dateFormat="MMM dd.yyyy"
              hideDivider
            />
          </div>
          <div
            id="event-cab-title"
            className="text-center absolute left-1/2 top-3 -translate-x-1/2 z-50 text-3xl text-white font-bold "
            style={{
              textShadow: '0 0 3px black, 0 0 2px black, 0 0 1px black',
            }}
          >
            <EventName id="event-title" className={clsx(classes.eventName)} />
            <span id="event-cab-divider" className="event-cab-divider mx-2 first:hidden">
              -
            </span>
            <span id="cab-name">{settings?.cabinet?.displayName}</span>
          </div>
          <div id="customFrameContainer" className="">
            <div
              id="customFrameVideoWindow"
              className={clsx(
                'absolute z-30 w-[1440px] h-[810px] top-[56px] left-[240px] rounded mix-blend-overlay',
                // ' shadow-[0_0_0_20px_currentColor,0_10px_0_20px_currentColor]',
                // 'text-yellow-500 shadow-[0_10px_0_20px_currentColor]',
                "after:contentr-[''] before:content-['']",
                'before:rounded-sm before:shadow-[0_0_15px_5px_rgba(25,25,25,.75)] before:mix-blend-overlay before:inset-0 before:absolute before:z-10 before:block',
                ' after:text-inherit after:absolute after:top-full after:h-8 after:bg-current after:w-[1920px] after:left-1/2 after:-translate-x-1/2 after:z-[9]',
              )}
            ></div>
            <svg
              id="customFrame"
              data-name="Layer 2"
              xmlns="http://www.w3.org/2000/svg"
              viewBox="0 0 1920 1080"
              className="absolute top-0 left-0 w-[1920px] aspect-video z-20 custom-color  "
            >
              <defs>
                <mask id="video">
                  <rect y="0" width="1920" height="1080" fill="white" />
                  <rect x="240" y="56" width="1440" height="810" fill="black" />
                </mask>
              </defs>
              <g mask="url(#video)">
                <polygon
                  points="1695.66 181.75 1706.82 96.63 1684.87 67.28 1435.76 9.38 1305.55 23.33 1276.31 9.38 960 37.29 643.69 9.38 614.45 23.33 484.24 9.38 235.13 67.28 213.18 96.63 224.34 181.75 215.84 286.2 240 425.27 220.07 598.01 229.13 675.96 217.61 747.27 249.63 833.52 245.14 903.51 294.17 888.19 327.45 895.64 529.42 910.26 614.91 927.45 677.35 914.2 715.46 925.42 755.53 914.2 832.87 943.47 960 887.67 1087.13 943.47 1164.47 914.2 1204.54 925.42 1242.65 914.2 1305.09 927.45 1390.58 910.26 1592.55 895.64 1625.83 888.19 1674.86 903.51 1670.37 833.52 1702.39 747.27 1690.87 675.96 1699.93 598.01 1680 425.27 1704.16 286.2 1695.66 181.75"
                  fill="currentColor"
                  className="custom-frame-bottom opacity-50"
                />
                <polygon
                  points="1690.78 124.98 1712.74 55.5 1690.71 19.35 1423.8 38.61 960 0 496.2 38.61 229.29 19.35 207.26 55.5 229.22 124.98 220.07 404.01 234.44 658.68 220.07 895.64 314.16 878.37 556.21 912.69 759.29 874.9 877.87 903.51 960 875.71 1042.13 903.51 1160.71 874.9 1363.79 912.69 1605.84 878.37 1699.93 895.64 1685.56 658.68 1699.93 404.01 1690.78 124.98"
                  fill="currentColor"
                  className="custom-frame-top"
                />
              </g>
            </svg>
          </div>

          {/* <div id="DELETEME" className="w-full h-full absolute z-[-1] bg-indigo-950"></div> */}

          <PlayersLoggedIn flipTeams={flipTeams} />
        </div>
        <div id="kill-feed-container" className="absolute bottom-0 w-full px-[250px] z-50 h-8">
          <KillFeed flipTeams={flipTeams} />
        </div>
        <div
          className={clsx(
            'bottom-bar',
            'absolute bottom-0 w-[1920px] h-[213px] !left-1/2 -translate-x-1/2 z-40',
            'flex justify-center',
          )}
        >
          <TeamSection
            className={clsx('team-section-left', 'absolute left-0 bottom-0 w-1/2 pb-5')}
            team={leftTeam}
            side={LEFT_TEAM}
            key="leftteamsection"
          />

          <Jumbotron flipTeams={flipTeams} />

          <TeamSection
            className={clsx(
              'team-section-right',
              'absolute right-0 bottom-0 w-1/2 text-right pb-5',
            )}
            team={rightTeam}
            side={RIGHT_TEAM}
            key="rightteamsection"
          />
        </div>
      </div>
    </>
  );
}

Manuka.themeProps = {
  name: 'manuka',
  displayName: 'Manuka',
  description: `Supercharged overlay with lots of stats and a jumbotron`,
  size: { w: 1920, h: 1080 },
  overlayWindow: { w: 1920, h: 1080, x: 0, y: 0 },
  gameCaptureWindow: { w: 1440, h: 810, x: 240, y: 57 },
  blueCameraWindow: { w: 700, h: 175, x: 0, y: 905 },
  goldCameraWindow: { w: 700, h: 175, x: 1220, y: 905 },
};
