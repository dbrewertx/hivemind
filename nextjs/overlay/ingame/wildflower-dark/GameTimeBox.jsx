import React from 'react';
import { mdiTimer } from '@mdi/js';

import TextWithIcon from './TextWithIcon';
import GameTime from 'overlay/components/GameTime';

export default function GameTimeBox({ ...props }) {
  return (
    <TextWithIcon path={mdiTimer} className="game-time" {...props}>
      <GameTime />
    </TextWithIcon>
  );
}
