import React, { useState } from 'react';
import clsx from 'clsx';
import { makeStyles } from '@material-ui/core/styles';
import { GOLD_TEAM, POSITIONS_BY_ID } from 'util/constants';

import { useGameStats } from 'overlay/GameStats';

const calcHatOffset = (position) => {
  const offset = (POSITIONS_BY_ID[position.ID].POSITION === 'queen') ? 2 : 5;
  const val =  position.TEAM == GOLD_TEAM ? offset : offset * -1 ;
  return val + 'px'
}
const useStyles = makeStyles(theme => ({
  hattrick: {
    height: '36px',
    width: '48px',
    backgroundRepeat: 'no-repeat',
    bacakgroundPosition: 'center',
    backgroundImage: `url(/static/chicago/hat-trick-hat.png)`,
    animation: '$slideDown 1s forwards 1 ease-in-out',
    position: 'relative',
    margin: '0 auto',
    left: ({position}) => calcHatOffset(position),
    top: ({position}) => POSITIONS_BY_ID[position.ID].POSITION === 'queen' ? '-7px' : 0,
  },
  '@keyframes slideDown': {
    '0%': {
      opacity: 0,
      transform: 'translateY(-100%) scale(1.25)'
    },
    '100%': {
      opacity: 1,
      transform: 'translateY(0%) scale(1)'
    },
  }

}));

export default function QueenKillIconContainer({ className, position, element }) {
  const stats = useGameStats();
  const numKills = parseInt(stats?.queenKills[position.ID]) ?? 0;
  const styles = useStyles({ position});
  return (
    <div className={className}>
      {(numKills > 0 && numKills < 3) && [...Array(numKills)].map((i, idx) => (
        <React.Fragment key={idx}>
          {element}
        </React.Fragment>
      )) }
      { (numKills === 3) &&  (
        <div className={styles.hattrick}></div>
      )}
    </div>
  );
}
