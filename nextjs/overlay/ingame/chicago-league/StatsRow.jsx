import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import StatValue from 'overlay/components/StatValue';
import QueenKillIconContainer from 'overlay/components/QueenKillIconContainer';
import { POSITIONS_BY_TEAM, BLUE_TEAM, GOLD_TEAM } from 'util/constants';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
    borderBottom: '2px solid #ffffff19',
    '&:first-child': {
      borderTop: '2px solid #ffffff19',
    }
  },
  cell: {
    textAlign: 'center',
    padding: '3px 0 1px 0',
    display: 'flex',
    flexDirection: 'row',
    width: '58px',
    justifyContent: 'center',
  },
  value: {
  },
  valueText: {
    color: 'white',
    fontSize: '18px',
    fontFamily: 'monospace',
    '&.zero': {
      color: '#ffffff3f',
    },
  },
  title: {
    width: '120px',
    textTransform: 'uppercase',
    color: '#ffffff7f',
    fontSize: '20px',
    fontFamily: 'Roboto Condensed'
  },
  queenKillContainer: {
    color: 'white',
    marginLeft: '3px',
  },
}));

export default function StatsRow({ className, title, queenKillIcon, ...props }) {
  const classes = useStyles();

  return (
    <div className={clsx(classes.row, className)}>

      {[...POSITIONS_BY_TEAM[BLUE_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <StatValue position={pos} className={classes.valueText} {...props} />
          {queenKillIcon && (
            <QueenKillIconContainer position={pos} className={classes.queenKillContainer} element={queenKillIcon} />
          )}
        </div>
      ))}

      <div className={clsx(classes.cell, classes.title)}>{title}</div>

      {[...POSITIONS_BY_TEAM[GOLD_TEAM]].reverse().map(pos => (
        <div key={pos.ID} className={clsx(classes.cell, classes.value)}>
          <StatValue position={pos} className={classes.valueText} {...props} />
          {queenKillIcon && (
            <QueenKillIconContainer position={pos} className={classes.queenKillContainer} element={queenKillIcon} />
          )}
        </div>
      ))}

    </div>
  );
}
