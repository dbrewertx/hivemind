import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM, POSITIONS_BY_TEAM } from 'util/constants';
import PlayerIconsRow from './PlayerIconsRow';
import StatsRow from './StatsRow';
import ComboStatsRow from './ComboStatRow';
import GameTimeBox from './GameTimeBox';
import BerriesRemainingBox from './BerriesRemainingBox';
import styles from './Chicago-League.module.css';
import SnailIcon from 'components/SnailIcon';

const useStyles = makeStyles(theme => ({
  row: {
    display: 'flex',
    flexDirection: 'row',
  },
  snailBerries: {
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    gap: '4px'
  },
  stack : {
    display: 'flex',
    justifyContent: 'center',
    flexDirection: 'column',
    fontSize: '13px',
    lineHeight: '14px'
  }
}));

export default function StatsSection({ className }) {
  const classes = useStyles();


  const berriesLeft = <BerriesRemainingBox className={styles.berriesRemaining} />
  const objective = (<div className={classes.snailBerries}>
    <SnailIcon style={{width: '20px', height: '20px'}} /> {'-'}
    {berriesLeft}
  </div>)
  const droneKills = (<div className={classes.stack}><span>Drone Kills /</span><span>Deaths as Drone</span></div>)
  return (
    <div className={clsx(classes.container, className)}>
      <PlayerIconsRow className={classes.row} />
      <GameTimeBox className={styles.gameTime} />
      <ComboStatsRow title="Military K/D" statKeys={['militaryKills', 'militaryDeaths']} colorOnChange={[{ higher: '#0f0', duration: 1200 },{ higher: '#f00', duration: 1200 }]} />
      <ComboStatsRow title="Drone K/D" statKeys={['workerKills', 'workerDeaths']} colorOnChange={[{ higher: '#0f0', duration: 1200 },{ higher: '#f00', duration: 1200 }]} queenMerge={true} />
      <ComboStatsRow title={objective} statKeys={['snailDistance', 'totalBerries']} divider=" - " colorOnChange={{ higher: '#f00', duration: 1200 }} queenDelete={true} />
      {/* <StatsRow title={berriesLeft} statKey="totalBerries" colorOnChange={{ higher: '#0f0', duration: 1200 }} /> */}
    </div>
  );
}
