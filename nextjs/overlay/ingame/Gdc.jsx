import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';
import TeamSection from './gdc/TeamSection';
import StatsSection from './gdc/StatsSection';

const useStyles = makeStyles(theme => ({
  headerBar: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'flex-start',
    justifyContent: 'center',
    backgroundColor: '#000000bf',
    height: '216px',
  },
  teamSection: {
    flexGrow: 1,
    flexBasis: 0,
    overflow: 'hidden',
  },
  statsSection: {
    flexGrow: 0,
  },
}));

export default function GdcOverlay({ }) {
  const classes = useStyles();

  return (
    <>
      <div className={classes.headerBar}>
        <TeamSection className={clsx(classes.teamSection, classes.blue)} team={BLUE_TEAM} />
        <StatsSection className={classes.statsSection} />
        <TeamSection className={clsx(classes.teamSection, classes.gold)} team={GOLD_TEAM} />
      </div>
    </>
  );
}

GdcOverlay.themeProps = {
  name: 'gdc',
  displayName: 'GDC V',
  description: `San Francisco's GDC V overlay.`,
  size: { w: 1920, h: 1080 },
  overlayWindow: { w: 1536, h: 216, x: 0, y: 0 },
  gameCaptureWindow: { w: 1536, h: 864, x: 0, y: 216 },
  blueCameraWindow: { w: 320, h: 180, x: 0, y: 0 },
  goldCameraWindow: { w: 320, h: 180, x: 1216, y: 0 },
};

