import ChicagoLeague from './ChicagoLeague';
import CssOverride from './CssOverride';
import Default from './Default';
import Gdc from './Gdc';
import Manuka from './Manuka';
import Wildflower from './Wildflower';
import WildflowerDark from './WildflowerDark';

const allIngameThemes = [
  Default,
  Wildflower,
  WildflowerDark,
  Manuka,
  Gdc,
  ChicagoLeague,
  CssOverride,
];

export function getIngameTheme(name) {
  for (const theme of allIngameThemes) {
    if (theme.themeProps.name == name) {
      return theme;
    }
  }
}

export { allIngameThemes };
