import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatchStats } from 'overlay/MatchStats';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM } from 'util/constants';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'justify-between',
  },
  matchInfo: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  teamContainer: {
    display: 'flex',
    flexDirection: 'row',
    '&:last-of-type': {
      flexDirection: 'row-reverse',
    },
    flexBasis: 0,
    flexGrow: 1,
    justifyContent: 'space-between',
    padding: '5px 15px',
  },
  teamName: {},
  score: {
    flexGrow: 0,
    width: '50px',
    flexBasis: '50px',
    textAlign: 'center',
  },
  blue: {
    background: theme.palette.blue.main,
  },
  gold: {
    background: theme.palette.gold.main,
  },
}));

export default function MatchSummary({ className }) {
  const classes = useStyles();
  const match = useMatchStats();
  const settings = useOverlaySettings();

  if (match === null) {
    return <></>;
  }

  const blueTeam = {
    slug: BLUE_TEAM,
    name: match?.blueTeam?.name,
    score: match?.blueScore,
  };

  const goldTeam = {
    slug: GOLD_TEAM,
    name: match?.goldTeam?.name,
    score: match?.goldScore,
  };

  const teams = settings?.goldOnLeft ? [goldTeam, blueTeam] : [blueTeam, goldTeam];
  const winner = blueTeam.score < goldTeam.score ? GOLD_TEAM : BLUE_TEAM;

  return (
    <div className={clsx(classes.container, className)}>
      <div className={classes.matchInfo}>
        {/* <div className={classes.tournamentName}>{match?.tournament?.name}</div> */}
        <div
          id="match-round-name"
          className={clsx(
            'font-player-names font-bold text-white text-shadow-thicc text-6xl -mt-14 mb-4',
          )}
        >
          {match?.roundName}
        </div>
      </div>

      <div
        className={clsx(
          'grid grid-cols-2 rounded-full overflow-hidden bg-gradient-to-b from-white to-gray-300',
        )}
      >
        {teams.map(team => (
          <div
            key={team.slug}
            className={clsx(classes.teamContainer, 'p-0 ', {
              'opacity-100': winner !== team.slug,
            })}
          >
            <div
              className={clsx(
                'font-custom font-bold text-6xl px-16 pt-5 pb-4 text-transparent ',
                team.slug == BLUE_TEAM ? 'blue-text-gradient' : 'gold-text-gradient',
              )}
            >
              {team.name}
            </div>
            <div
              className={clsx(
                classes[team.slug],
                'font-stat-numbers text-5xl place-content-center text-center grid grid-cols-1 w-24 mix-blend-multiply',
              )}
            >
              <div className="scale-150">{team.score}</div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
}
