import Default from './Default';

const allMatchThemes = [Default];

export function getMatchTheme(name) {
  for (const theme of allMatchThemes) {
    if (theme.themeProps.name == name) {
      return theme;
    }
  }
}

export { allMatchThemes };
