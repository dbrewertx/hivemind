import { createContext, useContext, useEffect, useReducer } from 'react';

import { useOverlaySettings } from 'overlay/Settings';
import { OVERLAY_SCENES } from 'util/constants';
import { useWebSocket } from 'util/websocket';

export const SceneControllerContext = createContext(null);

const reducer = (state, action) => {
  switch (action.type) {
    case 'tick':
      if (!state || state.sceneChanges.length == 0) {
        return state;
      }

      const time = Date.now();
      let changed = false;

      for (const change of state.sceneChanges) {
        if (change.time <= time) {
          state.scene = change.scene;
          if (change.obsScene && window.obsstudio?.setCurrentScene) {
            window.obsstudio.setCurrentScene(change.obsScene);
          }
          changed = true;
        }
      }
      state.sceneChanges = state.sceneChanges.filter(c => c.time > time);
      return changed ? { ...state } : state;

    case 'clear':
      state.scene = OVERLAY_SCENES.HIDDEN;
      state.sceneChanges = [];

      state.gameRunning = true;

      return { ...state };

    case 'gameend':
      state.endTime = Date.now();
      state.sceneChanges = [];

      state.sceneChanges.push({
        time: state.endTime + 6000,
        scene: OVERLAY_SCENES.POSTGAME,
        obsScene: '[HMAutoPostGame]',
      });
      state.sceneChanges.push({
        time: state.endTime + 21000,
        scene: OVERLAY_SCENES.HIDDEN,
        obsScene: '[HMAutoGame]',
      });

      state.gameRunning = false;

      return { ...state };

    case 'match':
      const currentMatch = action.message.currentMatch;

      if (
        state.gameRunning ||
        currentMatch === null ||
        currentMatch.blueScore > 0 ||
        currentMatch.goldScore > 0
      ) {
        return state;
      }

      let changeTime = Date.now() + 1000;
      if (state.sceneChanges.length > 0) {
        for (const change of state.sceneChanges) {
          if (change.scene == OVERLAY_SCENES.HIDDEN) {
            changeTime = change.time + 1000;
          }
        }
      }

      state.sceneChanges.push({ time: changeTime, scene: OVERLAY_SCENES.MATCH_PREVIEW });

      return { ...state };

    case 'matchend':
      for (const change of state.sceneChanges) {
        if (change.scene == OVERLAY_SCENES.HIDDEN) {
          change.scene = OVERLAY_SCENES.MATCH_SUMMARY;
          state.sceneChanges.push({ time: change.time + 30000, scene: OVERLAY_SCENES.HIDDEN });

          return { ...state };
        }
      }

      return state;

    default:
      throw new Error();
  }
};

export function SceneController({ children }) {
  const settings = useOverlaySettings();
  const initialState = {
    scene: OVERLAY_SCENES.HIDDEN,
    endTime: null,
    sceneChanges: [],
    gameRunning: true,
  };

  const [state, dispatch] = useReducer(reducer, initialState);

  useEffect(() => {
    setInterval(() => {
      dispatch({ type: 'tick' });
    }, 250);
  }, []);

  const ws = useWebSocket('/ws/gamestate');

  ws.onJsonMessage(
    message => {
      if (message.cabinetId != settings?.cabinet?.id) {
        return;
      }

      if (message.type == 'gameend') {
        dispatch({ type: 'gameend' });
      }

      if (message.type == 'matchend') {
        dispatch({ type: 'matchend' });
      }

      if (message.type == 'match') {
        dispatch({ type: 'match', message });
      }

      // Override and hide everything if a game starts
      if (message.type == 'gamestart' || message.type == 'playernames') {
        dispatch({ type: 'clear' });
      }
    },
    [settings?.cabinet?.id],
  );

  return (
    <SceneControllerContext.Provider value={state.scene}>
      {children}
    </SceneControllerContext.Provider>
  );
}

export function useSceneController() {
  return useContext(SceneControllerContext);
}
