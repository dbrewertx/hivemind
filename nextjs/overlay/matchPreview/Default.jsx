import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { useMatch } from 'overlay/Match';
import { useSceneController } from 'overlay/SceneController';
import { useOverlaySettings } from 'overlay/Settings';
import { BLUE_TEAM, GOLD_TEAM, OVERLAY_SCENES } from 'util/constants';
import EventNameBox from './default/EventNameBox';
import TeamNameBox from './default/TeamNameBox';

const useStyles = makeStyles(theme => ({
  container: {
    position: 'absolute',
    width: 1920,
    height: 1080,
    top: 0,
    left: 0,
    overflow: 'hidden',
  },
  eventName: {
    position: 'absolute',
    top: '-432px',
    left: '384px',
    width: '1152px',
    height: '114px',
    borderRadius: '20px',
    boxShadow: '3px 3px 2px rgba(0, 0, 0, 0.5)',
    background: theme.gradients.neutral.light,
    textAlign: 'center',
    fontSize: '54px',
    padding: '20px',
    fontWeight: 'bold',
    transition: 'top 1s',
    '.active &': {
      top: '54px',
    },
  },
  teamName: {
    position: 'absolute',
    top: '222px',
    width: '768px',
    borderRadius: '20px',
    boxShadow: '3px 3px 2px rgba(0, 0, 0, 0.5)',
    transition: 'left 1s',
  },
  teamNameLeft: {
    left: '-1152px',
    '.active &': {
      left: '96px',
    },
  },
  teamNameRight: {
    left: '110%',
    '.active &': {
      left: '1056px',
    },
  },
}));

export default function DefaultOverlay({}) {
  const classes = useStyles();
  const settings = useOverlaySettings();
  const scene = useSceneController();
  const match = useMatch();

  const active = (scene === OVERLAY_SCENES.MATCH_PREVIEW || scene === null) && match;

  const leftTeam = settings?.goldOnLeft ? GOLD_TEAM : BLUE_TEAM;
  const rightTeam = settings?.goldOnLeft ? BLUE_TEAM : GOLD_TEAM;

  return (
    <div id="matchPreviewOverlay" className={clsx(classes.container, { active })}>
      <EventNameBox className={clsx(classes.eventName, 'event-name')} />
      <TeamNameBox
        className={clsx(classes.teamName, classes.teamNameLeft, 'team', 'left-team')}
        team={leftTeam}
      />
      <TeamNameBox
        className={clsx(classes.teamName, classes.teamNameRight, 'team', 'right-team')}
        team={rightTeam}
      />
    </div>
  );
}

DefaultOverlay.themeProps = {
  name: 'default',
  displayName: 'Clover (Default)',
  description: `HiveMind's default match preview theme.`,
};
