import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import EventName from 'overlay/components/EventName';
import { useMatch } from 'overlay/Match';

const useStyles = makeStyles(theme => ({}));

export default function EventNameBox({ team, className }) {
  const classes = useStyles({ team });
  const match = useMatch();

  return (
    <div className={clsx(classes.container, className, 'event-container')}>
      <EventName />
    </div>
  );
}
