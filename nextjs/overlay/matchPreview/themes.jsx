import Default from './Default';
import Manuka from './Manuka';

const allMatchPreviewThemes = [Default, Manuka];

export function getMatchPreviewTheme(name) {
  for (const theme of allMatchPreviewThemes) {
    if (theme.themeProps.name == name) {
      return theme;
    }
  }
}

export { allMatchPreviewThemes };
