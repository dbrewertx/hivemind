import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { GOLD_TEAM } from 'util/constants';

const useStyles = makeStyles(theme => ({
  player: ({ team }) => ({
    flexBasis: 0,
    flexGrow: 1,
    overflow: 'hidden',
    display: 'flex',
    flexDirection: team == GOLD_TEAM ? 'row-reverse' : 'row',
    alignItems: 'center',
    justifyContent: 'center',
    padding: '0 20px',
  }),
  image: {
    // display: 'none',
    width: '125px',
    height: '125px',
    overflow: 'hidden',
    flexGrow: 0,
    flexShrink: 0,
    flexBasis: '125px',
    '& img': {
      width: '125px',
      height: '125px',
      objectFit: 'cover',
    },
    '.player-names.pics &': {
      display: 'block',
    },
  },
}));

export default function PlayerName({ player, team, className }) {
  const classes = useStyles({ team });

  return (
    <div className={clsx(classes.player, 'player flex justify-between px-16 py-1')}>
      <div className={clsx(classes.image, 'player-image bg-white/10 overflow-hidden rounded-lg')}>
        <img
          src={
            player.image && !player.doNotDisplay ? player.image : '/static/chicago/snailvatar.png'
          }
        />
      </div>
      <div className={clsx(classes.text, 'text', team === 'blue' && 'text-right')}>
        <div
          className={clsx(
            classes.playerName,
            'player-name cheee-lowgrav tracking-wider text-4xl',
            team === 'blue' && 'text-white text-shadow-thicc',
          )}
        >
          {player.name}
        </div>
        <div
          className={`player-meta space-x-2 font-bold text-2xl uppercase flex items-center ${
            team === 'blue' ? 'justify-end' : 'justify-start'
          }`}
        >
          {player.scene && <span className="player-scene">[{player.scene}]</span>}
          {player.pronouns && (
            <span className={clsx('pronouns italic text-[.8em]')}>{player.pronouns}</span>
          )}
        </div>
        {player.tidbit && <div className={clsx(classes.tidbit, 'tidbit')}>{player.tidbit}</div>}
      </div>
    </div>
  );
}
