import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import EventName from 'overlay/components/EventName';
import { useMatch } from 'overlay/Match';

const useStyles = makeStyles(theme => ({}));

export default function EventNameBox({ team, className }) {
  const classes = useStyles({ team });
  const match = useMatch();

  return (
    <div id="previewEventNameContainer" className={clsx(classes.container, className)}>
      <EventName id="previewEventName" />
      <svg
        id="previewEventNameBgFrame"
        data-name="Layer 2"
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 1380 164"
        className="absolute top-0 left-0 -z-10 text-[mediumvioletred] custom-color"
      >
        <path
          d="m1365.9,112.62c-128.85,85.38-70.75-114.55-34.54-66.95,30.85,40.55-4.16,60.28,15.5,60.28,15.05,0,33.13-41.25-9.74-66.7-28.14-16.71-80.07-28.36-116.78-39.24H159.66c-36.71,10.88-88.65,22.54-116.78,39.24C0,64.7,18.08,105.94,33.14,105.94c19.65,0-15.35-19.73,15.5-60.28C84.85-1.94,142.94,197.99,14.1,112.62c-41.32-27.38,12.07,89.94,109.7,37.91,97.63-52.03,200.83-75.32,222.83-43.02,43.17,63.37,140.53-12.82,343.38-12.82s300.21,76.18,343.38,12.82c22-32.29,125.2-9,222.83,43.02,97.63,52.03,151.02-65.29,109.7-37.91Z"
          fill="currentColor"
        />
      </svg>
    </div>
  );
}
