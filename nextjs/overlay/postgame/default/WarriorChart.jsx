import { makeStyles } from '@material-ui/core/styles';

import BaseWarriorChart from 'components/charts/BaseWarriorChart';
import { usePostgameStats } from 'overlay/PostgameStats';
import ChartContainer from './ChartContainer';

const useStyles = makeStyles(theme => ({}));

export default function WarriorChart({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  return (
    <ChartContainer title="Wars Up Queens Dwn" className={className}>
      {stats?.warriorData && <BaseWarriorChart game={stats} datasetProps={{ pointRadius: 0 }} />}
    </ChartContainer>
  );
}
