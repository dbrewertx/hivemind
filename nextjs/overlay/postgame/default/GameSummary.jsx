import { makeStyles } from '@material-ui/core/styles';
import clsx from 'clsx';

import { usePostgameStats } from 'overlay/PostgameStats';
import GameLinkQrCode from './GameLinkQrCode';

const useStyles = makeStyles(theme => ({
  container: {
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
  },
  textSection: {
    flexBasis: 0,
    flexGrow: 1,
    padding: '0 1vw',
  },
  victoryImage: {
    width: '4vw',
    height: '4vw',
    marginLeft: '0.5vw',
  },
  winDescription: {
    fontSize: '2.5vw',

    fontWeight: 'bold',
    textAlign: 'center',
    borderBottom: '1px solid #4f4f4f',
    marginBottom: '1vw',
  },
  gameInfo: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-evenly',
  },
  gameInfoEntry: {
    fontSize: '1.5vw',
  },
  summaryRows: {
    flexGrow: 1,
  },
}));

export default function GameSummary({ className }) {
  const classes = useStyles();
  const stats = usePostgameStats();

  if (stats === null) {
    return <></>;
  }

  return (
    <div className={clsx(classes.container, className)}>
      <div className={clsx(classes.textSection, 'text-section')}>
        <div
          className={clsx(
            classes.winDescription,
            'win-description cheee-lowgrav tracking-wider text-6xl bg-clip-text text-transparent',
            stats.winningTeam === 'blue'
              ? 'bg-gradient-to-b from-blue-500 to-blue-800'
              : 'bg-gradient-to-b from-gold-main from-10% to-50% to-gold-dark3',
          )}
        >
          {stats?.winningTeamDisplay ?? '???'} Victory
        </div>
        <div className={clsx(classes.gameInfo, 'game-info font-bold justify-between')}>
          <div className={clsx(classes.gameInfoEntry, 'game-info-entry')}>{stats.map}</div>
          <div className={clsx(classes.gameInfoEntry, 'game-info-entry')}>
            {stats.winConditionDisplay}
          </div>
          <div className={clsx(classes.gameInfoEntry, 'game-info-entry')}>{stats.length}</div>
          <div className={clsx(classes.gameInfoEntry, 'game-info-entry')}>
            Game ID: {stats.gameId}
          </div>
        </div>
      </div>
      <GameLinkQrCode className={classes.qrCode} />
    </div>
  );
}
