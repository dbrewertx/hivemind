import Default from './Default';
import Wildflower from './Wildflower';

const allPostgameThemes = [Default, Wildflower];

export function getPostgameTheme(name) {
  for (const theme of allPostgameThemes) {
    if (theme.themeProps.name == name) {
      return theme;
    }
  }
}

export { allPostgameThemes };
