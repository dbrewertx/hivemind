import React, { createContext, useState, useContext, useEffect } from 'react';
import PropTypes from 'prop-types';

import { OVERLAY_SCENES } from 'util/constants';
import { getAxios } from 'util/axios';
import { useWebSocket } from 'util/websocket';
import { useOverlaySettings } from 'overlay/Settings';
import { useSceneController } from 'overlay/SceneController';

export const PostgameStatsContext = createContext(null);

export function PostgameStatsProvider({ children }) {
  const axios = getAxios();
  const settings = useOverlaySettings();
  const scene = useSceneController();
  const [stats, setStats] = useState(null);
  const [hideTimeout, setHideTimeout] = useState(null);
  const [subscribed, setSubscribed] = useState(false);

  const ws = useWebSocket('/ws/gamestate');

  const subscribe = () => {
    if (ws && settings?.cabinet?.id) {
      setSubscribed(true);
      ws.sendJsonMessage({
        type: 'subscribe',
        scene_name: settings.cabinet.scene.name,
        cabinet_name: settings.cabinet.name,
      });
    }
  };

  useEffect(() => {
    if (settings?.cabinet && !subscribed) {
      subscribe();
    }
  }, [settings?.cabinet?.id]);

  const loadGame = async (id) => {
    setStats(null);

    let response = await axios.get(`/api/game/game/${id}/stats/`);
    const stats = response.data

    stats.loadedTime = Date.now();

    setStats(stats);
  };

  ws.onJsonMessage(message => {
    if (message.type == 'gameend' && message.cabinetId == settings?.cabinet?.id) {
      loadGame(message.gameId);
    }
  }, [settings?.cabinet?.id]);

  useEffect(() => {
    if (settings !== null && stats === null) {
      axios.get(`/api/game/game/recent/`, { params: { cabinetId: settings.cabinet.id } }).then(response => {
        if (response?.data?.results?.length > 0) {
          loadGame(response.data.results[0].id)
        }
      });
    }
  }, [settings?.cabinet?.id]);

  const value = {
    ...stats,
    visible: scene === OVERLAY_SCENES.POSTGAME || scene === null,
  }

  return (
    <PostgameStatsContext.Provider value={value}>
      {children}
    </PostgameStatsContext.Provider>
  );
}

PostgameStatsProvider.propTypes = {
  visible: PropTypes.bool,
  children: PropTypes.oneOfType([PropTypes.node, PropTypes.arrayOf(PropTypes.node)]),
};


export function usePostgameStats() {
  return useContext(PostgameStatsContext);
}
