import React, { useState } from 'react';

import { useOverlaySettings } from 'overlay/Settings';

export default function CabinetName({ className }) {
  const settings = useOverlaySettings();

  return (
    <span className={className}>
      {settings?.cabinet?.scene?.displayName} - {settings?.cabinet?.displayName}
    </span>
  );
}
