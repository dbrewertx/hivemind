import { useMatch } from 'overlay/Match';

export default function TeamName({ team, className }) {
  const match = useMatch();

  let name = match.teamName[team];

  return <span className={className}>{name ?? ''}</span>;
}
