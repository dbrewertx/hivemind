import clsx from 'clsx';
import { POSITIONS_BY_TEAM } from 'util/constants';

export default function WebcamPlayerIcons({ team, className }) {
  return (
    <div className={clsx('webcam-player-icons flex justify-between gap-2', className)}>
      {POSITIONS_BY_TEAM[team]?.reverse().map(pos => (
        <div
          key={pos.ID}
          className="min-w-6 h-5 flex-grow bg-no-repeat bg-center opacity-75"
          style={{ backgroundImage: `url("${pos?.ICON}")` }}
        ></div>
      ))}
    </div>
  );
}
