import clsx from 'clsx';
import PropTypes from 'prop-types';
import { useState } from 'react';
import useColorChange from 'use-color-change';

import { useGameStats } from 'overlay/GameStats';

export default function SnailPercentageValue({
  className,
  position,
  colorOnChange,
  useEstimatedDistance = false,
  directionCb,
}) {
  const stats = useGameStats();
  const [value, setValue] = useState(0);
  const colorStyle = useColorChange(value, colorOnChange);

  if (stats === null) {
    return <span className={clsx(className, 'zero', 'missing', 'loading')}>{0}</span>;
  }

  let statValue = stats.snailDistance;
  if (useEstimatedDistance && stats.estimatedSnailDistance) {
    statValue = stats.estimatedSnailDistance;
  }
  if (statValue === undefined) {
    return <span className={clsx(className, 'zero', 'missing')}>{0}</span>;
  }

  if (position) statValue = statValue[position?.ID] ?? 0;
  else {
    const distances = [0, 0];
    Object.keys(stats?.estimatedSnailDistance).forEach(key => {
      const arrIdx = Number(key) % 2; // 0 = gold, 1 = blue
      distances[arrIdx] += stats?.estimatedSnailDistance[key];
    });
    const [goldDist, blueDist] = distances;
    let difference = blueDist - goldDist;
    if (directionCb) directionCb(difference >= 0 ? 'left' : 'right');
    statValue = Math.abs(difference);
  }

  if (value != statValue) {
    setValue(statValue);
  }

  if (!stats.gameMap?.snailTrackWidth) {
    return (
      <span style={colorStyle} className={clsx(className, { zero: value === 0 })}>
        {Math.round(value / 21)} m
      </span>
    );
  }

  return (
    <span style={colorStyle} className={clsx(className, { zero: value === 0 })}>
      {Math.round((value / stats.gameMap.snailTrackWidth) * 100)}%
    </span>
  );
}

SnailPercentageValue.propTypes = {
  className: PropTypes.string,
  position: PropTypes.object,
  colorOnChange: PropTypes.object,
};

SnailPercentageValue.defaultProps = {
  className: undefined,
  colorOnChange: { duration: 0 },
};
