import clsx from 'clsx';
import { useSignIn } from 'overlay/SignIn';

export default function PlayerName({ position, className }) {
  const signIn = useSignIn();
  let name = signIn[position.ID] || position.NAME;
  if (name !== null && name == '') {
    name = position.NAME;
  }
  const fontSize = name?.length > 14 ? 'text-sm' : 'text-base';
  return (
    <span
      className={clsx(
        className,
        'text-ellipsis overflow-hidden whitespace-nowrap',
        { 'opacity-50 text-sm': name === position.NAME },
        fontSize,
      )}
    >
      {name ?? ''}
    </span>
  );
}
