export const BLUE_TEAM = 'blue';
export const GOLD_TEAM = 'gold';
export const TEAMS = [BLUE_TEAM, GOLD_TEAM];
export const LEFT_TEAM = 'left';
export const RIGHT_TEAM = 'right';
export const TEAM_OVERLAY_SIDES = [LEFT_TEAM, RIGHT_TEAM];

export const CABINET_POSITIONS = {
  GOLD_QUEEN: {
    ID: 1,
    NAME: 'Gold Queen',
    POSITION: 'queen',
    IMAGE: '/static/players/gold-queen.svg',
    ICON: '/static/players/gold-icon-queen.svg',
  },
  BLUE_QUEEN: {
    ID: 2,
    NAME: 'Blue Queen',
    POSITION: 'queen',
    IMAGE: '/static/players/blue-queen.svg',
    ICON: '/static/players/blue-icon-queen.svg',
  },
  GOLD_STRIPES: {
    ID: 3,
    NAME: 'Gold Stripes',
    POSITION: 'stripes',
    IMAGE: '/static/players/gold-drone-stripes.svg',
    ICON: '/static/players/gold-icon-stripes.svg',
  },
  BLUE_STRIPES: {
    ID: 4,
    NAME: 'Blue Stripes',
    POSITION: 'stripes',
    IMAGE: '/static/players/blue-drone-stripes.svg',
    ICON: '/static/players/blue-icon-stripes.svg',
  },
  GOLD_ABS: {
    ID: 5,
    NAME: 'Gold Abs',
    POSITION: 'abs',
    IMAGE: '/static/players/gold-drone-abs.svg',
    ICON: '/static/players/gold-icon-abs.svg',
  },
  BLUE_ABS: {
    ID: 6,
    NAME: 'Blue Abs',
    POSITION: 'abs',
    IMAGE: '/static/players/blue-drone-abs.svg',
    ICON: '/static/players/blue-icon-abs.svg',
  },
  GOLD_SKULLS: {
    ID: 7,
    NAME: 'Gold Skulls',
    POSITION: 'skulls',
    IMAGE: '/static/players/gold-drone-skulls.svg',
    ICON: '/static/players/gold-icon-skulls.svg',
  },
  BLUE_SKULLS: {
    ID: 8,
    NAME: 'Blue Skulls',
    POSITION: 'skulls',
    IMAGE: '/static/players/blue-drone-skulls.svg',
    ICON: '/static/players/blue-icon-skulls.svg',
  },
  GOLD_CHECKS: {
    ID: 9,
    NAME: 'Gold Checks',
    POSITION: 'checks',
    IMAGE: '/static/players/gold-drone-checks.svg',
    ICON: '/static/players/gold-icon-checks.svg',
  },
  BLUE_CHECKS: {
    ID: 10,
    NAME: 'Blue Checks',
    POSITION: 'checks',
    IMAGE: '/static/players/blue-drone-checks.svg',
    ICON: '/static/players/blue-icon-checks.svg',
  },
};

export const POSITIONS_BY_ID = {};
for (const pos of Object.values(CABINET_POSITIONS)) {
  POSITIONS_BY_ID[pos.ID] = pos;
}
export const POSITIONS_BY_NAME = {};
for (const pos of Object.values(CABINET_POSITIONS)) {
  POSITIONS_BY_NAME[pos.NAME] = pos;
}

export const POSITIONS_BY_TEAM = {};

POSITIONS_BY_TEAM[BLUE_TEAM] = [
  CABINET_POSITIONS.BLUE_STRIPES,
  CABINET_POSITIONS.BLUE_ABS,
  CABINET_POSITIONS.BLUE_QUEEN,
  CABINET_POSITIONS.BLUE_SKULLS,
  CABINET_POSITIONS.BLUE_CHECKS,
];

POSITIONS_BY_TEAM[GOLD_TEAM] = [
  CABINET_POSITIONS.GOLD_STRIPES,
  CABINET_POSITIONS.GOLD_ABS,
  CABINET_POSITIONS.GOLD_QUEEN,
  CABINET_POSITIONS.GOLD_SKULLS,
  CABINET_POSITIONS.GOLD_CHECKS,
];

for (const pos of POSITIONS_BY_TEAM[BLUE_TEAM]) pos.TEAM = BLUE_TEAM;
for (const pos of POSITIONS_BY_TEAM[GOLD_TEAM]) pos.TEAM = GOLD_TEAM;

export const SEASON_TYPES = {
  SHUFFLE: 'shuffle',
  BYOT: 'byot',
};

export const PERMISSION_TYPES = {
  ADMIN: 'admin',
  TOURNAMENT: 'tournament',
};

export const TOURNAMENT_LINK_TYPES = {
  MANUAL: 'manual',
  CHALLONGE: 'challonge',
};

export const MAP_NAMES = {
  map_day: 'Day',
  map_dusk: 'Dusk',
  map_night: 'Night',
  map_twilight2: 'Twilight',
};

export const WIN_CONDITIONS = {
  military: 'Military',
  economic: 'Economic',
  snail: 'Snail',
};

export const TEAM_NAMES = {
  blue: 'Blue',
  gold: 'Gold',
};

export const SIGN_IN_ACTIONS = {
  SIGN_IN: 'sign_in',
  SIGN_OUT: 'sign_out',
  NFC_REGISTER: 'nfc_register',
  NFC_REGISTER_TAPPED: 'nfc_register_tapped',
  NFC_REGISTER_SUCCESS: 'nfc_register_success',
  NFC_REGISTER_ERROR: 'nfc_register_error',
};

export const OVERLAY_SCENES = {
  HIDDEN: 'hidden',
  POSTGAME: 'postgame',
  MATCH_SUMMARY: 'match_summary',
  MATCH_PREVIEW: 'match_preview',
};

export const TEAM_TYPE = {
  FREE_AGENT: 'free-agent',
  JOIN_TEAM: 'join-team',
  CREATE_TEAM: 'create-team',
};

export const isQueen = pos =>
  pos === CABINET_POSITIONS.GOLD_QUEEN || pos === CABINET_POSITIONS.BLUE_QUEEN;

export const STAT_KEYS = [
  'militaryKills',
  'militaryDeaths',
  'totalBerries',
  'workerKills',
  'workerDeaths',
  'snailDistance',
];
export const STAT_ICON_PATHS = {
  militaryKills:
    'm22.32,17.7c.81.39,1.09,1.46.47,2.2-.42.51-1.16.62-1.75.33l-3.4-1.63-1.08,2.26c-.21.44-.73.62-1.17.41l-.61-.29c-.14-.07-.2-.23-.13-.37l1.32-2.75-3.21-1.21,1.93-.37-.94-2.12,1.51-.03,2,1.05,1.32-2.75c.07-.14.23-.2.37-.13l.61.29c.44.21.62.73.41,1.17l-1.08,2.26,3.45,1.65ZM5.52,10.22l.88-1.65,1.15,1.85.69-1.43,1.22.28-.81-6.04c0-.05-.03-.1-.06-.14L6.07.24c-.13-.14-.36-.1-.43.07l-1.43,3.52s-.02.1-.01.15l1.32,6.24ZM22.3,1.75l-.98,4.17c-.02.1-.07.19-.15.25l-12.1,11.23,2.2,2.2c.09.09.15.22.15.35,0,.13-.05.26-.15.35l-.1.1c-.28.28-.64.43-1.03.43s-.76-.15-1.03-.43l-1.9-1.9-3.24,3.24c-.6.6-1.63,1.27-2.58.32-.8-.8-.48-1.78.32-2.58l3.24-3.24-1.9-1.9c-.57-.57-.57-1.49,0-2.06l.1-.1c.2-.2.51-.2.71,0l2.2,2.2L17.27,2.28c.07-.07.16-.12.25-.15l4.17-.98c.17-.04.35.01.47.13s.17.3.13.47Zm-4.15,3.54c-.2-.2-.51-.2-.71,0L7.97,14.77l.71.71,9.47-9.47c.2-.2.2-.51,0-.71Z',
  militaryDeaths:
    'm12,14.16c0,1.2-.75,2.22-1.8,2.64v-4c0-.38-.3-.68-.68-.68s-.68.31-.68.68v4.55c-.14,1.18-.99,2.12-2.12,2.41v-5.75c0-.38-.3-.68-.68-.68s-.68.31-.68.68v7.72c-.46.94-1.42,1.6-2.54,1.6h-.77v-12.69c0-2.75,1.04-4.58,3.61-4.58,3.08,0,3.75,3.27,6.36,3.27s3.27-3.27,6.36-3.27c2.57,0,3.61,1.83,3.61,4.58v12.69h-.77c-1.12,0-2.08-.65-2.54-1.6v-7.72c0-.38-.3-.68-.68-.68s-.68.31-.68.68v5.75c-1.13-.28-1.98-1.23-2.12-2.41v-4.55c0-.38-.3-.68-.68-.68s-.68.31-.68.68v4c-1.05-.42-1.8-1.44-1.8-2.64h0Zm5.28-11c0-1.8-2.27-3.16-5.28-3.16s-5.28,1.36-5.28,3.16,2.27,3.16,5.28,3.16,5.28-1.36,5.28-3.16Zm-2.06,0c0,.28-1.11,1.1-3.22,1.1s-3.22-.82-3.22-1.1,1.11-1.1,3.22-1.1,3.22.82,3.22,1.1Z',
  totalBerries:
    'm16.43,18.91c0-.95.97-1.94,2.52-1.94,2.12,0,3.92,1.94,3.92,4.69,0,0-6.44-.66-6.44-2.75Zm-8.87,0c0-.95-.97-1.94-2.52-1.94-2.12,0-3.92,1.94-3.92,4.69,0,0,6.44-.66,6.44-2.75Zm4.44,4.31c1.27-.75,3.64-2.54,3.64-4.51,0-1.01-.82-1.76-1.7-1.74-1.17.04-1.94,1.19-1.94,1.19m0,0s-.77-1.15-1.94-1.19c-.88-.03-1.7.72-1.7,1.74,0,1.98,2.36,3.76,3.64,4.51m-3.02-17.21c-1.34,0-2.42,1.08-2.42,2.42s1.08,2.42,2.42,2.42,2.42-1.08,2.42-2.42-1.08-2.42-2.42-2.42Zm3.03,5.3c-1.34,0-2.42,1.08-2.42,2.42s1.08,2.42,2.42,2.42,2.42-1.08,2.42-2.42-1.08-2.42-2.42-2.42Zm3.03-5.3c-1.34,0-2.42,1.08-2.42,2.42s1.08,2.42,2.42,2.42,2.42-1.08,2.42-2.42-1.08-2.42-2.42-2.42Zm-9.15,5.25c-1.36,0-2.47,1.11-2.47,2.47s1.11,2.47,2.47,2.47,2.47-1.11,2.47-2.47-1.11-2.47-2.47-2.47Zm12.23.05c-1.34,0-2.42,1.08-2.42,2.42s1.08,2.42,2.42,2.42,2.42-1.08,2.42-2.42-1.08-2.42-2.42-2.42ZM12,.63c-1.38,0-2.5,1.12-2.5,2.5s1.12,2.5,2.5,2.5,2.5-1.12,2.5-2.5-1.12-2.5-2.5-2.5Z',
  workerKills:
    'm9.11,19.6l2.74.1-.04,2.56c-.11.07-.23.14-.35.2-1.78.87-3.97.47-5.29-1.11-1.22-1.47-1.31-3.48-.39-5.03.11-.2.23-.38.38-.56l3.35.36-.4,3.48Zm4.32-11.95c-.05.27-.07.54-.07.8l2.82-1.85,1.9,2.95,2.18-1.66,1.59,2c.07-.2.14-.39.18-.6.45-2.38-1.1-4.67-3.47-5.12s-4.67,1.1-5.12,3.48Zm8.28,14.87l-4.17-.98c-.1-.02-.19-.07-.25-.15L6.05,9.3l-2.2,2.2c-.09.09-.22.15-.35.15s-.26-.05-.35-.15l-.1-.1c-.28-.28-.43-.64-.43-1.03s.15-.76.43-1.03l1.9-1.9-3.24-3.24c-.6-.6-1.27-1.63-.32-2.58.8-.8,1.78-.48,2.58.32l3.24,3.24,1.9-1.9c.57-.57,1.49-.57,2.06,0l.1.1c.2.2.2.51,0,.71l-2.2,2.2,12.1,11.23c.07.07.12.16.15.25l.98,4.17c.04.17-.01.35-.13.47s-.3.17-.47.13Zm-3.54-4.15c.2-.2.2-.51,0-.71l-9.47-9.47-.71.71,9.47,9.47c.2.2.51.2.71,0Z',
  workerDeaths:
    'm22.75,15.81H1.25c-.13,0-.25.12-.24.25.15,3.71,3.53,1.21,3.53,3.62s.4,3.03.59,3.32c.17.26.7,1.61,6.22,1.61,5.08,0,6.64-1.48,6.93-2.34.11-.34.26-1.46.26-2.46,0-2.47,4.45.4,4.45-3.74,0-.14-.11-.25-.24-.25Zm-7.21,2.53c.64,0,1.15.52,1.15,1.15v2.01c0,.21-.11.41-.29.5-.38.19-.81.35-1.29.47-.36.1-.72-.16-.72-.54v-2.45c0-.64.52-1.15,1.15-1.15Zm-8-.01c.64,0,1.16.52,1.16,1.16v2.59c0,.35-.32.61-.67.55-.52-.09-.96-.2-1.28-.32-.22-.08-.36-.29-.36-.53v-2.29c0-.64.52-1.16,1.16-1.16Zm3.39,4.59c-.3,0-.55-.26-.55-.56v-2.89c0-.64.52-1.15,1.15-1.15s1.15.52,1.15,1.15v2.88c0,.3-.24.56-.54.56-.41.01-.81.02-1.22,0ZM23,6.47h0c0-3.41-2.3-5.68-11.27-5.68S1,3.06,1,6.47h0c0,.13.11.24.24.24h21.53c.13,0,.24-.11.24-.24Zm-14.59,3.11l-2.38,1.66,2.38,1.66-1.19,1.19-1.66-2.38-1.66,2.38-1.19-1.19,2.38-1.66-2.38-1.66,1.19-1.19,1.66,2.38,1.66-2.38,1.19,1.19Zm7.48,1.66l2.36,1.66-1.19,1.19-1.66-2.36-1.66,2.36-1.19-1.19,2.36-1.66-2.36-1.66,1.19-1.19,1.66,2.36,1.66-2.36,1.19,1.19-2.36,1.66Z',
  snailDistance:
    'm21.43,8.62c.29-1.24.37-2.71.23-4.11.7-.29,1.19-.98,1.19-1.79,0-1.07-.87-1.94-1.94-1.94s-1.94.87-1.94,1.94c0,.99.74,1.8,1.7,1.92.12,1.21.04,2.43-.15,3.44-.42-.14-.86-.18-1.31-.18s-.85.12-1.16.31c-.32-1.03-.78-2.06-1.2-2.88.37-.35.6-.85.6-1.39,0-1.07-.87-1.94-1.94-1.94s-1.94.87-1.94,1.94.87,1.94,1.94,1.94c.16,0,.32-.03.47-.06.48.94,1.02,2.17,1.29,3.28-.32.61-.42,1.31-.42,1.75,0,.92-.06,2.79-.68,3.76-.13.2-.54.38-.54.38,0,0-.06.21-.3-2.39-.33-3.56-3.45-6.29-7.61-6.29S.16,9.5.16,14.06c0,4.87,2.62,9.69,10.38,9.69,5.04,0,12.47-1.27,12.47-8.16,0-4.17-.65-6.09-1.58-6.97Zm-9.07,7.91c-.55-.03-.98-.5-.95-1.05.07-1.36-.22-2.39-.88-3.07-.8-.84-2.02-1-2.87-1-1.64.01-3.29,1-3.29,3.19,0,.75.24,1.38.68,1.76.41.36.99.49,1.7.38.66-.1,1.04-.37,1.13-.82.08-.41-.12-.85-.34-.98-.04-.02-.15-.09-.45.17-.42.36-1.05.32-1.41-.1-.36-.42-.32-1.05.1-1.41.86-.74,1.89-.89,2.76-.39.99.57,1.54,1.87,1.31,3.09-.24,1.29-1.29,2.2-2.8,2.43-.27.04-.53.06-.77.06-1.21,0-2.04-.47-2.54-.9-.89-.77-1.38-1.93-1.38-3.27,0-2.99,2.22-5.17,5.27-5.19,1.76-.01,3.3.55,4.33,1.62,1.05,1.1,1.53,2.63,1.43,4.56-.03.55-.51.97-1.05.95Z',
};
